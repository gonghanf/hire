<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | paypal参数设置
// +----------------------------------------------------------------------

return [
    // 必要配置
    'url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',//沙箱测试
    //'url' => 'https://www.paypal.com/cgi-bin/webscr', // 正式生产
    'client_id' => 'AUwpsfStaO9hnqTfohlqiyNGW--9ruAE6jK834p4yx58meg_RX5Q1L6OHtDKp7iUK0Nwf_toLZjLf9wL',
    'secret' => 'EAHBFkZyy5ayPIYMU6lresGFVnhGvjMo0uYLtWJOxEpjyGgeFFWCY2e3IfDRa89-eteKz5fEcJxbQiEm',
    'business' => '304761681-facilitator@qq.com'

];
