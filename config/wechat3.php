<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 微信参数设置
// +----------------------------------------------------------------------
return [
    // 必要配置
    'app_id' => 'wx890871734cd97893',
    'secret' => 'b99b809873271589d96535cfc742811a',
    'mch_id'             => '1559007601',
    'key'                => '418097d7f345c40f6c75ad31709d3f23',   // API 密钥

    // 如需使用敏感接口（如退款、发送红包等）需要配置 API 证书路径(登录商户平台下载 API 证书)
//    'cert_path'          => dirname(__FILE__).'\wxcert\apiclient_cert.pem', // XXX: 绝对路径！！！！
//    'key_path'           => dirname(__FILE__).'\wxcert\apiclient_key.pem',  // XXX: 绝对路径！！！！

    'notify_url'         => $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST'].'/mobile/notify/wxpayNotify',     // 你也可以在下单时单独设置来想覆盖它
    /**
     * OAuth 配置
     *
     * scopes：公众平台（snsapi_userinfo / snsapi_base），开放平台：snsapi_login
     * callback：OAuth授权完成后的回调页地址
     */
//    'oauth' => [
//        'scopes'   => ['snsapi_base'],
//        'callback' => $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER['HTTP_HOST'].'/mobile/login/getOpenid',
//    ],
];
