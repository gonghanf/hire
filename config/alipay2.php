<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 微信参数设置
// +----------------------------------------------------------------------
return [
    //应用ID,您的APPID。
    'app_id' => "2019101468394545",

    //商户私钥, 请把生成的私钥文件中字符串拷贝在此
    //'merchant_private_key' => "MIIEogIBAAKCAQEAr3RDnNPxHz7U9PGF6ufBi1IE6eYlHHjbj8O/6xPWHu8opmUUKnx7voURhr4ZirzIfUpV/NGcfgAzYAGI+zBwdEqqr9/Euai4+2T13Gr2mM9fU9176WhV2wW22jNuvsAQXCzgN8YbjskGoT3FOcSIhKqq2A3B9p0sh8i02I5pnSqJoum48H83Lb6LCafff+HaPh/63pRMpbyfVIGLT4miTLYkwgUkNxyFYSZmsxN81PwBUeRabtihi6/kjlbjLyn8XTQ44+gXYl/BC1mtDYpExqcl4iu376wA9BrtO/PcZQ9SarB3A/+o9kIFQ+tCDjDIFYMGSy+sAvvBiMjIdVnz2wIDAQABAoIBAAmS1n2gMu/5hbU32I7QxobLmukIXc4H6jMWA0rYcj6N3jH2IfuPf1EOAb7QcKRZWLc9byO0bE2TxTnf25GZOwmdaWLfWuSmiDFveNrmGz6LUjBJTZGW19hzn16Yn9pYsIvXBwUEEkzgOzRhInncd3aFuXzAg2zpdpySHwwR+UFSEbd3XxGW3jFcIsr+jKlTBsQ5LTfjt50um59UXG9nlEPA4UdwqzqpIpT44ntZPRwiXC2zBmMtZ2E+5JWhU+fZIYir4RB8KKFPdliA/JdPe1zMTYbThbidL0I/wKEQRo3+UTB0+6NVjnrvvM+QBQaNc/k05I3a4WpAKRMZsvevDcECgYEA31mxohudIBQZwa/5CojWYBLjLy8i6D3cEOQFE57mSqQ4w7CHnDedFbYm8GWMTFUj+F06n30tdb6mAF3v4zCpBK5SqU5xwpTngQj1J5g2Iv1tKlOXn89AdRma2t3CH4rB7Ccke+cw02uJu3jaXn7dZkVBOMdkg3uxQglmElR+socCgYEAyRot4qV/BwxOAq7rkD/NC7CuqT8gGnZSd63EW+4z+X7i9fL/WDmZ+mYGX6oKTnfdKr29e4CS7zDQNAtTawGUVzI3V7gGADkhy8CUQ5Nj+fZ+ioJ5/zRBv3AMLcypdfYEJsf5EyS9heZ7BtYsn4KMeNFXJzusSM4xojMEWmVvxQ0CgYB/dneaVPn5qZ79W0jcCzWUhJLkhZj9QvhJJ2gCR4eTh17hxufNbiuWjWpxfPJxSZGKluHyzQRZkuC3iuRxGn8KrdV8y8i+TC5GBG+sFgCnPUvEgQoh6KwXZ/Jo/29egtiixb/behfyQAfNVm4AyZPeXp2XIqupIbkKP8ThpxjbFwKBgFivyOiZH6oWDRk11wDogh7TrlNJWXkSEVBiO6RnKlivUpwwq9dWn8dD0y/BJ4ZvagP9ZWDf1OzTVB5/mCwqqCbEKI/wwgS7LWJT7Y7Xo1GLGUcwP2kCvLQWT+iqx1kUQ8RukDOXElXIzRy4uzklM5nciFjvAg2hkVX8tcC/10jJAoGAO24XoTe7JajyNQMASYJ2ppfaZFqwAmWYANFZaJcWgpRDoM6J97LxgP+ajiL0cpu8avg31uEjovS954KRIbUIM2Rnqkvvx4tkhvsPYT+H/E8ySHGN+LQrteH4VrLgLbZUNRyWTufDC/qLSAG2tDk7f1dBqbb1XBryKKG0eGpUjh4=",
    'merchant_private_key' => 'MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCfngv8ZQwoRi4BALEniA8vABpoRQTEckkqqOGWXbYjhYksHOTWDwplFxC6ziTbRMTUaQYHORQ7UeS3dI8VEC7KuHFcIZac+EAGRINCUEEtZPHfnDy5mSIeifg+9oBhmwypf9jADuiqe0APJOp/msRwA/hwcEY7TIxnsjzHMiYXMxcw4Nkb3Z+ws9t8C6vnL2PNgqwsPTNuAyUOkxn2otT0IO/yiyWzOSjkGeTf0glWFsPc0WHhBJhxljsa5+xh0mbpRx1FEnF0yos65GHWljoEm9Nmg1KeoJl/rRMQmJ9GzaIJr2eyn1SXs8kmzKb6fcQdtFrU1jSmuRleNjjse+ixAgMBAAECggEAKDmoQ58jhTLKfS4zdwbu7KVWAlkCbo9uqPWOhFpxgFoyy48sjFWWBQvfB3TUDP4zrGQbRM65rolg3mrddC1z3VrGFrXQQxa869dwTNH02v/JhNTW6N8SniefDB8LE44DjuPNN/mJ4QzWg/qFrkrOnnwhrBRfXqpX5/ofHwQgXU/2Hs76fNhqwwXgR4UQxRgyGPJY6vnhwFKyNnwFLIVb8HnjdAdQJsMJCbYmq/qJE0X5gLOk4WRLwOnP6LsfpnQy1dxwMaI3GGsKD2yev+0AlGHrGs9aehC1uU6Q0cBkgRqByHu1i+TAibSADXINCngTvr2+sjIdEkbho4TOdzC2kQKBgQD/pzJH+pHf++DmnWrXEmbZJPduRQ0ebtQmR3ZWxjViMUrk/K6EZ+uAWlwVYc7PvQZeUjuL+aJWOuIw9hyjkid/pw+bI2nSiIi5NcS/5E7CTiM59OXa44m0S+Tk+q4iQuzmzCZw2WGgrJG5qG0kuHl/pcC/2nXBbrbv4Bzv1aMCpwKBgQCf1X3Qf048JmYoD3a6gm6Himef58Oxo0q7bU0Q7ex66qyQyiuticAdRfzMDJhR38JjVHDrtkLeX8kLh8MITmYf9QmYmwSuIlm201uNqe2u3yqsN/qCrtNxKwkPwzBEE1TQRjEj3fq8T/3jEjYtjqHGv/kZ5NmcQc4lHYavg5vc5wKBgB41Y3ud4B7XZHvbngRL6FtAQvyErEgsUGU1jIY0ZZp0uKTWNsjufqVsB/V+2TEy5M3Aacjgdo455wBiFIA/hOtQkkeCp754467+64DBOY08Lm1qwr5apKrpTOLBvFHQFq4u8VvkU+Tof0+w1zS/l5u9ukjxR3jT3En78W9ljx2BAoGADUlqFBMBJe7rPxlJ/WcqWQogYyKZGgNBCyOi7G1a4zzu63pi9yIkXkqbITtKzkOTnT4/xNh55pJ/2/2FMQOTacSs74qzcOZaHuCjE7u0jnzuxzJAGOz/lsDFGBJZyM7ylf2/yAOD/HOJ3358En1w5VzAEG79yAeQY3V45M5D/rECgYEAlvBW0IvYmCi5ria8cPCNvziBE29AUNxK2QcwY+bjEmCSL0mBvQouDRyoi1liEaFGvDj/53rwUv8R7ZzANr5I/Vc6VhIfsv7TrmE4LHHNFrbsMp0yki1FuJHs0CybB/mIE2bSQvB/hHJcfPXt25d5UNFfwrCtcyXSGzLUu8iRYaQ=',
    //异步通知地址
    'notify_url' => \request()->domain()."/api/pay_notify/aliNotify",

    //同步跳转
    'return_url' => \request()->domain()."/mobile/company/companyCenter",

    //编码格式
    'charset' => "UTF-8",

    //签名方式
    'sign_type'=>"RSA2",

    //支付宝网关
    'gatewayUrl' => "https://openapi.alipay.com/gateway.do",

    //支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    'alipay_public_key' => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAn54L/GUMKEYuAQCxJ4gPLwAaaEUExHJJKqjhll22I4WJLBzk1g8KZRcQus4k20TE1GkGBzkUO1Hkt3SPFRAuyrhxXCGWnPhABkSDQlBBLWTx35w8uZkiHon4PvaAYZsMqX/YwA7oqntADyTqf5rEcAP4cHBGO0yMZ7I8xzImFzMXMODZG92fsLPbfAur5y9jzYKsLD0zbgMlDpMZ9qLU9CDv8oslszko5Bnk39IJVhbD3NFh4QSYcZY7GufsYdJm6UcdRRJxdMqLOuRh1pY6BJvTZoNSnqCZf60TEJifRs2iCa9nsp9Ul7PJJsym+n3EHbRa1NY0prkZXjY47HvosQIDAQAB',
];
