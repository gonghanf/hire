//designWidth:设计稿的实际宽度值，需要根据实际设置
//maxWidth:制作稿的最大宽度值，需要根据实际设置
//这段js的最后面有两个参数记得要设置，一个为设计稿实际宽度，一个为制作稿最大宽度，例如设计稿为750，最大宽度为750，则为(750,750)
;
(function (designWidth, maxWidth) {
    var doc = document,
        win = window,
        docEl = doc.documentElement,
        remStyle = document.createElement("style"),
        tid;

    function refreshRem() {
        var width = docEl.getBoundingClientRect().width;
        maxWidth = maxWidth || 540;
        width > maxWidth && (width = maxWidth);
        var rem = width * 100 / designWidth;
        remStyle.innerHTML = 'html{font-size:' + rem + 'px;}';
    }

    if (docEl.firstElementChild) {
        docEl.firstElementChild.appendChild(remStyle);
    } else {
        var wrap = doc.createElement("div");
        wrap.appendChild(remStyle);
        doc.write(wrap.innerHTML);
        wrap = null;
    }
    //要等 wiewport 设置好后才能执行 refreshRem，不然 refreshRem 会执行2次；
    refreshRem();

    win.addEventListener("resize", function () {
        clearTimeout(tid); //防止执行两次
        tid = setTimeout(refreshRem, 300);
    }, false);

    win.addEventListener("pageshow", function (e) {
        if (e.persisted) { // 浏览器后退的时候重新计算
            clearTimeout(tid);
            tid = setTimeout(refreshRem, 300);
        }
    }, false);

    if (doc.readyState === "complete") {
        doc.body.style.fontSize = "16px";
    } else {
        doc.addEventListener("DOMContentLoaded", function (e) {
            doc.body.style.fontSize = "16px";
        }, false);
    }
})(750, 750);

function changeLanguage(lang) {
    $.post('/home/index/changeLang',{lang:lang},function(res) {
        window.location.reload();
    })
}
//ajax加载数据
$.fn.ajaxLoadData = function (laypage,param) {
    var first_obj = this.eq(0);
    let default_option = {
        url:'',
        data:{},
        page:1,
        limit:10,
        templet: function () {
            return '';
        }
    };
    let option = $.extend({},default_option, param );
    var post_data = option.data;
    post_data.page = option.page;
    post_data.limit = option.limit;

    $.post({
        url: option.url,
        data: option.data,
        dataType: 'json',
        success: function(res) {
            var str = '';
            if(res.code == 1) {
                if (res.data.total>0){
                    for (var i = 0; i < res.data.data.length; i++) {
                        var o = res.data.data[i];
                        str += option.templet(o);
                    }
                } else{
                    if (option.page == 1){
                        str = '<div style="display:flex;justify-content:center;align-items:center;line-height: 50px">暂无数据</div>'
                    } else{
                        str = '<div style="display:flex;justify-content:center;align-items:center;line-height: 50px">无更多数据</div>'
                    }

                }
            }else{
                str = '<div style="display:flex;justify-content:center;align-items:center;line-height: 50px">暂无数据</div>'
            }
            $(first_obj).html(str);
            let page_item = '<div id="paging" class="mineCont-right-paging"></div>';
            $(first_obj).next('#paging').remove();
            $(first_obj).after(page_item);

            //总页数大于页码总数
            laypage.render({
                elem: 'paging',
                count: res.data.total?res.data.total:0,
                limit:option.limit,
                curr:option.page,
                theme: '#3373b3',
                jump: function (obj,first) {
                    option.page = obj.curr;
                    option.limit = obj.limit;
                    if (!first){
                        $(first_obj).ajaxLoadData(laypage,option);
                    }
                }
            });
        }
    });
    first_obj.curr_param = option;
    first_obj.laypage = laypage;
    first_obj.ajaxReloadData = function(laypage2,param2){
        laypage2 = laypage2?laypage2:this.laypage;
        param2 = param2?param2:this.curr_param;
        $(first_obj).ajaxLoadData(laypage2,param2);
    };
    return first_obj;
};
