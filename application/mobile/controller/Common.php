<?php
namespace app\mobile\controller;
use app\common\controller\GG;
use think\Db;
use clt\Leftnav;
use think\Controller;
use EasyWeChat\Factory;
use think\facade\Request;

class Common extends GG
{
    protected $bindMobile,$isLogin,$uid,$username,$user_avatar,$user_level,$user_type,$user_sn,$page,$pageSize;
    public function initialize()
    {
        if(!isMobile()){
            $this->redirect('home/index/index');
        }
        $controller = request()->controller();
        $action = request()->action();
        $this->is_login = true;
        if($controller == 'Hall' && in_array($action, ['index','likeposition','likecompany','changelang'])) {
            if(!session('uid')) {
                $this->is_login = false;
            }
        } else {
            if (!session('uid')){
                $this->redirect('login/login');
                exit;
            }
        }

        $this->uid = session('uid');
        $this->user_level = session('user_level');
        $this->user_type = session('user_type');
        $this->is_intermediary = session('is_intermediary');
        $this->licence = session('licence');
        $this->card1 = session('card1');
        $this->card2 = session('card2');
        $this->verify = session('verify');
        $this->username = session('user_name');
        $this->user_avatar = session('user_avatar');
        $this->user_sn = session('user_sn');
        $this->page = input('page')?:1;
        $this->pageSize = input('limit')?:config('app.pageSize');
        if(session('language') && session('language') == 'en') {
            $this->assign('language', 'en');
        } else {
            $this->assign('language', 'cn');
        }

        // if($controller == 'Upload' || ($controller == 'User' && $action == 'editfile')) {

        // } else if($this->user_type == 1) {
        //     // 求职者
        //     if(empty($this->card1) || empty($this->card2)) {
        //         // 没有上传身份证
        //         $this->redirect('login/uploadFile');
        //     }
        // } else if($this->user_type == 2) {
        //     // 求职者
        //     if((empty($this->card1) || empty($this->card2)) && $this->is_intermediary == 1) {
        //         // 没有上传身份证
        //         $this->redirect('login/uploadFile');
        //     } else if(empty($this->licence) && $this->is_intermediary == 0) {
        //         // 没有上传营业执照
        //         $this->redirect('login/uploadFile');
        //     }
        // }
//         if (input('token')){
//             $token = input('token');
//             $user = Db::name('users')->where([['token','eq',$token]])->find();
//             if ($user){
//                 if ($user['is_lock'] == 1){
//                     gg(-9,'您的账号已被禁用，请联系管理员解禁');
//                 }
//                 $this->isLogin = true;
//                 $this->uid = $user['id'];
//                 $this->username = $user['username'];
//                 $this->avatar = $user['avatar'];
//                 $this->page = input('page')?:1;
//                 $this->pageSize = input('limit')?:config('app.pageSize');
//
//             }else{
//                 gg(-9,'数据异常，请重新登录');
//             }
//
//         }else{
//             gg(-9,'数据异常，请重新登录');
//         }

    }
    //空操作
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }

    public function changeLang() {
        $lang = input('lang', 'en');
        
        session('language', $lang);
        return ['code'=>1,'msg'=>'切换成功'];
    }
}
