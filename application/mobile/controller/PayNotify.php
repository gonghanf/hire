<?php
/**
 * Author: 北川
 * Time: 2019/2/13 8:44
 *
 */

namespace app\mobile\controller;

use alipay\Wappay;
use alipay\Pagepay;
use alipay\Notify;
use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order;
use think\Controller;
use think\Db;

class PayNotify extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @author 北川
     * @time 2019/3/16 8:56
     * @comment 微信支付回调
     */
    public function wxNotify(){
        $options = config('app.wxpay');
        //Db::name('aaa')->insert(['value'=>json_encode($options)]);
        $app = new Application($options);
        $response = $app->payment->handleNotify(function($notify, $successful){
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order_info = Db::name('question_order')->where(['sn'=>$notify->out_trade_no])->find();
            if ($successful) {
                //订单逻辑处理
                return $this->handleSuccessLogistics($order_info);
            }else{
                return false;
            }
        });
        return $response;
    }

    /**
     * @author 北川
     * @time 2019/3/16 8:57
     * @comment 阿里支付回调
     */
    public function aliNotify(){
        $data = input('post.');
        //$data['out_trade_no'] = '2019031850985653';
        //Log::write($_POST,'notice');
        //Db::name('aaa')->insert(['value'=>json_encode($data)]);
        $order_info = Db::name('question_order')->where(['sn'=>$data['out_trade_no']])->find();
        if (!$order_info){
            echo 'fail1';
        }
        $param = [
            'out_trade_no' => $order_info['sn'],
            'total_amount' => $order_info['order_amount']
        ];

        if (PayNotify::check($param,$data)){//验签成功
            if ($data['trade_status'] == 'TRADE_SUCCESS') {//如果支付成功
                //订单逻辑处理
                $re = $this->handleSuccessLogistics($order_info);
                if ($re){
                    echo "success";
                }else{
                    echo 'fail2';
                }
            }else{
                echo 'fail3';
            }
        }else{
            echo 'fail4';
        }
    }

    /**
     * @param $order_info
     * @return bool
     * @author 北川
     * @time 2019/3/16 8:57
     * @comment 支付成功需要处理的逻辑
     */
    public function handleSuccessLogistics($order_info){
        if (empty($order_info)){
            return false;
        }
        // 检查订单是否已经更新过支付状态
        if ($order_info['pay_state'] == 1) {
            return true; // 已经支付成功了就不再更新了
        }
        Db::startTrans();
        try{
            //===============修改订单状态===========================//
            // 不是已经支付状态则修改为已经支付状态
            $sdata = [
                'pay_state' => 1,//已支付
                'pay_time' => time(),
                'update_time' =>time()
            ];
            //更新订单状态
            $re = Db::name('question_order')->where(['id'=>$order_info['id']])->update($sdata);
            if (!$re){
                Db::rollback();
            }
            $count = Db::name('question_title')->where(['id'=>$order_info['title_id']])->value('count');

            //更新会员量表可用次数
            $re2 = Db::name('member_question')->where(['member_id'=>$order_info['member_id'],'title_id'=>$order_info['title_id']])->setInc('count',$count);
            if (!$re2){
                Db::rollback();
            }
            $re3 = Db::name('member_question')->where(['member_id'=>$order_info['member_id'],'title_id'=>$order_info['title_id']])->setInc('buy_count',$count);
            if (!$re3){
                Db::rollback();
            }

            //提交事务
            Db::commit();
            return true;
        }catch(\Exception $e){
            //回滚
            Db::rollback();
            return false;
        }
    }
}