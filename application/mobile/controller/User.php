<?php
/**
 * Author: 北川
 * Time: 2019/6/19 23:08
 * @comment　
 */

namespace app\mobile\controller;


use app\common\controller\GG;
use app\common\model\Position;
use app\common\model\Users;
use app\common\model\WebConfig;
use tests\thinkphp\library\think\log\driver\fileTest;
use think\Db;

class User extends Common
{
    /**
     * @author 北川
     * @time 2019/6/19 23:14
     * @comment　用户详情表
     */
    public function userInfo(){
        $id = $this->uid;

        if($this->user_type == 2) {
            $this->redirect(url('/mobile/company/companyCenter'));
        }

        $user_model = new \app\common\model\Users();
        $info = $user_model->getDetail($id);
        $info['tags'] = $info['tags_arr'];
//        $info['tags'] = explode(',',$info['tags']);

        $sendCount = Db::name('resume_position')->where([['uid','eq',$id]])->count();
        $collectionCCount = Db::name('collection')->where([['uid','eq',$id],['type','eq',1]])->count();
        $collectionPCount = Db::name('collection')->where([['uid','eq',$id],['type','eq',2]])->count();

        $this->assign('collection_company_count', $collectionCCount);
        $this->assign('collection_position_count', $collectionPCount);
        $this->assign('send_count', $sendCount);
        $this->assign('info',$info);

        $this->assign('edu_log',$info['edu_log']);
        $this->assign('work_log',$info['work_log']);
        //$this->assign('education',$this->getEduLevel());

        //$this->assign('region_data',json_encode($this->allRegionData()));

        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:31
     * @comment　修改信息接口
     */
    public function editUserInfo(){
        $id = $this->uid;
        $data = input('post.');
        unset($data['file']);
        $users_model = new Users();
        $r = $users_model->isUpdate(true)->allowField(true)->save($data,['id' => $id]);
        if ($r){
            gg(1,'修改成功');
        }else{
            gg(0,'修改失败');
        }
    }

    public function editFile(){
        $id = $this->uid;
        $card1 = input('card1', '');
        $card2 = input('card2', '');
        $licence = input('licence', '');
        $data = ['card1'=>$card1,'card2'=>$card2,'licence'=>$licence];
        $users_model = new Users();
        $r = $users_model->isUpdate(true)->allowField(true)->save($data,['id' => $id]);
        if ($r){
            session('card1', $card1);
            session('card2', $card2);
            session('licence', $licence);
            // if(!empty('licence')) {
            //     gg(1,'上传成功，请等待后台审核');
            // }
            gg(1,'上传成功');
        }else{
            gg(0,'上传失败');
        }
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/6/19 23:06
     * @comment　更换手机号
     */
    public function changMobile(){

        $mobile = input('mobile');
        $code = cache('change_mobile'.$mobile);
        if (!input('code')){
            gg(0,'验证码不能为空');
        }
        if ($code != input('code')){
            gg(0,'验证码错误');
        }
        $has_mobile = Db::name('users')->where([['mobile','eq',$mobile]])->find();

        if ($has_mobile){
            if ($has_mobile['id'] == $this->uid){
                gg(0,'不能与旧手机号相同');
            }else{
                gg(0,'该手机号已绑定，请更换其他手机号');
            }
        }

        $r = Db::name('users')->where([['id','eq',$this->uid]])->update(['mobile'=>$mobile]);
        if ($r){
            gg(1,'更换手机号成功');
        }else{
            gg(0,'更换手机号失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/6/21 23:31
     * @comment　手机号解除绑定 --无用
     */
    public function releaseMobile(){

        $mobile = Db::name('users')->where([['id','eq',$this->uid]])->value('mobile');
        $code = cache('release_mobile'.$mobile);
        if (!input('code')){
            gg(0,'验证码不能为空');
        }
        if ($code != input('code')){
            gg(0,'验证码错误');
        }

        $r = Db::name('users')->where([['id','eq',$this->uid]])->update(['mobile'=>'']);
        if ($r){
            session('bindMobile', false);
            gg(1,'解绑成功');
        }else{
            gg(0,'解绑失败');
        }
    }



    /**
     * @author 北川
     * @time 2019/8/13 23:26
     * @comment　上传身份证照片
     */
    public function editCardImg(){
        if (request()->isPost()){
            $id = $this->uid;
            $card1 = input('card1');
            $card2 = input('card2');
            if(!$card1){
                gg(0,'请上传身份证正面');
            }
            if (!$card2){
                gg(0,'请上传身份证反面');
            }
            $r = Db::name('users')->where([['id','eq',$id]])->update(['card1' => $card1,'card2' => $card2,'update_time'=>time()]);
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }
        return $this->fetch();
    }


    /**
     * @param string $id
     * @return mixed
     * @author 北川
     * @time 2019/8/14 21:22
     * @comment　个人简历
     */
    public function myResume(){
        //$id = input('id',0);
        $id= $this->uid;
        if(request()->isPost()){
            $data = input('post.');
            unset($data['file']);
            $user_data = [
                'username' => $data['username'],
                'avatar' => $data['avatar'],
                //'email' => $data['email'],
                //'mobile' => $data['mobile'],
                //'sex' => $data['sex']
            ];

            Db::startTrans();
            //保存基本信息到users表
            if ($id){
                $r1 = Db::name('users')->where([['id','eq',$id]])->update($user_data);
            }else{
                $r1 = Db::name('users')->insertGetId($user_data);
                $id= $r1;
            }
            if ($r1 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //保存简历信息到简历表
            $resume_data = [
                'uid' => $id,
                'education' => $data['education'],
                'tags' => $data['tags'],
                'experience' => $data['experience'],
                'salary_min' => $data['salary_min'],
                'salary_max' => $data['salary_max'],
                'province' => $data['province'],
                'city' => $data['city'],
                'district' => $data['district'],
                'full_address' => $data['province'].' '.$data['city'].' '.$data['district'],
                'position_type' => $data['position_type'],
                'self_intro' => $data['self_intro'],
            ];
            $resume_info = Db::name('resume')->where([['uid','eq',$id],['is_del','eq',0]])->find();
            if ($resume_info){
                $r2 = Db::name('resume')->where([['id','eq',$resume_info['id']]])->update($resume_data);
            }else{
                $r2 = Db::name('resume')->insertGetId($resume_data);
            }
            if ($r2 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //保存教育经历
            //先删除就记录
            $r3 = Db::name('education_exp')->where([['uid','eq',$id]])->delete();
            if ($r3 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //再插入记录
            if (isset($data['school'])){
                $edu_data = [];
                foreach ($data['school'] as $k=>$v){
                    $edu_data[] = [
                        'uid' => $id,
                        'school' => $v,
                        'major' => $data['major'][$k],
                        'reward' => $data['reward'][$k],
                    ];
                }
                $r4 = Db::name('education_exp')->insertAll($edu_data);
                if ($r4 === false){
                    Db::rollback();
                    gg(0,'操作失败');
                }
            }
            //保存工作经历
            //先删除就记录
            $r5 = Db::name('work_exp')->where([['uid','eq',$id]])->delete();
            if ($r5 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //在保存记录
            if (isset($data['company'])){
                $edu_data = [];
                foreach ($data['company'] as $k=>$v){
                    $edu_data[] = [
                        'uid' => $id,
                        'company' => $v,
                        'position' => $data['position'][$k],
                        'desc' => $data['desc'][$k],
                    ];
                }
                $r6 = Db::name('work_exp')->insertAll($edu_data);
                if ($r6 === false){
                    Db::rollback();
                    gg(0,'操作失败');
                }
            }

            //提价表单
            Db::commit();
            gg(1,'操作成功');

        }else{

            $user_model = new \app\common\model\Users();

            $info = $user_model->getDetail($id);
            $info['tags'] = $info['tags_arr'];
//            $info['tags'] = array_filter(explode(',',$info['tags']));
//            $info['qualification'] = array_filter(explode(',',$info['qualification']));

            $prov_arr = Db::name('region')->where('pid',0)->select();
            $prov_id = Db::name('region')->where([['name','eq',$info['province']],['type','eq',1]])->value('id');
            $city_arr = Db::name('region')->where('pid',$prov_id)->select();
            $city_id = Db::name('region')->where([['name','eq',$info['city']],['type','eq',2]])->value('id');
            $dist_arr = Db::name('region')->where('pid',$city_id)->select();

            $this->assign('info',$info,true);

            $config = WebConfig::allConfig();

            $tag_arr = [];
            foreach ($config['tags'] as $k=>$v){
                $tag_arr[] = $v['name'];
            }
            $tags = array_unique(array_merge($tag_arr,$info['tags']));
            $qual_arr = [];
            foreach ($config['qualification'] as $k=>$v){
                $qual_arr[] = $v['name'];
            }
            $quals = array_unique(array_merge($qual_arr,$info['qualification']));
            $country = Db::name('country')->select();

            $this->assign('country',$country);
            $this->assign('tags',$tags);
            $this->assign('quals',$quals);
            $this->assign('position_type',$config['position_type']);
            $this->assign('experience',$config['experience']);
            $this->assign('education',$config['education']);

            $this->assign('edu_log',$info['edu_log']);
            $this->assign('work_log',$info['work_log']);

            $this->assign('province',$prov_arr);
            $this->assign('city',$city_arr);
            $this->assign('district',$dist_arr);
            //$this->assign('region_data',json_encode($this->allRegionData()));

            return $this->fetch();
        }
    }

    public function myCompany() {
        $id = $this->uid;

        $info = Db::name('users')->where([['id','eq',$id]])->find();

        $industry = Db::name('web_config')->where([['type','eq','industry']])->order('sort')->select();
        $scale = Db::name('web_config')->where([['type','eq','scale']])->order('sort')->select();

        $this->assign('info', $info);
        $this->assign('industry', $industry);
        $this->assign('scale', $scale);
        return $this->fetch();
    }

    public function mySend() {
        return $this->fetch();
    }

    public function myScore() {
        $point = Db::name('users')->where('id',$this->uid)->value('point');
        $total = Db::name('point_detail')->where([['uid','eq',$this->uid],['type','eq',1]])->sum('amount');
        $this->assign('total',$total);
        $this->assign('point',$point);
        return $this->fetch();
    }

    public function myQrcode() {
        $avatar = session('user_avatar');
        $invite_url = request()->domain().'/'.'mobile/login/mobileregister.html?pid='.session('uid');
        $this->assign('avatar',$avatar);
        $this->assign('invite_url',$invite_url);
        return $this->fetch();
    }

    public function myVideo() {
        $video = Db::name('users')->where('id',session('uid'))->value('video');
        $this->assign('video_url',$video);
        $this->assign('qiniu_domain',config('qiniu.url'));
        return $this->fetch();
    }

    public function myCollection() {
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:09
     * @comment　职位申请列表
     */
    public function positionAppList(){
        $uid = $this->uid;
        $where = [];
        $where[] = ['a.uid','eq',$uid];
        $where[] = ['a.is_del','eq',0];
        $where[] = ['b.id','gt',0];
        $field = 'a.status app_status,a.create_time,b.name,b.salary,b.salary_min,b.salary_max,b.province,b.city,b.district,b.full_address,b.id position_id,b.status position_status,b.is_del';
        $list = Db::name('resume_position')
            ->alias('a')
            ->leftJoin('position b','a.position_id = b.id')
            ->field($field)
            ->where($where)
            ->order('a.id desc')
            ->paginate(['list_rows'=>$this->pageSize,'page'=>$this->page])
            ->toArray();
        foreach ($list['data'] as $k=>$v){
            $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            $list['data'][$k] = $v;
        }
        gg(1,'success',$list);
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:35
     * @comment　用户收藏列表
     */
    public function userCollection(){
        $uid= $this->uid;
        $type = input('type');//1为收藏企业 2为职位
        if ($type == 2){ //收藏职位列表
            $where = [];
            $where[] = ['a.uid','eq',$uid];
            $where[] = ['a.type','eq',2];//收藏职位
            $field = 'a.id,a.create_time,b.name,b.salary,b.salary_min,b.salary_max,b.province,b.city,b.district,b.full_address,b.id position_id,b.status position_status,b.is_del,b.experience,b.education';
            $list = Db::name('collection')
                ->alias('a')
                ->leftJoin('position b','a.target_id = b.id and a.type=2')
                ->field($field)
                ->where($where)
                ->order('a.id desc')
                ->paginate(['list_rows'=>$this->pageSize,'page'=>$this->page])
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k] = $v;
            }
            gg(1,'success',$list);
        }else{//收藏企业列表
            $where = [];
            $where[] = ['a.uid','eq',$uid];
            $where[] = ['a.type','eq',1];//收藏企业
            $field = 'a.id,a.create_time,b.avatar,b.username,b.mobile,b.email,b.contact,b.contact_mobile,b.industry,b.scale,b.desc,b.is_lock,a.target_id';
            $list = Db::name('collection')
                ->alias('a')
                ->leftJoin('users b','a.target_id = b.id and a.type=1')
                ->field($field)
                ->where($where)
                ->order('a.id desc')
                ->paginate(['list_rows'=>$this->pageSize,'page'=>$this->page])
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k] = $v;
            }
            gg(1,'success',$list);
        }
    }
}