<?php
namespace app\mobile\controller;
use app\common\controller\GG;
use app\common\controller\UpFiles;
use clt\Lunar;
use think\Db;
use EasyWeChat\Factory;

class Index extends Common{
    public function initialize(){
        parent::initialize();
    }
    public function index() {
    	$this->redirect('/mobile/hall/index');
    }
}