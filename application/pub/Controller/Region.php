<?php
/**
 * Author: 北川
 * Time: 2019/8/14 21:38
 * @comment　
 */

namespace app\pub\controller;


use app\common\controller\GG;
use think\Db;

class Region extends GG
{
    /**
     * @param int $pid
     * @author 北川
     * @time 2019/8/14 21:39
     * @comment　获取子级地区
     */
    public function getChildRegion(){
        $pid = input('pid',0);
        $region = Db::name('region')->where([['pid','eq',$pid]])->select();
        gg(1,'succes',$region);
    }
    /**
     * @param int $pid
     * @author 北川
     * @time 2019/8/14 21:39
     * @comment　获取子级地区 通过名称和层级
     */
    public function getChildRegionByName(){
        $name = input('name','');
        $type = input('type',0);
        $where = [];
        if($type>0){
            $where[] = ['name','eq',$name];
        }
        $pid = Db::name('region')->where($where)->value('id');
        $pid = $pid?:-1;
        $region = Db::name('region')->where('pid',$pid)->select();
        gg(1,'succes',$region);
    }
}