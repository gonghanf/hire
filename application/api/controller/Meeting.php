<?php
/**
 * Author: 北川
 * Time: 2019/8/18 9:23
 * @comment　
 */

namespace app\api\controller;


use think\Db;

class Meeting extends Common
{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/11 21:02
     * @comment　招聘会列表
     */
    public function meetingList()
    {
        $where = [];
        $where[] = ['status', 'eq', 1];

        $meeting_model = new \app\common\model\Meeting();
        $list = $meeting_model->meetingList($where, $this->page, $this->pageSize);
        gg(1, 'success', $list);
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:29
     * @comment　资讯列表
     */
    public function newsList()
    {
        $keyword = input('post.keyword');
        $map = [];
        $map[] = ['open','eq',1];

        if ($keyword) {
            $map[] = ['title|editor', 'like', "%" . $keyword . "%"];
        }
        $link = Db::name('link')
            ->where($map)
            ->order('sort')
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();

        foreach ($link['data'] as $k => $v) {
            $link['data'][$k]['addtime'] = date('Y-m-d H:s', $v['addtime']);
        }
        gg(1, 'success', $link);
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:31
     * @comment　资讯详情
     */
    public function newsInfo()
    {

        $id = input('param.id');
        $info = db('link')->where(array('id' => $id))->find();
        $info['addtime'] = date('Y-m-d', $info['addtime']);
        gg(1, 'success', $info);
    }

}