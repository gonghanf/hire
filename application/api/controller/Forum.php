<?php
/**
 * Author: 北川
 * Time: 2019/8/18 9:32
 * @comment　
 */

namespace app\api\controller;


use think\Db;

class Forum extends Common
{
    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:36
     * @comment　帖子分类
     */
    public function forumCate()
    {
        $list = Db::name('forum_category')->select();
        gg(1, 'success', $list);
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　帖子列表
     */
    public function forumList()
    {
        $uid = $this->uid?:0;
        $page = input('page') ? input('page') : 1;
        $pageSize = input('limit') ? input('limit') : config('pageSize');
        $condition = [];
        $condition[] = ['a.is_hide', 'eq', 0];
        $condition[] = ['a.is_del', 'eq', 0];
        $condition[] = ['a.status', 'eq', 1];
        //$condition[] = ['is_delete','eq',0];
        if (input('title')) {
            $condition[] = ['a.title', 'like', '%' . input('title') . '%'];
        }
        if (input('cat_id')) {
            $condition[] = ['a.cat_id', 'eq', input('cat_id')];
        }
        $list = Db::name('forum')
            ->alias('a')
            //->leftJoin('forum_category c','a.cat_id = c.id')
            ->leftJoin('users d', 'a.uid = d.id')
            ->leftJoin('forum_good c','a.id=c.forum_id and c.uid='.$uid)
            ->field('a.*,d.username,c.op good')
            ->where($condition)
            ->order('a.is_top desc,a.id desc')
            ->paginate(array('list_rows' => $pageSize, 'page' => $page))
            ->toArray();
        //$sql = Db::getLastSql();
        foreach ($list['data'] as $k => $v) {
            $list['data'][$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
        }
        gg(1, 'success', $list);
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　帖子详情
     */
    public function forumInfo()
    {
        $uid = $this->uid;
        $id = input('id');
        $info = Db::name('forum')->where([['id', 'eq', $id]])->find();
        $info['good'] = Db::name('forum_good')->where([['forum_id', 'eq', $id], ['uid', 'eq', $uid],['type','eq',1]])->value('op');
        $info['create_time'] = date("Y-m-d H:i", $info['create_time']);
//        $boards = Db::name('forum_board')->select();
        if($info['uid'] == $this->uid) {
            $info['is_my'] = 1;
        } else {
            $info['is_my'] = 0;
        }
        $info['good_num'] = Db::name('forum_good')->where([['forum_id','eq',$id],['type','eq',1]])->count();
        $user = Db::name('users')->where([['id','eq',$info['uid']]])->find();
        $info['username'] = $user['username'];
        $info['content'] = str_replace("\n", "<br>", $info['content']);
        $info['content'] = str_replace(" ", "&nbsp;", $info['content']);
        gg(1, 'success', $info);
    }

    /**
     * @author 北川
     * @time 2019/8/21 23:28
     * @comment　发表编辑帖子
     */
    public function addEditForum()
    {
        $id = input('forum_id', 0);
        $data = input('post.');
        $data['update_time'] = time();
        if ($id) {
            $r = model('forum')->isUpdate(true)->allowField(['update_time', 'cat_id', 'title', 'content','img'])->save($data, ['id' => $id]);
            if ($r) {
                gg(1, '操作成功');
            } else {
                gg(0, '操作失败');
            }
        } else {
            $data['uid'] = $this->uid;
            $data['create_time'] = time();
            $r = model('forum')->isUpdate(false)->allowField(['uid', 'create_time', 'update_time', 'cat_id', 'title', 'content','img'])->save($data);
            if ($r) {
                gg(1, '发帖成功，请耐心等待后台审核');
            } else {
                gg(0, '发帖失败');
            }
        }

    }

    /**
     * @author 北川
     * @time 2019/10/13 13:33
     * @comment　删除帖子
     */
    public function delForum(){
        $id  = input('id',0);
        $r = Db::name('forum')->where('id',$id)->setField('is_del',1);
        if ($r === false){
            gg(0,'操作失败');
        }else{
            gg(1,'操作成功');
        }
    }

    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/12 22:15
     * @comment　评论列表
     */
    public function replyList()
    {

        $fid = input('forum_id', 0);
        $rid = input('reply_id', 0);

        $where = [['fid', 'eq', $fid], ['is_del', 'eq', 0]];
        if($rid > 0) {
            $where[] = ['a.pid','eq',$rid];
        } else {
            $where[] = ['a.pid','eq',0];
        }

        $list = Db::name('forum_reply')
            ->alias('a')
            ->leftJoin('users b', 'a.uid=b.id')
            ->leftJoin('forum_good c', 'a.id=c.reply_id and c.uid=' . $this->uid)
            ->field('a.*,b.username,b.sn,c.op good,b.avatar')
            ->where($where)
            ->paginate(array('list_rows' => $this->pageSize, 'page' => $this->page))
            ->toArray();
        foreach ($list['data'] as $k => $v) {
            //$list[$k]['lay_is_open']=true;
            if($v['uid'] == $this->uid) {
                $list['data'][$k]['is_my'] = 1;
            } else {
                $list['data'][$k]['is_my'] = 0;
            }
            $list['data'][$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
            $list['data'][$k]['child'] = $this->getChildReply($v['id']);
            if(!$list['data'][$k]['good_num']) {
                $list['data'][$k]['good_num'] = 0;
            }
        }
        gg(1, 'success', $list);
    }

    /**
     * @author 北川
     * @time 2019/8/18 10:02
     * @comment　获取回复评论
     */
    public function getChildReply($pid)
    {
        $list = Db::name('forum_reply')
            ->alias('a')
            ->leftJoin('users b', 'a.uid=b.id')
            ->field('a.*,b.username,b.sn')
            ->where([['a.pid', 'eq', $pid], ['is_del', 'eq', 0]])
            ->select();
        if ($list) {
            foreach ($list as $k => $v) {
                $list[$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
                $list[$k]['good'] = $v['good'] ? true : false;
                $list[$k]['child'] = $this->getChildReply($v['id']);
            }
        } else {
            $list = [];
        }
        return $list;

    }

    /**
     * @author 北川
     * @time 2019/8/18 9:49
     * @comment　评论和回复评论
     */
    public function reply()
    {
        $fid = input('forum_id', 0);
        $rid = input('reply_id', 0);
        $uid = $this->uid;
        $content = input('content');
        $indata = [
            'fid' => $fid,
            'pid' => $rid,
            'uid' => $uid,
            'content' => $content,
            'is_del' => 0,
            'create_time' => time(),
            'update_time' => time()
        ];
        $r = Db::name('forum_reply')->insert($indata);
        if ($r) {
            Db::name('forum')->where([['id', 'eq', $fid]])->setInc('reply_num', 1);
            gg(1, '评论成功');
        } else {
            gg(0, '评论失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 9:57
     * @comment　删除评论
     */
    public function delReply()
    {
        $id = input('reply_id', 0);
        $reply_info = Db::name('forum_reply')->where([['id', 'eq', $id]])->find();
        if (!$reply_info) {
            gg(0, '删除失败', 1);
        }
        $r = Db::name('forum_reply')->where([['id', 'eq', $id]])->setField('is_del', 1);
        if ($r) {
            Db::name('forum')->where([['id', 'eq', $reply_info['fid']]])->setDec('reply_num', 1);
            gg(1, '删除成功');
        } else {
            gg(0, '删除失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 16:49
     * @comment　点赞帖子
     */
    public function forumGood()
    {
        $id = input('forum_id', 0);
        $op = input('op',1);
        $indata = [
            'forum_id' => $id,
            'uid' => $this->uid,
            'op' => $op,
            'type' => 1,
            'create_time' => time(),
            'update_time' => time(),
        ];

        $has_good = Db::name('forum_good')->where([['uid', 'eq', $this->uid], ['forum_id', 'eq', $id],['type','eq',1]])->find();
        if ($has_good) {
            Db::name('forum_good')->where([['uid', 'eq', $this->uid], ['forum_id', 'eq', $id],['type','eq',1]])->delete();
            if ($has_good['op'] != $op){
                $r = Db::name('forum_good')->insert($indata);
            }else{
                $op = 0;
            }
        }else{
            $r = Db::name('forum_good')->insert($indata);
        }

        $good_num = Db::name('forum_good')->where([['forum_id','eq',$id],['type','eq',1],['op','eq',1]])->count();
        $bad_num = Db::name('forum_good')->where([['forum_id','eq',$id],['type','eq',1],['op','eq',2]])->count();
        $updata = [
            'good_num' => ($good_num?:0),
            'bad_num' => ($bad_num?:0),
        ];
        $res = Db::name('forum')->where([['id', 'eq', $id]])->update($updata);
        if ($res) {
            $updata['op'] = $op;
            gg(1, '点赞成功',$updata);
        } else {
            gg(0, '点赞失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 16:49
     * @comment　点赞评论
     */
//    public function replyGood()
//    {
//        $id = input('forum_id', 0);
//        $rid = input('reply_id', 0);
//        $op = input('op',1);
//        $has_good = Db::name('forum_good')->where([['uid', 'eq', $this->uid], ['forum_id', 'eq', $id], ['reply_id', 'eq', $rid],['type','eq',2]])->find();
//        if ($has_good) {
//            if ($has_good['op'] == $op){
//                gg(1, '点赞成功');
//            }
//            if ($has_good['op'] == 1){
//                $field = 'goods_num';
//            }else{
//                $field = 'bad_num';
//            }
//            Db::name('forum_good')->where([['uid', 'eq', $this->uid], ['forum_id', 'eq', $id], ['reply_id', 'eq', $rid],['type','eq',2]])->delete();
//
//            Db::name('forum_reply')->where([['id', 'eq', $rid]])->setDec($field, 1);
//        }
//        $indata = [
//            'forum_id' => $id,
//            'reply_id' => $rid,
//            'uid' => $this->uid,
//            'op' => $op,
//            'type' => 2,//点赞评论
//            'create_time' => time(),
//            'update_time' => time(),
//        ];
//        $r = Db::name('forum_good')->insert($indata);
//        if ($r) {
//            if ($op == '1'){
//                $field = 'good_num';
//            }else{
//                $field = 'bad_num';
//
//            }
//            Db::name('forum_reply')->where([['id', 'eq', $rid]])->setInc($field, 1);
//            gg(1, '点赞成功');
//        } else {
//            gg(0, '点赞失败');
//        }
//    }
    public function replyGood()
    {
//        $id = input('forum_id', 0);
        $rid = input('reply_id', 0);
        $fid = Db::name('forum_reply')->where('id',$rid)->value('fid');
        $op = input('op',1);
        $indata = [
            'forum_id' => $fid,
            'reply_id' => $rid,
            'uid' => $this->uid,
            'op' => $op,
            'type' => 2,
            'create_time' => time(),
            'update_time' => time(),
        ];

        $has_good = Db::name('forum_good')->where([['uid', 'eq', $this->uid], ['reply_id', 'eq', $rid],['type','eq',2]])->find();
        if ($has_good) {
            Db::name('forum_good')->where([['uid', 'eq', $this->uid], ['reply_id', 'eq', $rid],['type','eq',2]])->delete();
            if ($has_good['op'] != $op){
                $r = Db::name('forum_good')->insert($indata);
            }else{
                $op = 0;
            }
        }else{
            $r = Db::name('forum_good')->insert($indata);
        }

        $good_num = Db::name('forum_good')->where([['reply_id','eq',$rid],['type','eq',2],['op','eq',1]])->count();
        $bad_num = Db::name('forum_good')->where([['reply_id','eq',$rid],['type','eq',2],['op','eq',2]])->count();
        $updata = [
            'good_num' => ($good_num?:0),
            'bad_num' => ($bad_num?:0),
        ];
        $res = Db::name('forum_reply')->where([['id', 'eq', $rid]])->update($updata);
        if ($res) {
            $updata['op'] = $op;
            gg(1, '点赞成功',$updata);
        } else {
            gg(0, '点赞失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 16:58
     * @comment　举报帖子和评论   type = 1 举报帖子 2举报评论
     */
    public function forumComplain()
    {
        $forum_id = input('forum_id', 0);
        $reply_id = input('reply_id', 0);
        $type = input('type', 1);
        $content = input('content');
        if (!$content) {
            gg(0, '请输入举报内容');
        }

        $indata = [
            'forum_id' => $forum_id,
            'reply_id' => $reply_id,
            'uid' => $this->uid,
            'type' => $type,//点赞评论
            'content' => $content,//举报内容
            'create_time' => time(),
            'update_time' => time(),
        ];
        $r = Db::name('forum_complain')->insert($indata);
        if ($r) {
            gg(1, '举报成功');
        } else {
            gg(0, '举报失败');
        }
    }
}