<?php
/**
 * Author: 北川
 * Time: 2019/8/14 21:49
 * @comment　
 */

namespace app\api\controller;


use app\common\model\Position;
use app\common\model\Users;
use spec\Prophecy\Argument\Token\ArrayCountTokenSpec;
use think\Db;

class Hall extends Common
{
    /**
     * @author 北川
     * @time 2019/8/14 21:51
     * @comment　大厅首页
     */
    public function index()
    {
        $banner = Db::name('ad')->where([['as_id', 'eq', 1]])->select();
        $hot_position = Db::name('position')->where([['is_del', 'eq', 0], ['is_hot', 'eq', 1], ['status', 'eq', 1]])->select();
        $field = 'id,username,industry,desc';
        $hot_company = Db::name('users')->field($field)->where([['is_hot', 'eq', 1], ['status', 'eq', 1]])->select();
        $data['banner'] = $banner;
        $data['hot_position'] = $hot_position;
        $data['hot_company'] = $hot_company;
        gg(1, 'success', $data);
    }

    /**
     * @author 北川
     * @time 2019/8/14 22:47
     * @comment　所有职位
     */
    public function positionList()
    {
        $company = new Company();
        return $company->positionList();
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/14 23:21
     * @comment　职位详情
     */
    public function positionInfo()
    {
        $id = input('position_id', 0);

        $position_info = Db::name('position')->where('id', $id)->find();
        //翻译
        $tag_arr = (array)array_filter(explode(',',$position_info['tags']));
        $position_info['tags'] = array_map('lan',$tag_arr);
        $wel_arr = (array)array_filter(explode(',',$position_info['welfare']));
        $position_info['welfare'] = array_map('lan',$wel_arr);
        $qual_arr = (array)array_filter(explode(',',$position_info['qualification']));
        $position_info['qualification'] = array_map('lan',$qual_arr);
        $position_info['education'] = lan($position_info['education']);
        $position_info['experience'] = lan($position_info['experience']);
        $position_info['position_type'] = lan($position_info['position_type']);

        $position_info['desc'] = str_replace("\n", '<br>', $position_info['desc']);
        $position_info['desc'] = str_replace(" ", '&nbsp;', $position_info['desc']);

        $collectionList = Db::name('collection')->where([['uid','eq',$this->uid]])->select();
        $position_info['is_collection'] = 0;
        foreach ($collectionList as $key1 => $p) {
            if($position_info['id']== $p['target_id'] && $p['type']==2) {
                $position_info['is_collection'] = 1;
                break;
            }
        }

        $company_id = isset($position_info['company_id']) ? $position_info['company_id'] : 0;
        $field = 'avatar,contact,contact_mobile,username,scale,industry,desc,mobile';
        $company_info = Db::name('users')->field($field)->where([['id', 'eq', $company_id]])->find();
        //翻译
        $company_info['industry'] = lan($company_info['industry']);


        $company_info['desc'] = str_replace("\n", '<br>', $company_info['desc']);
        $company_info['desc'] = str_replace(" ", '&nbsp;', $company_info['desc']);
        $data['position_info'] = $position_info;
        $data['company_info'] = $company_info;
        gg(1, 'success', $data);
    }

    /**
     * @author 北川
     * @time 2019/8/14 23:22
     * @comment　投递职位
     */
    public function appPosition()
    {
        $position_id = input('position_id', 0);
        $company_id = Db::name('position')->where('id', $position_id)->value('company_id');
        $indata = [
            'uid' => $this->uid,
            'company_id' => $company_id,
            'position_id' => $position_id,
            'status' => 0,
            'is_del' => 0,
            'create_time' => time(),
            'update_time' => time()
        ];
        $r = Db::name('resume_position')->insert($indata);
        if ($r) {
            gg(1, '投递成功');
        } else {
            gg(0, '投递失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/14 23:34
     * @comment　收藏职位
     */
    public function likePosition()
    {
        $position_id = input('position_id', 0);
        $where = [
            ['uid', 'eq', $this->uid],
            ['target_id', 'eq', $position_id],
            ['type', 'eq', 2]
        ];
        $id = Db::name('collection')->where($where)->value('id');
        if ($id) {
            $r = Db::name('collection')->where('id', $id)->delete();
            $op = '取消收藏';
        } else {
            $company_id = Db::name('position')->where('id', $position_id)->value('company_id');
            $indata = [
                'uid' => $this->uid,
                'company_id' => $company_id,
                'target_id' => $position_id,
                'type' => 2,//收藏职位
                'create_time' => time(),
                'update_time' => time()
            ];
            $r = Db::name('collection')->insert($indata);
            $op = '收藏';

        }
        if ($r) {
            gg(1, '操作成功',['status'=>($id?0:1)]);
        } else {
            gg(0, '操作失败');
        }
    }


    /**
     * @author 北川
     * @time 2019/8/15 21:22
     * @comment　企业列表
     */
    public function companyList()
    {
        $where = [];
        $where[] = ['is_lock', 'eq', 0];
        $where[] = ['type', 'eq', 2];//企业
        //企业名称
        if (input('username')) {
            $where[] = ['username|b.name', 'like', '%' . input('username') . '%'];
        }
        //行业
        if (input('industry')) {
            $where[] = ['industry', 'eq', input('industry')];
        }
        if (input('scale')) {
            $where[] = ['scale', 'eq', input('scale')];
        }

        $user_model = new Users();
        $list = $user_model->companyList($where, 'distinct(a.id),avatar,mobile,email,contact,scale,contact_mobile,username,industry,a.desc,a.province,a.city', $this->page, $this->pageSize);
        foreach ($list['data'] as $key => $o) {
            $list['data'][$key]['position_count'] = Db::name('position')->where([['company_id','eq',$o['id']],['is_del','eq',0],['status','eq',1]])->count();
        }
        gg(1, 'success', $list);
    }

    /**
     * @author 北川
     * @time 2019/8/15 21:37
     * @comment　企业详情
     */
    public function companyInfo()
    {
        $id = input('id');
        $field = 'id,avatar,mobile,email,contact,scale,contact_mobile,username,industry,desc,position';
        $info = Db::name('users')->field($field)->where([['id', 'eq', $id]])->find();
        $info['industry'] = lan($info['industry']);
//        $position = Db::name('position')->where([['company_id','eq',$id],['is_del','eq',0],['status','eq',1]])->select();
//        $info['position'] = $position;
        gg(1, 'success', $info);
    }

    /**
     * @author 北川
     * @time 2019/8/14 23:34
     * @comment　收藏/取消收藏企业
     */
    public function likeCompany()
    {
        $company_id = input('company_id', 0);
        $where = [
            ['uid', 'eq', $this->uid],
            ['target_id', 'eq', $company_id],
            ['type', 'eq', 1]
        ];
        $id = Db::name('collection')->where($where)->value('id');
        if ($id) {
            $r = Db::name('collection')->where('id', $id)->delete();
            $op = '取消收藏';
        } else {
            $indata = [
                'uid' => $this->uid,
                'company_id' => $company_id,
                'target_id' => $company_id,
                'type' => 1,//收藏企业
                'create_time' => time(),
                'update_time' => time()
            ];
            $r = Db::name('collection')->insert($indata);
            $op = '收藏';
        }
        if ($r) {
            gg(1, $op . '成功');
        } else {
            gg(0, $op . '失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/15 21:57
     * @comment　企业上线职位
     */
    public function companyPosition()
    {
        $company_id = input('company_id', 0);
        $where = [];
        $where[] = ['company_id', 'eq', $company_id];
        $where[] = ['a.is_del', 'eq', 0];
        $where[] = ['a.status', 'eq', 1];
        $position_model = new Position();
        $list = $position_model->positionList($where, $this->page, $this->pageSize);
        $collectionList = Db::name('collection')->where([['uid','eq',$this->uid]])->select();

        foreach ($list['data'] as $key => $o) {
            $list['data'][$key]['is_collection'] = 0;
            foreach ($collectionList as $key1 => $p) {
                if($o['id']== $p['target_id'] && $p['type']==2) {
                    $list['data'][$key]['is_collection'] = 1;
                    break;
                }
            }
        }
        gg(1, 'success', $list);
    }


    /**
     * @author 北川
     * @time 2019/8/15 21:22
     * @comment　求职者列表
     */
    public function userList()
    {
        $where = [];
        $where[] = ['is_lock', 'eq', 0];
        $where[] = ['type', 'eq', 1];//求职者

        $match = input('match',false);
        $field = '';
        $order = 'a.id desc';
        if ($match){
            $position_info = Db::name('position')->where('id',$match)->find();
            if ($position_info['city']){
                $field .= "(case when b.city = '{$position_info['city']}' then 3 else 0 end) as w1,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w1,";
            }

            if ($position_info['start_date']){
                $field .= "(case when b.start_date >= '{$position_info['start_date']}' then 3 else 0 end) as w2,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w2,";
            }

            if ($position_info['position_type']){
                $field .= "(case when b.position_type = '{$position_info['position_type']}' then 3 else 0 end) as w3,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w3,";
            }

            if ($position_info['salary_min']){
                $field .= "(case when b.salary_min <= '{$position_info['salary_max']}' then 2 else 0 end) as w4,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w4,";
            }

            $education_arr = Db::name('web_config')->where([['type','eq','education']])->order('sort asc')->column('name');
            $edu_key = array_search($position_info['education'],$education_arr);
            if ($edu_key !== false){
                $edu_str = implode("','",array_slice($education_arr,$edu_key));
                $field .= "(case when b.education in ('{$edu_str}') then 2 else 0 end) as w5,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w5,";
            }

//            $apartment_arr = ['无','租房补贴','免费'];
            $apartment_arr = ['免费','租房补贴','无'];
            $apt_key = array_search($position_info['apartment'],$apartment_arr);
            if ($apt_key !== false){
                $apt_str = implode("','",array_slice($apartment_arr,$apt_key));
                $field .= "(case when b.apartment in ('{$apt_str}') then 2 else 0 end) as w6,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w6,";
            }

            if ($position_info['experience']){
                $field .= "(case when b.experience <= ('{$position_info['experience']}') then 2 else 0 end) as w7,";

            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w7,";
            }

            if ($position_info['job_form']){
                $field .= "(case when b.job_form = ('{$position_info['job_form']}') then 2 else 0 end) as w8,";
            }else{
                $field .= "(case when b.id > 0 then 1 else 0 end) as w8,";
            }
            $order = '(w1+w2+w3+w4+w5+w6+w7+w8) desc';
        }
        $field .= 'a.id,country,username,sex,avatar,mobile,email,salary,salary_min,salary_max,b.province,b.city,b.district,b.full_address,b.experience,b.education,b.position_type';


        //关键字
        if (input('keyword')){
            $where[] = ['username|tags','like','%'.input('keyword').'%'];
        }
        //求职者名称
        if (input('username')) {
            $where[] = ['username', 'like', '%' . input('username') . '%'];
        }
        //经验
        if (input('experience')) {
            $where[] = ['experience', 'eq', input('experience')];
        }
        //学历
        if (input('education')) {
            $where[] = ['education', 'eq', input('education')];
        }
        //职位类型
        if (input('position_type')) {
            $where[] = ['position_type', 'eq', input('position_type')];
        }
        //薪资
        if (input('salary_min')) {
            $where[] = ['salary_min', 'egt', input('salary_min')];
        }
        if (input('salary_max')) {
            $where[] = ['salary_max', 'elt', input('salary_max')];
        }
        if (input('province')) {
            $where[] = ['province', 'eq', input('province')];
        }
        if (input('city')) {
            $where[] = ['city', 'eq', input('city')];
        }
        if (input('start_date')) {
            $where[] = ['start_date', 'elt', input('start_date')];
        }
        if (input('apartment')) {
            $where[] = ['apartment', 'eq', input('apartment')];
        }
        if (input('job_form')) {
            $where[] = ['job_form', 'eq', input('job_form')];
        }
        if (input('qualification')) {
            $where[] = ['qualification', 'eq', input('qualification')];
        }

        $list = Db::name('users')
            ->alias('a')
            ->leftJoin('resume b', 'a.id=b.uid')
//            ->field('a.id,username,avatar,mobile,email,salary,salary_min,salary_max,b.province,b.city,b.district,b.full_address,b.experience,b.education,b.position_type')
            ->field($field)
            ->where($where)
            ->orderRaw($order)
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();
        $collectionList = Db::name('collection')->where([['uid','eq',$this->uid],['type','eq',3]])->select();
        $sex_arr = [
            '男' => 'Man',
            '女' => 'Woman'
        ];
        foreach ($list['data'] as $key => $o) {
//            $o['age_trans'] = $o['age'].((session('language') == 'en')?'yeas old':'岁');
            $o['sex_trans'] = ((session('language') == 'en')?$sex_arr[$o['sex']]:$o['sex']);
            $o['education'] = lan($o['education']);
            $o['experience'] = lan($o['experience']);
            $o['position_type'] = lan($o['position_type']);
            $list['data'][$key] = $o;
            $list['data'][$key]['is_collection'] = 0;
            foreach ($collectionList as $key1 => $p) {
                if($o['id']== $p['target_id'] && $p['type']==3) {
                    $list['data'][$key]['is_collection'] = 1;
                    break;
                }
            }
        }
        gg(1, 'success', $list);

    }

    /**
     * @author 北川
     * @time 2019/8/25 14:04
     * @comment　配置参数列表
     */
    public function configList()
    {
        $config = input('config', '');
        $config_arr = explode(',', $config);
        $data = [];
        if (in_array('education', $config_arr)) {
            $data['education'] = Db::name('web_config')->where([['type', 'eq', 'education']])->order('sort')->select();
        }
        if (in_array('tags', $config_arr)) {
            $data['tags'] = Db::name('web_config')->where([['type', 'eq', 'tags']])->order('sort')->select();
        }
        if (in_array('industry', $config_arr)) {
            $data['industry'] = Db::name('web_config')->where([['type', 'eq', 'industry']])->order('sort')->select();
        }
        if (in_array('position_type', $config_arr)) {
            $data['position_type'] = Db::name('web_config')->where([['type', 'eq', 'position_type']])->order('sort')->select();
        }
        if (in_array('experience', $config_arr)) {
            $data['experience'] = Db::name('web_config')->where([['type', 'eq', 'experience']])->order('sort')->select();
        }
        if (in_array('welfare', $config_arr)) {
            $data['welfare'] = Db::name('web_config')->where([['type', 'eq', 'welfare']])->order('sort')->select();
        }
        if (in_array('scale', $config_arr)) {
            $data['scale'] = Db::name('web_config')->where([['type', 'eq', 'scale']])->order('sort')->select();
        }
        gg(1, 'success', $data);

    }


    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/16 23:14
     * @comment　人才详情
     */
    public function userInfo()
    {
        $id = input('user_id', 0);
        $user_model = new \app\common\model\Users();
        $info = $user_model->getDetail($id);
        $info['tags'] = $info['tags_arr'];
        $company_id = $this->uid;
        //非会员企业,未解锁该用户手机号则隐藏用户手机号
        $user_level = Db::name('users')->where('id',$this->uid)->value('level');
        if ($user_level == 1) {
            $is_release = Db::name('mobile_release')->where([['company_id', 'eq', $company_id], ['uid', 'eq', $id],['status','eq',1]])->find();
            if (!$is_release) {
                $info['mobile'] = hide_mobile($info['mobile']);
            }
        }
        $collectionList = Db::name('collection')->where([['uid','eq',$this->uid],['type','eq',3]])->select();
        $info['is_collection'] = 0;
        foreach ($collectionList as $key1 => $p) {
            if($info['id']== $p['target_id'] && $p['type']==3) {
                $info['is_collection'] = 1;
                break;
            }
        }
        gg($info);
    }

    public function visaService() {
        $page = input('page') ? input('page') : 1;
        $pageSize = input('limit') ? input('limit') : config('pageSize');
        $where = [];
        $where[] = ['is_del', 'eq', 0];
        //$condition[] = ['is_delete','eq',0];
        if (input('title')) {
            $where[] = ['title', 'like', '%' . input('title') . '%'];
        }
        $list = Db::name('service')
            ->where($where)
            ->order('create_time desc')
            ->paginate(array('list_rows' => $pageSize, 'page' => $page))
            ->toArray();
        //$sql = Db::getLastSql();
        foreach ($list['data'] as $k => $v) {
            $list['data'][$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
        }
        gg(1, 'success', $list);
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:29
     * @comment　签证资讯列表
     */
    public function visaList()
    {
        $keyword = input('post.keyword');
        $map = [];
        $map[] = ['open','eq',1];
        if ($keyword) {
            $map[] = ['title|editor', 'like', "%" . $keyword . "%"];
        }
        $link = Db::name('visa')
            ->where($map)
            ->order('sort')
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();

        foreach ($link['data'] as $k => $v) {
            $link['data'][$k]['addtime'] = date('Y-m-d H:s', $v['addtime']);
        }
        gg(1, 'success', $link);
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:31
     * @comment　证资讯详情
     */
    public function visaInfo()
    {
        $id = input('param.id');
        $info = db('visa')->where(array('id' => $id))->find();
        $info['addtime'] = date('Y-m-d', $info['addtime']);
        gg(1, 'success', $info);
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:29
     * @comment　下载表格列表
     */
    public function chartList()
    {
        $keyword = input('post.keyword');
        $map = [];
        $map[] = ['open','eq',1];
        if ($keyword) {
            $map[] = ['title', 'like', "%" . $keyword . "%"];
        }
        $link = Db::name('chart')
            ->where($map)
            ->order('sort')
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();

        foreach ($link['data'] as $k => $v) {

            $link['data'][$k]['addtime'] = date('Y-m-d H:s', $v['addtime']);
        }
        gg(1, 'success', $link);
    }

}