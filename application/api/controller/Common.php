<?php

namespace app\api\controller;

use app\common\controller\GG;
use think\Db;
use clt\Leftnav;
use think\Controller;
use EasyWeChat\Factory;

class Common extends GG
{
    protected $bindMobile, $isLogin, $uid, $username, $user_avatar, $user_type, $user_sn, $page, $pageSize;

    public function initialize()
    {
        // session('uid', 8);
        if (!session('uid')) {
            //gg(-9, '请先登录');
        }
        $this->uid = session('uid')?:0;
        $this->user_type = session('user_type');
        $this->username = session('user_name');
        $this->user_avatar = session('user_avatar');
        $this->user_sn = session('user_sn');
        $this->page = input('page') ?: 1;
        $this->pageSize = input('limit') ?: config('app.pageSize');
//        $this->assign('configLan',config('zh_en.'));
//         if (input('token')){
//             $token = input('token');
//             $user = Db::name('users')->where([['token','eq',$token]])->find();
//             if ($user){
//                 if ($user['is_lock'] == 1){
//                     gg(-9,'您的账号已被禁用，请联系管理员解禁');
//                 }
//                 $this->isLogin = true;
//                 $this->uid = $user['id'];
//                 $this->username = $user['username'];
//                 $this->avatar = $user['avatar'];
//                 $this->page = input('page')?:1;
//                 $this->pageSize = input('limit')?:config('app.pageSize');
//
//             }else{
//                 gg(-9,'数据异常，请重新登录');
//             }
//
//         }else{
//             gg(-9,'数据异常，请重新登录');
//         }

    }

    //空操作
    public function _empty()
    {
        return $this->error('空操作，返回上次访问页面中...');
    }
}
