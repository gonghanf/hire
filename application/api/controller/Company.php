<?php
/**
 * Author: 北川
 * Time: 2019/8/15 22:46
 * @comment　
 */

namespace app\api\controller;


use alipay\Pagepay;
use alipay\Wappay;
use app\admin\controller\Config;
use app\common\model\Position;
use app\common\model\ResumePosition;
use app\common\model\Users;
use EasyWeChat\Factory;
use think\Db;
use Yansongda\Pay\Pay;

class Company extends Common
{
    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:17
     * @comment　企业中心
     */
    public function companyCenter()
    {
        $uid = $this->uid;
        $user_info = Db::name('users')->where([['id', 'eq', $uid]])->find();
        $resume_num = Db::name('resume_position')->where([['company_id', 'eq', $uid], ['is_del', 'eq', 0]])->count();
        $collection_num = Db::name('collection')->where([['uid', 'eq', $uid]])->count();
        $user_info['resume_num'] = $resume_num;//收到的简历
        $user_info['collection_num'] = $collection_num;//收藏数量
        $user_info['reg_time'] = date('Y-m-d H:i', $user_info['reg_time']);
        $user_info['level_text'] = Db::name('user_level')->where([['level_id', 'eq', $user_info['level']]])->value('level_name');
        $user_info['avatar'] = add_domain($user_info['avatar']);
        gg($user_info);
    }

    /**
     * @author 北川
     * @time 2019/6/19 23:14
     * @comment　企业信息详情
     */
    public function companyInfo()
    {
        $uid = $this->uid;
        $user_info = Db::name('users')->where([['id', 'eq', $uid]])->find();
        $user_info['reg_time'] = date('Y-m-d H:i', $user_info['reg_time']);
        $user_info['level_text'] = Db::name('user_level')->where([['level_id', 'eq', $user_info['level']]])->value('level_name');
        $user_info['avatar'] = add_domain($user_info['avatar']);
        gg($user_info);

    }

    /**
     * @author 北川
     * @time 2019/8/18 9:18
     * @comment　修改企业信息
     */
    public function editCompanyInfo()
    {
        $data = input('post.');
        unset($data['file']);

        $user = new Users();
        $r = $user->isUpdate(true)->allowField(true)->save($data, ['id' => $this->uid]);
        if ($r) {
            if ($data['username'] && $data['industry'] && $data['contact'] && $data['position'] && $data['contact_mobile'] && $data['recruiter_type']){
                session('verify',1);
                if ($user::value('verify') == 0){
                    Db::name('users')->where('id',$this->uid)->setField('verify',1);
                }
            }
            gg(1, '修改成功');

        }else {
            gg(0, '修改失败');
        }
    }


    /**
     * @author 北川
     * @time 2019/8/13 23:26
     * @comment　上传营业执照
     */
    public function editLicence()
    {
        $id = $this->uid;
        $licence = input('licence');
        if (!$licence) {
            gg(0, '请上传营业执照');
        }
        $r = Db::name('users')->where([['id', 'eq', $id]])->update(['card1' => $licence, 'update_time' => time()]);
        if ($r) {
            gg(1, '操作成功');
        } else {
            gg(0, '操作失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/14 22:47
     * @comment　职位列表
     */
    public function positionList()
    {
        $where = [];
        $company_id = input('post.company_id');
        if ($company_id == 'self'){
            $where[] = ['a.company_id', 'eq', $this->uid];
        }elseif ($company_id != ''){
            $where[] = ['a.company_id', 'eq', $company_id];
            $where[] = ['a.verify','eq',1];
        }else{
            $where[] = ['a.verify','eq',1];
        }
        $match = input('match',0);
        $field = '';
        $order = 'a.id desc';
        if ($match){
            $user_model = new \app\common\model\Users();
            $user_info = $user_model->getDetail($this->uid);
            if ($user_info['city']){
                $field .= "(case when a.city = '{$user_info['city']}' then 3 else 0 end) as w1,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w1,";
            }

            if ($user_info['start_date']){
                $field .= "(case when start_date >= '{$user_info['start_date']}' then 3 else 0 end) as w2,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w2,";
            }

            if ($user_info['position_type']){
                $field .= "(case when position_type = '{$user_info['position_type']}' then 3 else 0 end) as w3,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w3,";
            }

            if ($user_info['salary_min']){
                $field .= "(case when salary_max >= '{$user_info['salary_min']}' then 2 else 0 end) as w4,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w4,";
            }

            $education_arr = Db::name('web_config')->where([['type','eq','education']])->order('sort desc')->column('name');
            $edu_key = array_search($user_info['education'],$education_arr);
            if ($edu_key !== false){
                $edu_str = implode("','",array_slice($education_arr,$edu_key));
                $field .= "(case when education in ('{$edu_str}') then 2 else 0 end) as w5,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w5,";
            }

            $apartment_arr = ['无','租房补贴','免费'];
            $apt_key = array_search($user_info['apartment'],$apartment_arr);
            if ($apt_key !== false){
                $apt_str = implode("','",array_slice($apartment_arr,$apt_key));
                $field .= "(case when apartment in ('{$apt_str}') then 2 else 0 end) as w6,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w6,";
            }

            if ($user_info['experience']){
                $field .= "(case when experience <= ('{$user_info['experience']}') then 2 else 0 end) as w7,";

            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w7,";
            }

            if ($user_info['job_form']){
                $field .= "(case when job_form = ('{$user_info['job_form']}') then 2 else 0 end) as w8,";
            }else{
                $field .= "(case when a.id > 0 then 1 else 0 end) as w8,";
            }
            $order = '(w1+w2+w3+w4+w5+w6+w7+w8) desc';
        }
        $field .= 'a.*,count(c.id) app_num';
        //职位名称
        if (input('keyword')) {
            $where[] = ['a.name|b.username', 'like', '%' . input('keyword') . '%'];
        }
        //经验
        if (input('experience')) {
            $where[] = ['experience', 'eq', input('experience')];
        }
        //学历
        if (input('education')) {
            $where[] = ['education', 'eq', input('education')];
        }
        //职位类型
        if (input('position_type')) {
            $where[] = ['position_type', 'eq', input('position_type')];
        }
        //薪资
        if (input('salary_min')) {
            $where[] = ['salary_min', 'egt', input('salary_min')];
        }
        if (input('salary_max')) {
            $where[] = ['salary_max', 'elt', input('salary_max')];
        }
        if (input('province')) {
            $where[] = ['a.province', 'eq', input('province')];
        }
        if (input('city')) {
            $where[] = ['a.city', 'eq', input('city')];
        }
        if (input('start_date')) {
            $where[] = ['a.start_date', 'elt', input('start_date')];
        }
        if (input('apartment')) {
            $where[] = ['a.apartment', 'eq', input('apartment')];
        }
        if (input('insurance')) {
            $where[] = ['a.insurance', 'like', '%'.input('insurance').'%'];
        }
        if (input('job_form')) {
            $where[] = ['a.job_form', 'eq', input('job_form')];
        }
        if (input('qualification')) {
            $where[] = ['a.qualification', 'eq', input('qualification')];
        }
        $position_model = new Position();
        $list = $position_model->positionList($where, $this->page, $this->pageSize,$order,$field);
        $collectionList = Db::name('collection')->where([['uid','eq',$this->uid]])->select();

        foreach ($list['data'] as $key => $o) {
            $list['data'][$key]['is_collection'] = 0;
            foreach ($collectionList as $key1 => $p) {
                if($o['id']== $p['target_id'] && $p['type']==2) {
                    $list['data'][$key]['is_collection'] = 1;
                    break;
                }
            }
        }
        gg(1, 'success', $list);
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/15 23:04
     * @comment　修改发布职位  带id上传则为更新 ,否则为新增
     */
    public function addEditPosition()
    {
        $data = input('post.');
        $data['company_id'] = $this->uid;
        $data['salary'] = $data['salary_min'] . 'k-' . $data['salary_max'] . 'k';
        $data['full_address'] = $data['province'] . ' ' . $data['city'] . ' ' . $data['district'];
        if (is_array($data['tags'])) {
            $data['tags'] = implode(',', $data['tags']);
        }
        if (is_array($data['welfare'])) {
            $data['welfare'] = implode(',', $data['welfare']);
        }
        $data['update_time'] = time();
        if (!isset($data['id'])) {
            $data['create_time'] = time();
            $r = model('position')->isUpdate(false)->allowField(true)->save($data);
            if ($r) {
                gg(1, '操作成功，请耐心等待后台审核');
            } else {
                gg(0, '操作失败');
            }
        } else {
            $r = model('position')->isUpdate(true)->allowField(true)->save($data);
            if ($r) {
                gg(1, '操作成功');
            } else {
                gg(0, '操作失败');
            }
        }


    }

    /**
     * @author 北川
     * @time 2019/8/14 23:34
     * @comment　收藏/取消收藏人才
     */
    public function likeUser()
    {
        $user_id = input('user_id', 0);
        $where = [
            ['uid', 'eq', $this->uid],
            ['target_id', 'eq', $user_id],
            ['type', 'eq', 3]
        ];
        $id = Db::name('collection')->where($where)->value('id');
        if ($id) {
            $r = Db::name('collection')->where('id', $id)->delete();
            $op = '取消收藏';
        } else {
            $indata = [
                'uid' => $this->uid,
                'target_id' => $user_id,
                'type' => 3,//收藏人才
                'create_time' => time(),
                'update_time' => time()
            ];
            $r = Db::name('collection')->insert($indata);
            $op = '收藏';
        }
        if ($r) {
            gg(1, $op . '成功');
        } else {
            gg(0, $op . '失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/5 23:43
     * @comment　简历库
     */
    public function resumeRepository()
    {
        //$company = input('id');
        $company = $this->uid;
        $where = [];
        $where[] = ['a.company_id', 'eq', $company];
        if (input("key")) {
            $where[] = ['c.username|c.mobile|c.email|c.sn', 'like', '%' . input('key') . '%'];
        }
        $resume_po = new ResumePosition();
        $list = $resume_po->getResumePosition($where, $this->page, $this->pageSize);
        gg(1, 'success', $list);
    }

    /**
     * @author 北川
     * @time 2019/10/13 13:23
     * @comment　简历有意向无意向
     */
    public function dealPositionApply(){
       $like = input('like');
       $id = input('id');
       if (!$like || !$id){
           gg(0,'操作失败','参数异常');
       }
       $r = Db::name('resume_position')->where([['id','eq',$id],['company_id','eq',$this->uid]])->update(['like'=>$like,'status'=>2,'update_time'=>time()]);
       if ($r === false){
           gg(0,'操作失败');
       }else{
           gg(0,'操作成功');
       }
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:35
     * @comment　企业收藏列表
     */
    public function myCollection()
    {
        $uid = $this->uid;
        $where = [];
        $where[] = ['a.uid', 'eq', $uid];
        $where[] = ['a.type', 'eq', 3];//收藏用户
        $field = 'b.id user_id,b.avatar,b.username,b.mobile,b.email,b.is_lock,c.*,c.id resume_id,a.id collection_id,a.create_time';

        $list = Db::name('collection')
            ->alias('a')
            ->leftJoin('users b', 'a.target_id = b.id and a.type=3')
            ->leftJoin('resume c', 'a.target_id = c.uid')
            ->field($field)
            ->where($where)
            ->order('a.id desc')
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();
        foreach ($list['data'] as $k => $v) {
            $v['create_time'] = date('Y-m-d H:i', $v['create_time']);
            $list['data'][$k] = $v;
        }
        gg(1, 'success', $list);
    }

    /**
     * @author 北川
     * @time 2019/8/25 10:20
     * @comment　消费记录
     */
    public function orderList()
    {
        $company_id = $this->uid;
        $where = [];
        $where[] = ['uid', 'eq', $company_id];
        $where[] = ['is_del', 'eq', 0];
        $where[] = ['status', 'eq', 1];//已支付
        $order_model = new \app\common\model\Order();
        $list = $order_model->orderList($where,$this->page,$this->pageSize);
        gg(1, 'success', $list);
    }

    /**
     * @author 北川
     * @time 2019/8/25 10:22
     * @comment　充值和解锁手机号购买
     */
    public function buy()
    {
        $pay_type = input('pay_type');
        $user_id = input('user_id',0);
        $open_id = Db::name('users')->where('id', $this->uid)->value('openid');
        $goods_id = input('goods_id', 0);
        $goods_info = Db::name('goods')->where('id', $goods_id)->find();
        if (!$goods_info) {
            gg(0, '数据异常');
        }
        //生成订单
        $order_data = [
            'sn' => create_sn(),
            'goods_id' => $goods_info['id'],
            'goods_name' => $goods_info['name'],
            'price' => $goods_info['price'],
            'USD_price' => $goods_info['USD_price'],
            'num' => 1,
            'goods_amount' => $goods_info['price'] * 1,
            'USD_amount' => $goods_info['USD_price'] * 1,
            'uid' => $this->uid,
            'username' => $this->username,
            'user_sn' => $this->user_sn,
            'status' => 0,
            'is_del' => 0,
            'create_time' => time(),
            'update_time' => time(),
        ];
        $res = Db::name('order')->insertGetId($order_data);
        if (!$res) {
            gg(0, '订单生成失败');
        }
        //如果是购买单次查看会员联系方式
        if ($goods_id == 1){
            $re_data = [
                'order_id' => $res,
                'company_id' => $this->uid,
                'uid' => $user_id,
                'status' => 0,
                'create_time' => time(),
                'update_time' => time(),
            ];
            Db::name('mobile_release')->insert($re_data);
        }
        $order_data['id'] = $res;
        //支付
        return $this->orderPay($pay_type, $order_data, $open_id);

    }

    /**
     * @param $pay_type
     * @param $order_data
     * @param string $openid
     * @author 北川
     * @time 2019/8/25 11:22
     * @comment　支付接口
     */
    public function orderPay($pay_type, $order_data, $openid = '')
    {

        if ($pay_type == 'ali_wap') {
            $config = config('alipay.');
            $config['notify_url'] =  request()->domain() . '/api/pay_notify/aliNotify';
            $config['return_url'] =  request()->domain() . '/mobile/company/companyCenter';
            $param = [
                'subject' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_amount' => $order_data['goods_amount']
            ];
            $alipay = Pay::alipay($config)->wap($param);

            return $alipay->send();// laravel 框架中请直接 `return $alipay`

        } elseif ($pay_type == 'ali_pc') {
            $config = config('alipay.');
            $config['notify_url'] =  request()->domain() . '/api/pay_notify/aliNotify';
            $config['return_url'] =  request()->domain() . '/home/company/companyCenter';
            $param = [
                'subject' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_amount' => $order_data['goods_amount']
            ];
            $alipay = Pay::alipay($config)->web($param);

            return $alipay->send();// laravel 框架中请直接 `return $alipay`

        } elseif ($pay_type == 'wx_jsapi') {
            $config = config('wechat.');
            $config['notify_url'] =  request()->domain() . '/api/pay_notify/wxNotify';
            $config['return_url'] =  request()->domain() . '/mobile/company/companyCenter';
            $param = [
                'body' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_fee' => $order_data['goods_amount']*100,
//                'openid' => 'onkVf1FjWS5SBIixxxxxxx',

            ];

            $pay = Pay::wechat($config)->mp($param);
        } elseif ($pay_type == 'wx_pc') {
            $config = config('wechat.');
            $config['notify_url'] =  request()->domain() . '/api/pay_notify/wxNotify';
            $config['return_url'] =  request()->domain() . '/home/company/companyCenter';
            $param = [
                'body' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_fee' => $order_data['goods_amount']*100,
//                'openid' => 'onkVf1FjWS5SBIixxxxxxx',

            ];
            $pay = Pay::wechat($config)->scan($param);
//            $pay = Pay::wechat($config)->scan($order);
            $this->assign('url', urlencode($pay['code_url']));
            $this->assign(['id' => $order_data['id'], 'money' =>$order_data['goods_amount'],'return_url'=>$config['return_url']]);
            return view('wxpay');
        }elseif ($pay_type == 'paypal'){
            gg(1,'success',['url'=>request()->domain().url('api/company/paypalPay','sn='.$order_data['sn'])]);
        }elseif ($pay_type == 'wx_wap') {
            $config = config('wechat.');
            $config['notify_url'] =  request()->domain() . '/api/pay_notify/wxNotify';
            $config['return_url'] =  request()->domain() . '/mobile/company/companyCenter';
            $param = [
                'body' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_fee' => $order_data['goods_amount']*100,
//                'openid' => 'onkVf1FjWS5SBIixxxxxxx',

            ];

            $pay = Pay::wechat($config)->wap($param);

            //return $alipay->send();// laravel 框架中请直接 `return $alipay`

        }else {
            gg(0, '支付方式异常');
        }
    }

    /***
     * 微信支付
     * @param $order_id  订单ID
     * @param $order_sn  订单编号
     * @param $money  订单金额
     * @return mixed
     */

    public function payStatus()
    {
        $domain=\request()->domain();
        $id = input('post.id');
        $status = model('order')->where(['id' => $id])->value('status');
        if ($status == 1) {
            return json(array('code'=>1,'msg'=>"支付成功"));
        } else {
            return json(array('code'=>0,'msg'=>"未完成支付"));
        }
    }

    /**
     * @author 北川
     * @time 2019/9/16 22:03
     * @comment　跳转paypal支付页
     */
    public function paypalPay(){
        $order_info = Db::name('order')->where('sn',input('sn',-1))->find();
        $this->assign('info',$order_info);
        $config = config('paypal.');
        $config['return_url'] = request()->domain().'';
        $config['notify_url'] = request()->domain().url('api/payNorify/paypalNotify');
        $config['cancel_return'] = request()->domain().'';
        $this->assign('config',$config);
        return view();
    }

    /**
     * @author 北川
     * @time 2019/10/25 13:47
     * @comment　上线下线职位
     */
    public function changePositionStatus(){
        $id = input('id',0);
        $info = Db::name('position')->where([['id','eq',$id]])->find();
        if (!$info){
            gg(0,'数据异常');
        }
        if ($info['company_id'] != $this->uid){
            gg(0,'数据异常','只能修改自己的职位');
        }
        $up_status = ($info['status'] == 1)?2:1;
        $re = Db::name('position')->where([['id','eq',$id]])->setField('status',$up_status);
        if ($re === false){
            gg(0,'操作失败');
        }else{
            gg(1,'操作成功',['status'=>$up_status]);
        }

    }

    public function visaService() {
        $page = input('page') ? input('page') : 1;
        $pageSize = input('limit') ? input('limit') : config('pageSize');
        $where = [['uid','eq',$this->uid]];
        $where[] = ['is_del', 'eq', 0];
        //$condition[] = ['is_delete','eq',0];
        if (input('title')) {
            $where[] = ['title', 'like', '%' . input('title') . '%'];
        }
        $list = Db::name('service')
            ->where($where)
            ->order('create_time desc')
            ->paginate(array('list_rows' => $pageSize, 'page' => $page))
            ->toArray();
        //$sql = Db::getLastSql();
        foreach ($list['data'] as $k => $v) {
            $list['data'][$k]['create_time'] = date('Y-m-d H:i', $v['create_time']);
            $list['data'][$k]['username'] = $this->username;
        }
        gg(1, 'success', $list);
    }

    public function addEditService() {
        $id = input('id', 0);
        $data = input('post.');
        $data['update_time'] = time();
        if ($id) {
            $r = Db::name('service')->isUpdate(true)->allowField(['update_time', 'title', 'content'])->save($data, ['id' => $id]);
        } else {
            $data['uid'] = $this->uid;
            $data['create_time'] = time();
            $r = Db::name('service')->insert($data);
        }
        if ($r) {
            gg(1, '操作成功');
        } else {
            gg(0, '操作失败');
        }
    }

    public function deleteService() {
        $id = input('id', 0);

        $where = [['id','eq',$id],['uid','eq',$this->uid]];
        $r = Db::name('service')->where($where)->delete();
        
        if ($r) {
            gg(1, '操作成功');
        } else {
            gg(0, '操作失败');
        }
    }
}