<?php
/**
 * Author: 北川
 * Time: 2019/2/13 8:44
 *
 */

namespace app\api\controller;

use alipay\Wappay;
use alipay\Pagepay;
use alipay\Notify;
use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order;
use think\Controller;
use think\Db;
use Yansongda\Pay\Pay;

class PayNotify extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @author 北川
     * @time 2019/3/16 8:56
     * @comment 微信支付回调
     */
    public function wxNotify_old()
    {
        $options = config('wechat');
        //Db::name('aaa')->insert(['value'=>json_encode($options)]);
        $app = new Application($options);
        $response = $app->payment->handleNotify(function ($notify, $successful) {
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order_info = Db::name('order')->where(['sn' => $notify->out_trade_no])->find();
            if ($successful) {
                //订单逻辑处理
                return $this->handleSuccessLogistics($order_info);
            } else {
                return false;
            }
        });
        return $response;
    }
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @author 北川
     * @time 2019/3/16 8:56
     * @comment 微信支付回调
     */
    public function wxNotify(){
        $config = config('wechat.');
        $pay = Pay::wechat($config);

        try{
            $data = $pay->verify(); // 是的，验签就这么简单！
            if ($data['return_code'] == 'SUCCESS'){
                $order_info = Db::name('order')->where(['sn' => $data['out_trade_no']])->find();
                if (!$order_info) {
                    echo 'fail1';
                }
                //订单逻辑处理
                return $this->handleSuccessLogistics($order_info);
            }else {
                echo "fail";
            }
//            Log::debug('Wechat notify', $data->all());
        } catch (\Exception $e) {
            // $e->getMessage();
        }

        return $pay->success()->send();
    }

    /**
     * @author 北川
     * @time 2019/3/16 8:57
     * @comment 阿里支付回调
     */
    public function aliNotify()
    {
        $alipay = Pay::alipay(config('alipay.'));

        $data = $alipay->verify(); // 是的，验签就这么简单！
        //$data['out_trade_no'] = '19101621435110041';
        //Db::name('coupon')->insert(['content'=>json_encode($data),'name'=>date('Y-m-d H:i:s',time())]);
        //$data['out_trade_no'] = '2019031850985653';
        //Log::write($_POST,'notice');
        //Db::name('aaa')->insert(['value'=>json_encode($data)]);

        if ($data->trade_status == 'TRADE_SUCCESS') {//如果支付成功
            $order_info = Db::name('order')->where(['sn' => $data->out_trade_no])->find();
            if (!$order_info) {
                echo 'fail1';
            }
            //订单逻辑处理
            $re = $this->handleSuccessLogistics($order_info);
            if ($re) {
                echo "success";
            } else {
                echo 'fail2';
            }
        } else {
            echo 'fail3';
        }
    }

    /**
     * @param $order_info
     * @return bool
     * @author 北川
     * @time 2019/3/16 8:57
     * @comment 支付成功需要处理的逻辑
     */
    public function handleSuccessLogistics($order_info )
    {
        //$order_info = Db::name('order')->where('sn','19101609031258164')->find();
        if (empty($order_info)) {
            return false;
        }
        // 检查订单是否已经更新过支付状态
        if ($order_info['status'] == 1) {
            return true; // 已经支付成功了就不再更新了
        }
        Db::startTrans();
        try {
            //===============修改订单状态===========================//
            // 不是已经支付状态则修改为已经支付状态
            $sdata = [
                'status' => 1,//已支付
                'pay_time' => time(),
                'update_time' => time()
            ];
            //更新订单状态
            $re = Db::name('order')->where(['id' => $order_info['id']])->update($sdata);
            if (!$re) {
                Db::rollback();
                return false;
            }
            $user_vip_endtime = Db::name('users')->where('id',$order_info['uid'])->value('vip_end');

            $base_time = ($user_vip_endtime<time())?time():$user_vip_endtime;
            //单次购买用户手机号解锁
            if ($order_info['goods_id'] == 1){
                $r = Db::name('mobile_release')->where('order_id',$order_info['id'])->setField('status',1);

            }elseif ($order_info['goods_id'] == 2){//月卡  用户升级
                $updata = [
                    'level' => 2,
//                    'vip_start' => time(),
                    'vip_end' => strtotime('+1 month',$base_time),
                    'update_time' => time()
                ];
                $r = Db::name('users')->where('id',$order_info['uid'])->update($updata);
            }elseif ($order_info['goods_id'] == 3){//季卡卡
                $updata = [
                    'level' => 2,
//                    'vip_start' => time(),
                    'vip_end' => strtotime('+3 month',$base_time),
                    'update_time' => time()
                ];
                $r = Db::name('users')->where('id',$order_info['uid'])->update($updata);
            }elseif ($order_info['goods_id'] == 4){//年卡卡
                $updata = [
                    'level' => 2,
//                    'vip_start' => time(),
                    'vip_end' => strtotime('+1 year',$base_time),
                    'update_time' => time()
                ];
                $r = Db::name('users')->where('id',$order_info['uid'])->update($updata);
            }else{
                Db::rollback();
                return false;
            }
            if ($r === false){
                Db::rollback();
                return false;
            }

            //提交事务
            Db::commit();
            return true;
        } catch (\Exception $e) {
            //回滚
            Db::rollback();
            return false;
        }
    }

    /**
     * @return string
     * @author 北川
     * @time 2019/9/16 21:55
     * @comment　paypal 回调
     */
    public function paypalNotify()
    {
        if (!request()->isPost()) die();
        $post = request()->param();

        $order_sn = $post['invoice']; //收取订单号
        $config = config('paypal.');

        $data['cmd'] = '_notify-validate'; //增加cmd参数，
        foreach ($post as $key => $item) $data[$key] = ($item);  //如果数据验证失败，请在这里将参数urlencode
        //file_put_contents('aaa.txt',var_export($data,true));
        $res = $this->curlRequest($config['url'], $data, 'POST');

        if (!empty($res)) {
            if (strcmp($res, "VERIFIED") == 0) {

                if ($post['payment_status'] == 'Completed') {
                    //验证收款账号
                    if ($post['business'] != $config['business']){
                        return 'fail';
                    }

                    //付款完成，这里修改订单状态
                    $order_info = Db::name('order')->where(['sn' => $order_sn])->find();
                    if (!$order_info) {
                        return 'fail';
                    }
                    //验证收款金额
                    if ($post['payment_gross'] != $order_info['USD_amount']){
                        return 'fail';
                    }
                    $re = $this->handleSuccessLogistics($order_info);
                    if ($re) {
                        return "success";
                    } else {
                        return 'fail';
                    }
                }
            } elseif (strcmp($res, "INVALID") == 0) {
                //未通过认证，有可能是编码错误或非法的 POST 信息
                return 'fail';
            }
        } else {
            //未通过认证，有可能是编码错误或非法的 POST 信息

            return 'fail';

        }
        return 'fail';
    }

    /**
     * 模拟提交参数，支持https提交 可用于各类api请求
     * @param string $url ： 提交的地址
     * @param array $data : POST数组
     * @param string $method : POST/GET，默认GET方式
     * @return mixed
     */
    private function curlRequest($url, $data = [], $method = 'GET')
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
            if ($data != '') {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
            }
        }
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据
    }

    public function payTest(){

    }
}