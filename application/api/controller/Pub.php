<?php
/**
 * Author: 北川
 * Time: 2019/9/17 23:44
 * @comment　
 */

namespace app\api\controller;



use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\LabelAlignment;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Response\QrCodeResponse;
use think\Db;

class Pub extends Common
{
    /**
     * @author 北川
     * @time 2019/9/17 23:48
     * @comment　获取子地区
     */
    public function getChildRegion(){
        $type = input('type',0);
        if ($type == 0){
            $pid = 0;
        }else{
            $pid = Db::name('region')->where([['name','eq',input('name',0)],['type','eq',$type]])->value('id');
        }
        $list = Db::name('region')->where('pid',$pid)->select();
        gg(1,'success',$list);
    }

    /**
     * @author 北川
     * @time 2019/10/22 22:37
     * @comment　字符串转二维码
     */
    public function textToQr(){
        $text = input('text');
        $qrCode = new QrCode($text);
        $qrCode->setSize(300);

// Set advanced options
        $qrCode->setWriterByName('png');
        $qrCode->setMargin(10);
//        $qrCode->setEncoding('UTF-8');
//        $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
        $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
        $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
//        $qrCode->setLabel('Scan the code', 16, __DIR__.'/../assets/fonts/noto_sans.otf', LabelAlignment::CENTER);
//        $qrCode->setLogoPath(__DIR__.'/../assets/images/symfony.png');
//        $qrCode->setLogoSize(150, 200);
//        $qrCode->setRoundBlockSize(true);
//        $qrCode->setValidateResult(false);
//        $qrCode->setWriterOptions(['exclude_xml_declaration' => true]);

// Directly output the QR code
        header('Content-Type: '.$qrCode->getContentType());
        echo $qrCode->writeString();
    }

}