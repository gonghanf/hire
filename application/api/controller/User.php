<?php
/**
 * Author: 北川
 * Time: 2019/6/19 23:08
 * @comment　
 */

namespace app\api\controller;


use app\common\controller\GG;
use app\common\model\Position;
use app\common\model\Users;
use tests\thinkphp\library\think\log\driver\fileTest;
use think\Db;

class User extends Common
{
    /**
     * @author 北川
     * @time 2019/6/19 23:14
     * @comment　用户详情表
     */
    public function userInfo()
    {

        $id = $this->uid;

        $user_model = new \app\common\model\Users();
        $info = $user_model->getDetail($id);
        $info['tags'] = $info['tags_arr'];
        gg(1, 'success', $info);
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:31
     * @comment　修改信息接口
     */
    public function editUserInfo()
    {
        $id = $this->uid;
        $data = input('post.');
        unset($data['file']);
        $users_model = new Users();
        $r = $users_model->isUpdate(true)->allowField(true)->save($data, ['id' => $id]);
        if ($r) {
            gg(1, '修改成功');
        } else {
            gg(0, '修改失败');
        }
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/6/19 23:06
     * @comment　更换手机号
     */
    public function changMobile()
    {

        $mobile = input('mobile');
        $code = cache('change_mobile' . $mobile);
        if (!input('code')) {
            gg(0, '验证码不能为空');
        }
        if ($code != input('code')) {
            gg(0, '验证码错误');
        }
        $has_mobile = Db::name('users')->where([['mobile', 'eq', $mobile]])->find();

        if ($has_mobile) {
            if ($has_mobile['id'] == $this->uid) {
                gg(0, '不能与旧手机号相同');
            } else {
                gg(0, '该手机号已绑定，请更换其他手机号');
            }
        }

        $r = Db::name('users')->where([['id', 'eq', $this->uid]])->update(['mobile' => $mobile]);
        if ($r) {
            gg(1, '更换手机号成功');
        } else {
            gg(0, '更换手机号失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/6/21 23:31
     * @comment　手机号解除绑定 --无用
     */
    public function releaseMobile()
    {

        $mobile = Db::name('users')->where([['id', 'eq', $this->uid]])->value('mobile');
        $code = cache('release_mobile' . $mobile);
        if (!input('code')) {
            gg(0, '验证码不能为空');
        }
        if ($code != input('code')) {
            gg(0, '验证码错误');
        }

        $r = Db::name('users')->where([['id', 'eq', $this->uid]])->update(['mobile' => '']);
        if ($r) {
            session('bindMobile', false);
            gg(1, '解绑成功');
        } else {
            gg(0, '解绑失败');
        }
    }


    /**
     * @author 北川
     * @time 2019/8/13 23:26
     * @comment　上传身份证照片
     */
    public function editCardImg()
    {
        $id = $this->uid;
        $card1 = input('card1');
        // $card2 = input('card2');
        if (!$card1) {
            gg(0, '请上传身份证正面');
        }
        // if (!$card2) {
        //     gg(0, '请上传身份证反面');
        // }
        $r = Db::name('users')->where([['id', 'eq', $id]])->update(['card1' => $card1, 'update_time' => time()]);
        if ($r) {
            gg(1, '操作成功');
        } else {
            gg(0, '操作失败');
        }
    }

    /**
     * @param string $id
     * @return mixed
     * @author 北川
     * @time 2019/8/14 21:22
     * @comment　个人简历
     */
    public function myResume()
    {
        //$id = input('id',0);
        $id = $this->uid;
        $data = input('post.');
        unset($data['file']);
        $user_data = [
            'username' => $data['username'],
            'avatar' => $data['avatar'],
            //'email' => $data['email'],
            //'mobile' => $data['mobile'],
            'sex' => $data['sex'],
            'country' => $data['country'],
            'province' => $data['current_province'],
            'birthday' => $data['birthday'],
        ];

        Db::startTrans();
        //保存基本信息到users表
        if ($id) {
            $r1 = Db::name('users')->where([['id', 'eq', $id]])->update($user_data);
        } else {
            $r1 = Db::name('users')->insertGetId($user_data);
            $id = $r1;
        }
        if ($r1 === false) {
            Db::rollback();
            gg(0, '操作失败');
        }
        session('user_avatar',$user_data['avatar']);
        session('user_name',$user_data['username']);

        //保存简历信息到简历表
        $resume_data = [
            'uid' => $id,
            'education' => $data['education'],
            'tags' => $data['tags'],
            'job_form' => $data['job_form'],
            'qualification' => $data['qualification'],
            'experience' => $data['experience'],
            'salary_min' => $data['salary_min'],
            'salary_max' => $data['salary_max'],
            'salary' => floatval($data['salary_min']).'k - '.floatval($data['salary_max']).'k',
            'apartment' => $data['apartment'],
            'province' => $data['province'],
            'city' => $data['city'],
            'start_date' => $data['start_date'],
            'district' => $data['district'],
            'full_address' => $data['province'] . ' ' . $data['city'] . ' ' . $data['district'],
            'position_type' => $data['position_type'],
            'self_intro' => $data['self_intro'],
        ];
        $resume_info = Db::name('resume')->where([['uid', 'eq', $id], ['is_del', 'eq', 0]])->find();
        if ($resume_info) {
            $resume_data['update_time'] = time();
            $r2 = Db::name('resume')->where([['id', 'eq', $resume_info['id']]])->update($resume_data);
        } else {
            $resume_data['create_time'] = time();
            $r2 = Db::name('resume')->insertGetId($resume_data);
        }
        if ($r2 === false) {
            Db::rollback();
            gg(0, '操作失败');
        }
        //保存教育经历
        //先删除就记录
        $r3 = Db::name('education_exp')->where([['uid', 'eq', $id]])->delete();
        if ($r3 === false) {
            Db::rollback();
            gg(0, '操作失败');
        }
        //再插入记录
        if (isset($data['school'])) {
            $edu_data = [];
            foreach ($data['school'] as $k => $v) {
                $edu_data[] = [
                    'uid' => $id,
                    'school' => $v,
                    'major' => $data['major'][$k],
                    'reward' => $data['reward'][$k],
                ];
            }
            $r4 = Db::name('education_exp')->insertAll($edu_data);
            if ($r4 === false) {
                Db::rollback();
                gg(0, '操作失败');
            }
        }
        //保存工作经历
        //先删除就记录
        $r5 = Db::name('work_exp')->where([['uid', 'eq', $id]])->delete();
        if ($r5 === false) {
            Db::rollback();
            gg(0, '操作失败');
        }
        //在保存记录
        if (isset($data['company'])) {
            $edu_data = [];
            foreach ($data['company'] as $k => $v) {
                $edu_data[] = [
                    'uid' => $id,
                    'company' => $v,
                    'position' => $data['position'][$k],
                    'desc' => $data['desc'][$k],
                ];
            }
            $r6 = Db::name('work_exp')->insertAll($edu_data);
            if ($r6 === false) {
                Db::rollback();
                gg(0, '操作失败');
            }
        }

        //提价表单
        Db::commit();
        gg(1, '操作成功');
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:09
     * @comment　职位申请列表
     */
    public function positionAppList()
    {
        $uid = $this->uid;
        $where = [];
        $where[] = ['a.uid', 'eq', $uid];
        $where[] = ['a.is_del', 'eq', 0];
        $where[] = ['b.id', 'gt', 0];
        $field = 'b.id,a.status app_status,a.create_time,b.name,b.salary,b.salary_min,b.salary_max,b.province,b.city,b.district,b.full_address,b.id position_id,b.status position_status,b.is_del,b.education';
        $list = Db::name('resume_position')
            ->alias('a')
            ->leftJoin('position b', 'a.position_id = b.id')
            ->field($field)
            ->where($where)
            ->order('a.id desc')
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();
        $collectionList = Db::name('collection')->where([['uid','eq',$this->uid]])->select();

        foreach ($list['data'] as $key => $o) {

            $list['data'][$key]['is_collection'] = 0;
            $list['data'][$key]['education'] = lan($o['education']);

            foreach ($collectionList as $key1 => $p) {
                if($o['id']== $p['target_id'] && $p['type']==2) {
                    $list['data'][$key]['is_collection'] = 1;
                    break;
                }
            }
        }
        foreach ($list['data'] as $k => $v) {
            $v['create_time'] = date('Y-m-d H:i', $v['create_time']);
            $list['data'][$k] = $v;
        }
        gg(1, 'success', $list);
    }

    /**
     * @author 北川
     * @time 2019/8/15 22:35
     * @comment　用户收藏列表
     */
    public function userCollection()
    {
        $uid = $this->uid;
        $type = input('type');//1为收藏企业 2为职位
        if ($type == 2) { //收藏职位列表
            $where = [];
            $where[] = ['a.uid', 'eq', $uid];
            $where[] = ['a.type', 'eq', 2];//收藏职位
            $field = 'a.id,a.create_time,b.name,b.salary,b.salary_min,b.salary_max,b.province,b.city,b.district,b.full_address,b.id position_id,b.status position_status,b.is_del,b.education,b.experience';
            $list = Db::name('collection')
                ->alias('a')
                ->leftJoin('position b', 'a.target_id = b.id and a.type=2')
                ->field($field)
                ->where($where)
                ->order('a.id desc')
                ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
                ->toArray();
            foreach ($list['data'] as $k => $v) {
                $v['create_time'] = date('Y-m-d H:i', $v['create_time']);
                $v['experience'] = lan($v['experience']).'年';
                $v['education'] = lan($v['education']);
                $list['data'][$k] = $v;
            }
            gg(1, 'success', $list);
        } else {//收藏企业列表
            $where = [];
            $where[] = ['a.uid', 'eq', $uid];
            $where[] = ['a.type', 'eq', 1];//收藏企业
            $field = 'a.id,a.target_id,a.create_time,b.avatar,b.username,b.mobile,b.email,b.contact,b.contact_mobile,b.industry,b.scale,b.desc,b.is_lock,b.id company_id';
            $list = Db::name('collection')
                ->alias('a')
                ->leftJoin('users b', 'a.target_id = b.id and a.type=1')
                ->field($field)
                ->where($where)
                ->order('a.id desc')
                ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
                ->toArray();
            foreach ($list['data'] as $k => $v) {
                $v['create_time'] = date('Y-m-d H:i', $v['create_time']);
                $list['data'][$k] = $v;
            }
            gg(1, 'success', $list);
        }
    }

    /**
     * @author 北川
     * @time 2019/10/22 23:00
     * @comment　积分详情
     */
    public function pointList(){
        $where = [];
        $where[] = ['uid','eq',$this->uid];
        $list = Db::name('point_detail')
            ->where($where)
            ->order('id desc')
            ->paginate(['list_rows' => $this->pageSize, 'page' => $this->page])
            ->toArray();
        foreach ($list['data'] as $k => $v) {
            if ($v['type'] == 1){
                $v['amount'] = '+'.$v['amount'];
            }else{
                $v['amount'] = '-'.$v['amount'];
            }
            $list['data'][$k] = $v;
        }
        gg(1, 'success', $list);
    }
}