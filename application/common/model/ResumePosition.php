<?php
/**
 * Author: 北川
 * Time: 2019/8/2 22:48
 * @comment　
 */

namespace app\common\model;


use think\Db;
use think\Model;

class ResumePosition extends Model
{
    protected $type       = [
        // 设置时间戳类型（整型）
        'create_time' => 'timestamp:Y-m-d H:i',
        'update_time' => 'timestamp:Y-m-d H:i'
    ];
    /**
     * @author 北川
     * @time 2019/8/2 22:49
     * @comment　获取个人简历信息
     */
    public function getLog($where = [],$page = 1,$limit = 10,$order = 'a.id desc'){
        $log = $this
            ->alias('a')
            ->join('users b','a.company_id=b.id')
            ->leftJoin('position c','a.position_id=c.id')
            ->field('a.*,b.username company_name,c.name position_name')
            ->where([['a.is_del','eq',0]])
            ->where($where)
            ->order($order)
            ->paginate(['list_rows'=>$limit,'page'=>$page])
            ->toArray();
        return $log;
    }
    /**
     * @author 北川
     * @time 2019/8/2 22:49
     * @comment　获取企业简历
     */
    public function getResumePosition($where = [],$page = 1,$limit = 10,$order = 'a.id desc'){
        $list = $this
            ->alias('a')
            ->join('users b','a.company_id=b.id')
            ->join('users d','a.uid=d.id')
            ->join('resume e', 'a.uid=e.uid')
            ->leftJoin('position c','a.position_id=c.id')
            ->where([['a.is_del','eq',0]])
            ->field('a.*,b.username company_name,b.mobile company_mobile,b.email company_email,b.sn company_sn,c.name position_name,d.username user_name,d.mobile user_mobile,d.email user_email,d.sn user_sn,e.experience,e.education,e.salary,e.full_address,d.avatar')
            ->where($where)
            ->order($order)
            ->paginate(['list_rows'=>$limit,'page'=>$page])
            ->toArray();
        foreach($list['data'] as $k=>$v){
            $is_collect = Db::name('collection')->where([['uid','eq',$v['company_id']],['target_id','eq',$v['uid']],['type','eq',3]])->value('id');

            $v['is_collect'] = ($is_collect>0)?1:0;
            //翻译
            if (isset($v['tags'])){
                $tag_arr = (array)array_filter(explode(',',$v['tags']));
                $v['tags'] = array_map('lan',$tag_arr);
            }
            if (isset($v['welfare'])){
                $wel_arr = (array)array_filter(explode(',',$v['welfare']));
                $v['welfare'] = array_map('lan',$wel_arr);
            }
            if (isset($v['qualification'])){
                $qual_arr = (array)array_filter(explode(',',$v['qualification']));
                $v['qualification'] = array_map('lan',$qual_arr);
            }
            if (isset($v['education'])){
                $v['education'] = lan($v['education']);
            }
            if (isset($v['experience'])){
                $v['experience'] = lan($v['experience']);
            }
            if (isset($v['position_type'])){
                $v['position_type'] = lan($v['position_type']);
            }

            $list['data'][$k] = $v;
        }

        return $list;
    }
}