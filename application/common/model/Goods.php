<?php
/**
 * Author: 北川
 * Time: 2019/9/19 22:48
 * @comment　
 */

namespace app\common\model;


use think\Model;

class Goods extends Model
{
    /**
     * @param array $where
     * @return array
     * @author 北川
     * @time 2019/9/19 22:50
     * @comment　获取商品列表
     */
    public function goodsList($where = []){
        $list = $this->where($where)->select();
        $arr = [];
        foreach ($list as $v) {
            $arr[$v['id']] = $v;
        }
        return $arr;
    }

}