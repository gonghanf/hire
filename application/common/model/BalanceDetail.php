<?php
/**
 * Author: 北川
 * Time: 2019/6/22 22:40
 * @comment　
 */

namespace app\common\model;



use think\Db;
use think\Model;

class BalanceDetail extends Model
{
    /**
     * @param $uid
     * @param $type
     * @param $order_id
     * @param $money
     * @return int|string
     * @author 北川
     * @time 2019/6/22 22:44
     * @comment　添加用户资金明细
     */
    public static function addLog($uid,$type,$order_id,$money){
        $data = [
            'uid' => $uid,
            'type' =>$type, //1充值 2消费
            'order_id' => $order_id,
            'money' => $money,
            'create_time' => time()
        ];
        return Db::name('balance_detail')->insert($data);
    }

}