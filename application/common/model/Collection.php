<?php
/**
 * Author: 北川
 * Time: 2019/9/18 22:10
 * @comment　
 */

namespace app\common\model;


use think\Db;
use think\Model;

class Collection extends Model
{
    /**
     * @author 北川
     * @time 2019/9/18 22:11
     * @comment　收藏记录集合
     */
    public static function collectionArr($uid,$type = 0){
        $collectionList = Db::name('collection')->where([['uid','eq',$uid]])->select();
        $col_arr = [];
        foreach ($collectionList as $k=>$v){
            $col_arr[$v['type']][$v['target_id']] = $v;
        }
        if ($type){
            return $col_arr[$type];
        }else{
            return $col_arr;
        }
    }

}