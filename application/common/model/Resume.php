<?php
/**
 * Author: 北川
 * Time: 2019/8/2 22:48
 * @comment　
 */

namespace app\common\model;


use think\Db;
use think\Model;

class Resume extends Model
{
    /**
     * @author 北川
     * @time 2019/8/2 22:49
     * @comment　获取个人简历信息
     */
    public function getResumeInfo($uid){
        $resume_info = Db::name('resume')->where([['uid','eq',$uid]])->find();
        $education_exp = Db::name('education_exp')->where([['uid','eq',$uid]])->select();
        $work_exp = Db::name('work_exp')->where([['uid','eq',$uid]])->select();
        $resume_info['education_exp'] = $education_exp;
        $resume_info['word_exp'] = $work_exp;
        return $resume_info;
    }

}