<?php
/**
 * Author: 北川
 * Time: 2019/8/11 12:08
 * @comment　
 */

namespace app\common\model;


use think\Db;
use think\Model;

class Position extends Model
{
    /**
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return array
     * @author 北川
     * @time 2019/8/11 12:10
     * @comment　职位列表
     */
    public function positionList($where = [],$page = 1,$limit = 10,$order = 'a.id desc',$field = "b.*,a.*,count(c.id) app_num"){
        $list = $this
            ->alias('a')
            ->leftJoin('users b','a.company_id = b.id and b.type >=2')
            ->leftJoin('resume_position c','a.id=c.position_id')
            ->where($where)
            ->where([['a.is_del','eq',0]])
            ->field($field)
            ->group('a.id')
            ->orderRaw($order)
            ->paginate(['list_rows'=>$limit,'page'=>$page])
            ->toArray();
        foreach($list['data'] as $k=>$v){
            //翻译
            if (isset($v['tags'])){
                $tag_arr = (array)array_filter(explode(',',$v['tags']));
                $v['tags'] = array_map('lan',$tag_arr);
            }
            if (isset($v['welfare'])){
                $wel_arr = (array)array_filter(explode(',',$v['welfare']));
                $v['welfare'] = array_map('lan',$wel_arr);
            }
            if (isset($v['qualification'])){
                $qual_arr = (array)array_filter(explode(',',$v['qualification']));
                $v['qualification'] = array_map('lan',$qual_arr);
            }
            if (isset($v['education'])){
                $v['education'] = lan($v['education']);
            }
            if (isset($v['experience'])){
                $v['experience'] = lan($v['experience']);
            }
            if (isset($v['position_type'])){
                $v['position_type'] = lan($v['position_type']);
            }

            $list['data'][$k] = $v;
        }

        return $list;
    }

    /**
     * @param $where
     * @return array|mixed|null|\PDOStatement|string|Model
     * @author 北川
     * @time 2019/8/11 15:03
     * @comment　职位详情
     */
    public function positionInfo($where){
        $info = Db::name('position')->where($where)->find();
        $info['tags_arr'] = (array)array_filter(explode(',',$info['tags']));
        $info['qual_arr'] = (array)array_filter(explode(',',$info['qualification']));
        $info['welfare_arr'] = (array)array_filter(explode(',',$info['welfare']));
        return $info;
    }

}