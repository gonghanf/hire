<?php
namespace app\common\model;

use think\Db;
use think\Model;

class Users extends Model
{
	protected $name = 'users';
    protected $type       = [
        // 设置addtime为时间戳类型（整型）
        'reg_time' => 'timestamp:Y-m-d H:i:s',
        'create_time' => 'timestamp:Y-m-d H:i:s',
        'update_time' => 'timestamp:Y-m-d H:i:s'
    ];
	// birthday修改器
	protected function setpwdAttr($value){
			return md5($value);
	}

    /**
     * @param $uid
     * @return array
     * @author 北川
     * @time 2019/8/5 23:14
     * @comment　获取个人信息详情
     */
	public function getDetail($uid){
	    $info= Db::name('users')->where([['id','eq',$uid]])->find();
	    //$info= $this->where([['id','eq',$uid]])->find()->toArray();
        $info['current_province'] = $info['province'];
	    //$field = 'job_desc,start_date,apartment,job_form,qualification,education,tags,salary_min,salary_max,province,city,district,job,self_intro';
	    $field = 'salary,position_type,job_desc,start_date,apartment,job_form,qualification,education,tags,salary_min,salary_max,province,city,district,position_type,self_intro,experience,qualification';
	    $resume = Db::name('resume')->field($field)->where([['uid','eq',$uid]])->find();
	    $detail = array_merge($info,(array)$resume);
        $tag_arr = (array)array_filter(explode(',',$detail['tags']));
        $detail['tags_arr'] = array_map('lan',$tag_arr);
        $qual_arr = (array)array_filter(explode(',',$detail['qualification']));
        $detail['qualification'] = array_map('lan',$qual_arr);
        $detail['education'] = lan($detail['education']);
        $detail['experience'] = lan($detail['experience']);
        $detail['position_type'] = lan($detail['position_type']);
	    $detail['edu_log'] = Db::name('education_exp')->where([['uid','eq',$uid]])->select();
	    $detail['work_log'] = Db::name('work_exp')->where([['uid','eq',$uid]])->select();
	    return $detail;

    }

    /**
     * @author 北川
     * @time 2019/8/15 21:29
     * @comment　公司列表
     */
    public function companyList($where = [],$field = '*',$page = 1,$limit = 10,$order = 'a.id desc'){
        $list = $this
            ->alias('a')
            ->leftJoin('position b','a.id=b.company_id')
            ->field($field)
            ->where($where)
            ->order($order)
            ->paginate(['list_rows'=>$limit,'page'=>$page])
            ->toArray();
        foreach($list['data'] as $k=>$v){
            $v['industry'] = lan($v['industry']);
            $list['data'][$k] = $v;
        }

        return $list;
    }

    /**
     * @param array $where
     * @param string $field
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return array
     * @author 北川
     * @time 2019/8/25 9:18
     * @comment　人才列表
     */
    public function userList($where = [],$field = '',$page = 1,$limit = 10,$order = 'id desc'){
        if ($field == ''){
            $field = 'a.id,username,avatar,mobile,email,salary,salary_min,salary_max,b.province,b.city,b.district,b.full_address';
        }
        $list = Db::name('users')
            ->alias('a')
            ->leftJoin('resume b','a.id=b.uid')
            ->field($field)
            ->where($where)
            ->order($order)
            ->paginate(['list_rows'=>$this->pageSize,'page'=>$this->page])
            ->toArray();

        return $list;
    }

    /**
     * @author 北川
     * @time 2019/10/25 22:37
     * @comment　获取公司详情
     */
    public function getCompanyInfo($uid){
        $user_info = Db::name('users')->where([['id','eq',$uid]])->find();
        $resume_num = Db::name('resume_position')->where([['company_id','eq',$uid],['is_del','eq',0]])->count();
        $collection_num = Db::name('collection')->where([['uid','eq',$uid]])->count();
        $user_info['resume_num'] = $resume_num;//收到的简历
        $user_info['collection_num'] = $collection_num;//收藏数量
        $user_info['reg_time'] = date('Y-m-d H:i',$user_info['reg_time']);
        $user_info['level_text'] = Db::name('user_level')->where([['level_id','eq',$user_info['level']]])->value('level_name');
        return $user_info;
    }

    /**
     * @author 北川
     * @time 2019/10/27 9:57
     * @comment　获取个人中心左侧信息
     */
    public function userCenterLeft($uid){
        $user_info = Db::name('users')->where([['id','eq',$uid]])->find();
        $user_info['collect_company'] = Db::name('collection')->where([['uid','eq',$uid],['type','eq',3]])->count();
        $user_info['collect_position'] = Db::name('collection')->where([['uid','eq',$uid],['type','eq',2]])->count();
        return $user_info;
    }

}