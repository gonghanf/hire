<?php
/**
 * Author: 北川
 * Time: 2019/8/25 10:17
 * @comment　
 */

namespace app\common\model;


use think\Model;

class Order extends Model
{
    protected $type       = [
        // 设置时间戳类型（整型）
        'create_time' => 'timestamp:Y-m-d H:i',
        'update_time' => 'timestamp:Y-m-d H:i'
    ];

    /**
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param string $field
     * @param string $order
     * @return array
     * @author 北川
     * @time 2019/8/25 10:19
     * @comment　订单列表
     */
    public function orderList($where = [],$page = 1,$limit = 10,$field = '*',$order = 'id desc'){
        $list= $this
            ->field($field)
            ->where($where)
            ->order($order)
            ->paginate(['list_rows'=>$limit,'page'=>$page])
            ->toArray();
        return $list;
    }

}