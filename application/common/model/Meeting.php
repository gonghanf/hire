<?php
/**
 * Author: 北川
 * Time: 2019/8/11 20:59
 * @comment　
 */

namespace app\common\model;


use think\Model;

class Meeting extends Model
{
    protected $type       = [
        // 设置时间戳类型（整型）
        'create_time' => 'timestamp:Y-m-d H:i',
        'update_time' => 'timestamp:Y-m-d H:i',
        'start_time' => 'timestamp:Y-m-d H:i:s',
        'end_time' => 'timestamp:Y-m-d H:i:s'
    ];

    /**
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param string $order
     * @return array
     * @author 北川
     * @time 2019/8/11 12:10
     * @comment　签证列表
     */
    public function meetingList($where = [],$page = 1,$limit = 10,$order = 'id desc'){
        $list = $this
            ->where($where)
            ->where([['is_del','eq',0]])
            ->order($order)
            ->paginate(['list_rows'=>$limit,'page'=>$page])
            ->toArray();

        return $list;
    }

    /**
     * @param $where
     * @return array|mixed|null|\PDOStatement|string|Model
     * @author 北川
     * @time 2019/8/11 15:03
     * @comment　签证详情
     */
    public function meetingInfo($where){
        $info = $this->where($where)->find();
        return $info;
    }

}