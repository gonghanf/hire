<?php
namespace app\common\model;

use think\Db;
use think\Model;

class WebConfig extends Model
{

    /**
     * @return array
     * @author 北川
     * @time 2019/9/15 17:09
     * @comment　获取所有配置
     */
    public static function allConfig(){
        $list = Db::name('web_config')->order('sort')->select();
        $list_arr = [];
        foreach ($list as $k=>$v){
            $list_arr[$v['type']][]=$v;
        }
        return $list_arr;
    }

}