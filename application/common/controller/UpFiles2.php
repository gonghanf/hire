<?php
namespace app\common\controller;
use gmars\qiniu\Qiniu;
use think\Db;
use think\Request;
use think\Controller;
use think\facade\Env;
class UpFiles extends Controller
{
    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/13 21:59
     * @comment　单图上传
     */
    public function upload(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下

        $info = $file->validate(['ext' => 'jpg,png,gif,jpeg'])->move('uploads');
        if($info){
            $result['code'] = 1;
            $result['info'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息

            $result['code'] =0;
            $result['info'] =  $file->getError();
            $result['url'] = '';
            return $result;
        }
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/13 21:59
     * @comment　单文件上传
     */
    public function file(){
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['ext' => 'zip,rar,pdf,swf,ppt,psd,ttf,txt,xls,doc,docx'])->move('uploads');
        if($info){
            $result['code'] = 0;
            $result['info'] = '文件上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());

            $result['url'] = '/uploads/'. $path;
            $result['ext'] = $info->getExtension();
            $result['size'] = byte_format($info->getSize(),2);
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =1;
            $result['info'] = '文件上传失败!';
            $result['url'] = '';
            return $result;
        }
    }
    public function pic(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['ext' => 'jpg,png,gif,jpeg'])->move(Env::get('root_path') . 'public/uploads');
        if($info){
            $result['code'] = 1;
            $result['info'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result['url'] = '/uploads/'. $path;
            return json_encode($result,true);
        }else{
            // 上传失败获取错误信息
            $result['code'] =0;
            $result['info'] = '图片上传失败!';
            $result['url'] = '';
            return json_encode($result,true);
        }
    }
    /**
     * 后台：wangEditor
     * @return \think\response\Json
     */
    public function editUpload(){
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['ext' => 'jpg,png,gif,jpeg'])->move('uploads');
        if($info){
            $path=str_replace('\\','/',$info->getSaveName());
            return '/uploads/'. $path;
        }else{
            // 上传失败获取错误信息
            $result['code'] =1;
            $result['msg'] = '图片上传失败!';
            $result['data'] = '';
            return json_encode($result,true);
        }
    }
    //多图上传
    public function upImages(){
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['ext' => 'jpg,png,gif,jpeg'])->move(Env::get('root_path') . 'public/uploads');
        if($info){
            $result['code'] = 0;
            $result['msg'] = '图片上传成功!';
            $path=str_replace('\\','/',$info->getSaveName());
            $result["src"] = '/uploads/'. $path;
            return $result;
        }else{
            // 上传失败获取错误信息
            $result['code'] =1;
            $result['msg'] = '图片上传失败!';
            return $result;
        }
    }
    /**
     * 后台：NKeditor
     * @return \think\response\Json
     */
    public function editimg(){
        $allowExtesions = array(
            'image' => 'gif,jpg,jpeg,png,bmp',
            'flash' => 'swf,flv',
            'media' => 'swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb',
            'file' => 'doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2',
        );
        // 获取上传文件表单字段名
        $fileKey = array_keys(request()->file());
        // 获取表单上传文件
        $file = request()->file($fileKey['0']);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['ext'=>$allowExtesions[input('fileType')]])->move('./uploads');
        if($info){
            $path=str_replace('\\','/',$info->getSaveName());
            $url = '/uploads/'. $path;
            $result['code'] = '000';
            $result['message'] = '图片上传成功!';
            $result['item'] = ['url'=>$url];
            return json($result);
        }else{
            // 上传失败获取错误信息
            $result['code'] =001;
            $result['message'] = $file->getError();
            $result['url'] = '';
            return json($result);
        }
    }
    /**
     * @author 北川
     * @time 2019/3/19 17:47
     * @comment base64上传图片
     */
    public function base64Upload(){
        /**
         * base64图片上传
         * @param $base64_img
         * @return array
         */
        $base64_img = trim(get_data('img'));
        //$up_dir = '/uploads/'.date('Ymd');//存放在当前目录的upload文件夹下
        $up_dir = './uploads/'.date('Ymd').'/';
        if(!file_exists($up_dir)){
            mkdir($up_dir,0777,true);
        }
        if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_img, $result)){
            $type = $result[2];
            if(in_array($type,array('pjpeg','jpeg','jpg','gif','bmp','png'))){
                $new_file = $up_dir.md5(uniqid(mt_rand(),true)).'.'.$type;
                if(file_put_contents($new_file, base64_decode(str_replace($result[1], '', $base64_img)))){
                    $img_path = substr($new_file,1);
                    //$img_path = str_replace('../../..', '', $new_file);
                    $result = [
                        'path' => $img_path,
                        'url' => add_domain($img_path)
                    ];
                    return ['code'=>1,'msg'=>'上传成功','data'=>$result];
                }else{
                    return ['code'=>0,'msg'=>'上传失败'];

                }
            }else{
                //文件类型错误
                return ['code'=>0,'msg'=>'图片上传类型错误'];
            }
        }else{
            //文件错误
            return ['code'=>0,'msg'=>'文件错误'];
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 19:44
     * @comment　七牛base64上传
     */
    public function qiniuBase64Upload()
    {
        $accessKey = config('qiniu.accesskey');//七牛云accesskey
        $secretKey = config('qiniu.secretkey');//七牛云secretkey
        $bucket = config('qiniu.bucket');//七牛云存储空间
        $url = config('qiniu.url');//七牛云绑定的域名默认为测试域名
        //$base64 = trim(input('post.base64'));
        $base64 = get_data('img');
        if (!$base64) {
            gg(0,'未上传图片', '');
        }
        //$base64 = str_replace('data:image/jpeg;base64,', '', $base64); //只要逗号后面的
        $base64_arry = explode(',', $base64);
        $base64 = $base64_arry[1];
        $qiniu = new Qiniu($accessKey,$secretKey,$bucket);
        $ret = $qiniu->baseUplod($base64);
        if ($ret) {
            $code = 1;
            $message = '上传成功';
            $img_url = $url.'/'.$ret;//只要返回的key,将key与绑定的域名拼接就能得到 上传后的url了
            $img_path = $ret;//只要返回的key,将key与绑定的域名拼接就能得到 上传后的url了

        } else {
            $code = 0;
            $message = '上传失败';
            $img_url = '';//只要返回的key,将key与绑定的域名拼接就能得到 上传后的url了
            $img_path = '';//只要返回的key,将key与绑定的域名拼接就能得到 上传后的url了
        }
        $data['img_path'] = $img_path;
        $data['img_url'] = $img_url;
        gg($code, $message,$data);
    }
}