<?php
/**
 * Author: 北川
 * Time: 2019/7/29 23:25
 * @comment　
 */

namespace app\common\controller;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use think\Controller;

class Mail extends Controller
{
    public function sendEmail($reciver,$code){
        $mail = new PHPMailer(true);
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.qq.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = '304761681@qq.com';                     // SMTP username
            $mail->Password   = 'lgszyytitlbabgbf';                     // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            //$mail->SMTPSecure = 'ssl';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('304761681@qq.com', '招聘平台');
            $mail->addAddress($reciver);     // Add a recipient
            //$mail->addAddress('ellen@example.com');               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            // Attachments
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
            $mail->CharSet= 'UTF-8';
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = '【招聘平台】身份验证';
            $mail->Body    = '【招聘平台】您正在进行身份验证，验证码：'.$code.'。请不要将收到的验证码告诉别人！';
            //$mail->AltBody = '请使用支持HTML的客户端浏览邮件！';

            $mail->send();
            return ['code'=>1,'msg'=>'发送成功'];

        } catch (Exception $e) {
            return ['code'=>0,'msg'=>$mail->ErrorInfo];

        }
    }
}