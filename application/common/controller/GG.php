<?php
/**
 * Author: 北川
 * Time: 2019/8/2 23:09
 * @comment　
 */

namespace app\common\controller;


use think\Controller;
use think\Db;

class GG extends Controller
{
    /**
     * @author 北川
     * @time 2019/8/2 23:10
     * @comment　获取学历等级
     */
    public function getEduLevel(){
        $list = Db::name('web_config')->where([['type','eq','education']])->select();
        return $list;
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/3 0:02
     * @comment　获取带全部的地区
     */
    public function allRegionData($level = 3,$add_all = 1){
        $region_data = cache('all_region_data'.$level);
        if ($region_data){
            return $region_data;
        }
        $prov = Db::name('region')->where([['type','eq',1]])->select();
        $region_data = [];
        if ($add_all){
            $all = [
                'value' => '全部',
                'label' => '全部',
                'children' => []
            ];
            if ($level>=2){
                $all['children'] = [
                    'value' => '全部',
                    'label' => '全部',
                    'children' => []
                ];
            }
            if ($level >=3){
                $all['children'][0] = [
                    'value' => '全部',
                    'label' => '全部',
                    'children' => []
                ];
            }
            $region_data[] = $all;
        }

        foreach ($prov as $k=>$v){
            $p = [];
            $p['value'] = $v['name'];
            $p['label'] = $v['name'];
            $p['children']=[];
            if ($level >=2){
                if ($add_all){
                    $p['children'][] = [
                        'value' => '全部',
                        'label' => '全部',
                        'children' => [[
                            'value' => '全部',
                            'label' => '全部',
                            'children' => []
                        ]]
                    ];
                }

                $city = Db::name('region')->where([['pid','eq',$v['id']]])->select();
                foreach ($city as $k2=>$v2){
                    $m = [];
                    $m['value'] = $v2['name'];
                    $m['label'] = $v2['name'];
                    $m['children'] = [];
                    if ($level >=3){
                        if ($add_all){
                            $m['children'][] = [
                                'value' => '全部',
                                'label' => '全部',
                                'children' => []
                            ];
                        }

                        $dist = Db::name('region')->where([['pid','eq',$v2['id']]])->select();
                        foreach ($dist as $k3=>$v3){
                            $n = [];
                            $n['value'] = $v3['name'];
                            $n['label'] = $v3['name'];
                            $n['children'] = [];
                            $m['children'][] = $n;
                        }
                    }
                    $p['children'][] = $m;
                }
            }
            $region_data[] = $p;
        }
        cache('all_region_data'.$level,$region_data);
        return $region_data;
    }

}