<?php
namespace app\admin\controller;
use Think\Db;
class Config extends Common
{
    private $type_arr = [
      'tags' => '标签',
      'education' => '学历',
      'industry' => '行业',
      'position_type' => '职位类型',
      'experience' => '经验要求',
      'welfare' => '福利',
      'scale' => '企业规模',
      'qualification' => '职业资格'
    ];
    public function configList($type){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','tags'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        $this->assign('title',$this->type_arr[$type]);
        $this->assign('type',$type);
        return $this->fetch();
    }

    /**
     * @param $type
     * @return array|mixed
     * @author 北川
     * @time 2019/10/9 21:40
     * @comment　编辑配置
     */
    public function configEdit(){
        $id = input('id',0);
        $type = input('type');
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = $type;
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        $this->assign('title',$this->type_arr[$type]);
        $this->assign('type',$type);

        return $this->fetch();
    }


    /**
     * @return array
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　删除配置
     */
    public function configDel(){
        $id = input('id',0);
        $res = Db::name('web_config')->where([['id','eq',$id]])->delete();
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    /**
     * @return array
     * @author 北川
     * @time 2019/10/9 21:40
     * @comment　配置排序
     */
    public function configOrder(){
        $id = input('id',0);
        $sort= input('sort',0);
        $res = Db::name('web_config')->where([['id','eq',$id]])->update(['sort'=>$sort,'update_time'=>time()]);
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }
    public function tagsList(){
        return $this->configList('tags');
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　标签列表
     */
    public function tagsList2(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','tags'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        $this->assign('title','标签列表');
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑标签
     */
    public function tagsEdit2(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'tags';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　行业列表
     */
    public function industryList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','industry'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','行业列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑行业
     */
    public function industryEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'industry';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　工作经验列表
     */
    public function experienceList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','experience'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','经验要求列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑工作经验
     */
    public function experienceEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'experience';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职位类型列表
     */
    public function positionTypeList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','position_type'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','职位类型列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑工作经验
     */
    public function positionTypeEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'position_type';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职位类型列表
     */
    public function educationList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','education'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','学历列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑工作经验
     */
    public function educationEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'education';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职位类型列表
     */
    public function welfareList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','welfare'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','福利待遇列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑福利待遇
     */
    public function welfareEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'welfare';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　企业规模列表
     */
    public function scaleList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','scale'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','企业规模列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑企业规模
     */
    public function scaleEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'scale';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职业资格列表
     */
    public function qualificationList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq','qualification'];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','职业资格列表');
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑职业资格
     */
    public function qualificationEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = 'qualification';
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }

}