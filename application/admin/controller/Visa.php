<?php
namespace app\admin\controller;
use think\Db;

class Visa extends Common
{
    public function index(){
        if(request()->isPost()) {
            $keyword=input('post.keyword');
            $map = [];
            if ($keyword){
                $map[] = ['title|editor','like',"%".$keyword."%"];
            }
            $link=Db::name('visa')->where($map)->order('sort')->select();
            $code_base_url = request()->domain().'/link/';
            foreach ($link as $k=>$v){
                $link[$k]['addtime'] = date('Y-m-d H:s',$v['addtime']);
                $link[$k]['code_url'] = $code_base_url.$link[$k]['code'];
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$link,'rel'=>1];
        }
        return $this->fetch();
    }
    //修改栏目状态
    public function linkState(){
        $id=input('post.id');
        $open=input('post.open');
        if(db('visa')->where('id='.$id)->update(['open'=>$open])!==false){
            return ['status'=>1,'msg'=>'设置成功!'];
        }else{
            return ['status'=>0,'msg'=>'设置失败!'];
        }
    }
    public function listOrder(){
        $data = input('post.');
        if(db('visa')->update($data)!==false){
            cache('linkList', NULL);
            return $result = ['msg' => '操作成功！','url'=>url('index'), 'code' => 1];
        }else{
            return $result = ['code'=>0,'msg'=>'操作失败！'];
        }
    }
    //添加
    public function add(){
        if(request()->isPost()){
            $data = input('post.');
            unset($data['file']);

            $data['addtime'] = time();
            db('visa')->insert($data);
            $result['code'] = 1;
            $result['msg'] = '资讯添加成功!';
            $result['url'] = url('index');
            cache('linkList', NULL);
            return $result;
        }else{
            $this->assign('title',lang('add').'栏目');
            $this->assign('info',[]);
            return $this->fetch('form');
        }
    }
    //修改栏目
    public function edit(){
        if(request()->isPost()){
            $data = input('post.');
            unset($data['file']);
            db('visa')->update($data);
            $result['code'] = 1;
            $result['msg'] = '资讯修改成功!';
            $result['url'] = url('index');
            cache('linkList', NULL);
            return $result;
        }else{
            $id=input('param.id');
            $info=db('visa')->where(array('id'=>$id))->find();
            $this->assign('info',json_encode($info,true));
            $this->assign('title',lang('edit').'栏目');
            return $this->fetch('form');
        }
    }
    public function del(){
        db('visa')->where(array('id'=>input('id')))->delete();
        cache('linkList', NULL);
        return ['code'=>1,'msg'=>'删除成功！'];
    }
    public function delall(){
        $map[] =array('id','IN',input('param.ids/a'));
        db('visa')->where($map)->delete();
        $result['msg'] = '删除成功！';
        $result['code'] = 1;
        $result['url'] = url('index');
        cache('linkList', NULL);
        return $result;
    }
}