<?php
namespace app\admin\controller;
use Think\Db;
class Forum extends Common{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　帖子列表
     */
    public function index(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            //$condition[] = ['is_delete','eq',0];
            if (input('get.title')){
                $condition[] = ['a.title','like','%'.input('get.title').'%'];
            }
            if (input('get.cat_id')){
                $condition[] = ['c.id','eq',input('get.cat_id')];
            }
            $list = Db::name('forum')
                ->alias('a')
                ->leftJoin('forum_category c','a.cat_id = c.id')
                ->leftJoin('users d','a.uid = d.id')
                ->field('a.*,c.name cat_name,d.username')
                ->where($condition)
                ->order('a.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            //$sql = Db::getLastSql();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        $cate = Db::name('forum_category')->select();
        $this->assign('cate',$cate);
        return $this->fetch();
    }

    /**
     * @return array
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　修改帖子状态
     */
    public function changeState(){
        if (!input('id')){
            return api_return('0','参数异常');
        }
        $info = Db::name('forum')->where([['id','eq',input('id')]])->find();
        if (!$info){
            return api_return('0','参数异常');
        }
        $updata = array();
        $updata['update_time'] = time();
        if (input('event')=='top'){
            $updata['is_top'] = $info['is_top']?0:1;
        }elseif(input('event')=='good'){
            $updata['is_good'] = $info['is_good']?0:1;
        }elseif(input('event')=='hide'){
            $updata['is_hide'] = $info['is_hide']?0:1;
        }else{
            return api_return('0','参数异常');
        }
        $res = Db::name('forum')->where([['id','eq',input('id')]])->update($updata);
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　编辑帖子
     */
    public function forumEdit(){
        if(request()->isPost()){
            if (!input('id')){
                return api_return(0,'参数异常');
            }
            $updata = input('post.');
            unset($updata['id']);
            unset($updata['file']);
            $updata['update_time'] = time();
            //dump($updata);
            $res = Db::name('forum')->where([['id','eq',input('id')]])->update($updata);
            if ($res){
                return api_return(1,'编辑成功');
            }else{
                return api_return(0,'编辑失败');
            }
        }
        $id = input('id');
        $info = Db::name('forum')->where([['id','eq',$id]])->find();
        $categorys = Db::name('forum_category')->select();
//        $boards = Db::name('forum_board')->select();
        $this->assign('info',$info);
        $this->assign('category',$categorys);
        //$this->assign('boards',$boards);
        $this->assign('title','帖子编辑');
        return $this->fetch();
    }

    /**
     * @return array
     * @author 北川
     * @time 2019/10/29 23:04
     * @comment　审核
     */
    public function forumVerify(){

        $r = Db::name('forum')->where('id',input('id',0))->setField('status',input('status'));
        if ($r !== false){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　分类列表
     */
    public function categoryList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            if (input('get.title')){
                $condition[] = ['a.title','like','%'.input('get.title').'%'];
            }
            if (input('get.cate')){
                $condition[] = ['c.id','eq',input('get.cate')];
            }
            if (input('get.board')){
                $condition[] = ['b.id','eq',input('get.board')];
            }
            $list = Db::name('forum_category')
                ->order('id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['addtime'] =$v['addtime']?date('Y-m-d H:i',$v['addtime']):'';
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @return array
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　删除分类
     */
    public function cateDel(){
        if (!input('id')){
            return api_return(0,'参数异常');
        }
        $res = Db::name('forum_category')->where([['id','eq',input('id')]])->delete();
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　编辑分类
     */
    public function cateEdit(){
        if(request()->isPost()){
            if (!input('id')){
                return api_return(0,'参数异常');
            }
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['en_name'] = input('en_name');
            $updata['updatetime'] = time();
            $res = Db::name('forum_category')->where([['id','eq',input('id')]])->update($updata);
            if ($res){
                return api_return(1,'编辑成功');
            }else{
                return api_return(0,'编辑失败');
            }
        }
        $id = input('id');
        $info = Db::name('forum_category')->where([['id','eq',$id]])->find();
        $this->assign('info',$info);
        $this->assign('title','分类编辑');
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:34
     * @comment　添加分类
     */
    public function cateAdd(){
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $indata = array();
            $indata['name'] = input('name');
            $updata['en_name'] = input('en_name');

            $indata['updatetime'] = $indata['addtime'] = time();
            $res = Db::name('forum_category')->insert($indata);
            if ($res){
                return api_return(1,'添加成功');
            }else{
                return api_return(0,'添加失败');
            }
        }
        $this->assign('title','添加分类');
        return $this->fetch('cate_edit');
    }

    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/12 22:15
     * @comment　评论列表
     */
    public function replyList(){
        if(request()->isPost()){
            $fid = input('fid',0);
            $list = Db::name('forum_reply')
                ->alias('a')
                ->leftJoin('users b','a.uid=b.id')
                ->field('a.*,b.username,b.sn')
                ->where([['fid','eq',$fid]])->select();
            foreach($list as $k=>$v){
                $list[$k]['lay_is_open']=true;
                $list[$k]['create_time']=date('Y-m-d H:i',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'is'=>true,'tip'=>'操作成功'];
        }
        return view();
    }
    public function boardAdd(){
        if(request()->isPost()){
            $data = input('post.');
            unset($data['file']);
            $data['addtime'] = $data['updatetime'] = time();
            $res = Db::name('forum_board')->insert($data);
            if ($res){
                cache('boardList', NULL);
                return api_return(1,'添加成功');
            }else{
                return api_return(0,'添加失败');
            }

        }else{
            $arr = Db::name('forum_board')->where([['pid','eq',0]])->order('pid asc')->select();
            $this->assign('boards',$arr);//板块列表
            return $this->fetch();
        }
    }

    public function boardDel(){
        if (!input('id')){
            return api_return(0,'参数异常');
        }
        $res = Db::name('forum_board')->where([['id','eq',input('id')]])->delete();
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    public function boardEdit(){
        if(request()->isPost()) {
            $data = input('post.');
            unset($data['file']);
            $id = $data['id'];
            unset($data['id']);
            $data['updatetime'] = time();
            $res = Db::name('forum_board')->where([['id','eq',$id]])->update($data);
            if ($res){
                cache('boardList', NULL);
                return api_return(1,'修改成功');
            }else{
                return api_return(0,'修改失败');
            }
        }else{
            $info = Db::name('forum_board')->where([['id','eq',input('id')]])->find();
            $this->assign('info',$info);
            return $this->fetch();
        }
    }
}