<?php
/**
 * Author: 北川
 * Time: 2019/6/11 22:45
 * @comment　
 */

namespace app\admin\controller;


use think\Db;

class Recharge extends Common
{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/5/14 16:33
     * @comment　优惠券列表
     */
    public function ruleList(){
        if(request()->isPost()){
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['name','like','%'.input('keyword').'%'];
            }
            $list=Db::name('recharge_rule')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $coupon = Db::name('recharge_rule_coupon')->where([['rule_id','eq',$v['id']]])->column('coupon_name');
                $v['coupon_name'] = implode(',',$coupon);
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k] = $v;
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/5/14 16:34
     * @comment　添加编辑优惠券
     */
    public function addEditRule(){
        if(request()->isPost()){
            $param = input('post.');
            $param['update_time'] = time();
            $coupon_id = $param['coupon_id'];
            unset($param['coupon_id']);
            $id = input('id');
            Db::startTrans();
            if ($id){
                $r = Db::name('recharge_rule')->where([['id','eq',$id]])->update($param);
            }else{
                $r = Db::name('recharge_rule')->insertGetId($param);
                $id = $r;
            }
            if (!$r){
                Db::rollback();
                gg(0,'操作失败');

            }

            $r2 = Db::name('recharge_rule_coupon')->where([['rule_id','eq',$id]])->delete();
            if ($r2 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            if ($coupon_id){
                $coupon_list = Db::name('coupon')->where([['id','in',$coupon_id]])->select();
                $in_data = [];
                foreach ($coupon_list as $k=>$v) {
                    $p = [
                        'rule_id' => $id,
                        'coupon_id' => $v['id'],
                        'coupon_name' => $v['name'],
                    ];
                    $in_data[] = $p;
                }
                $r3 = Db::name('recharge_rule_coupon')->where([['rule_id','eq',$id]])->insertAll($in_data);
                if ($r3 === false){
                    Db::rollback();
                    gg(0,'操作失败');
                }
            }
            Db::commit();
            gg(1,'操作成功');
        }else{
            if (input('id')){
                $info = Db::name('recharge_rule')->where([['id','eq',input('id')]])->find();
                $info['create_time'] = date("Y-m-d H:i",$info['create_time']);
                $coupon = Db::name('recharge_rule_coupon')
                    ->alias('a')
                    ->leftJoin('coupon b','a.coupon_id=b.id')
                    ->field('b.*')
                    ->where([['rule_id','eq',$info['id']]])
                    ->select();
                foreach ($coupon as $k=>$v){
                    $v['start_time'] = date('Y-m-d',$v['start_time']);
                    $v['end_time'] = date('Y-m-d',$v['end_time']);
                    $coupon[$k] = $v;
                }
            }else{
                $info = [];
                $coupon = [];
            }
            $this->assign('info',json_encode((object)$info));
            $this->assign('coupon',$coupon);
            return $this->fetch();
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/5/14 16:33
     * @comment　优惠券列表
     */
    public function addCoupon(){
        if(request()->isPost()){
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('coupon'))>0){
                $where[] = ['coupon','like','%'.input('coupon').'%'];
            }
            $list=Db::name('coupon')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k]['start_time'] = date('Y-m-d',$v['start_time']);
                $list['data'][$k]['end_time'] = date('Y-m-d',$v['end_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/5/21 18:20
     * @comment　删除优惠券
     */
    public function delCoupon(){

        $r = Db::name('coupon')->where([['id','eq',input('id')]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/6/15 21:33
     * @comment　充值订单
     */
    public function rechargeOrder(){
        if(request()->isPost()){
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['name','like','%'.input('keyword').'%'];
            }
            if (strlen(input('start_time'))>0){
                $where[] = ['a.create_time','egt',strtotime(input('start_time')) ];
            }
            if (strlen(input('end_time'))>0){
                $where[] = ['a.create_time','elt',strtotime(input('end_time')) ];
            }
            if (strlen(input('pay_state'))>0){
                $where[] = ['pay_state','eq',input('pay_state')];
            }
            $list=Db::name('recharge_order')
                ->alias('a')
                ->leftJoin('users b','a.uid=b.id')
                ->field('a.*,b.username,b.mobile')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                //$coupon = Db::name('recharge_rule_coupon')->where([['rule_id','eq',$v['id']]])->column('coupon_name');
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k] = $v;
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/6/15 21:33
     * @comment　删除订单
     */
    public function delOrder(){
        $r = Db::name('recharge_order')->where([['id','eq',input('id')]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/6/15 21:33
     * @comment　充值赠送优惠券
     */
    public function sendCouponList(){
        if(request()->isPost()){
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['name','like','%'.input('keyword').'%'];
            }
            $where[] = ['recharge_id','eq',input('recharge_id')];
            $list=Db::name('recharge_send_coupon')
                ->alias('a')
                ->leftJoin('coupon b','a.coupon_id=b.id')
                ->field('b.*')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $v['start_time'] = date('Y-m-d',$v['start_time']);
                $v['end_time'] = date('Y-m-d',$v['end_time']);
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k] = $v;
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

}