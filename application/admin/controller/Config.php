<?php
namespace app\admin\controller;
use Think\Db;
use think\facade\Env;

class Config extends Common
{
    private $type_arr = [
      'tags' => '标签',
      'education' => '学历',
      'industry' => '行业',
      'position_type' => '职位类型',
      'experience' => '经验要求',
      'welfare' => '福利待遇',
      'scale' => '企业规模',
      'qualification' => '职业资格'
    ];
    public function configList($type){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $condition[] = ['type','eq',$type];
            $list = Db::name('web_config')
                ->where($condition)
                ->order('sort asc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        $this->assign('title',$this->type_arr[$type]);
        $this->assign('type',$type);
        return $this->fetch('config_list');
    }

    /**
     * @param $type
     * @return array|mixed
     * @author 北川
     * @time 2019/10/9 21:40
     * @comment　编辑配置
     */
    public function configEdit(){
        $id = input('id',0);
        $type = input('type');
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['en_name'] = input('en_name');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('web_config')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['type'] = $type;
                $updata['create_time'] = time();
                $res = Db::name('web_config')->insert($updata);
            }
            if ($res){
                $this->saveDataToConfig();
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('web_config')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        $this->assign('title',$this->type_arr[$type]);
        $this->assign('type',$type);

        return $this->fetch('config_edit');
    }

    /**
     * @author 北川
     * @time 2019/10/10 23:35
     * @comment　保存中英配置到zh_en.php
     */
    private function saveDataToConfig(){
        $config = Db::name('web_config')->select();
        $arr = [];
        foreach ($config as $v){
            $arr[$v['name']] = $v['en_name'];
        }
        $zh_en = '<?php
//北川

// +----------------------------------------------------------------------
// | 中英
// +----------------------------------------------------------------------
return ';
        $zh_en .= var_export($arr,true);
        $zh_en .= ';';
        $config_path = Env::get('config_path').'/zh_en.php';
        file_put_contents($config_path,$zh_en);
    }


    /**
     * @return array
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　删除配置
     */
    public function configDel(){
        $id = input('id',0);
        $res = Db::name('web_config')->where([['id','eq',$id]])->delete();
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    /**
     * @return array
     * @author 北川
     * @time 2019/10/9 21:40
     * @comment　配置排序
     */
    public function configOrder(){
        $id = input('id',0);
        $sort= input('sort',0);
        $res = Db::name('web_config')->where([['id','eq',$id]])->update(['sort'=>$sort,'update_time'=>time()]);
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }
    public function tagsList(){
        return $this->configList('tags');
    }


    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　行业列表
     */
    public function industryList(){
        return $this->configList('industry');
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　工作经验列表
     */
    public function experienceList(){
        return $this->configList('experience');

    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职位类型列表
     */
    public function positionTypeList(){
        return $this->configList('position_type');
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职位类型列表
     */
    public function educationList(){
        return $this->configList('education');

    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职位类型列表
     */
    public function welfareList(){
        return $this->configList('welfare');
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　企业规模列表
     */
    public function scaleList(){
        return $this->configList('scale');
    }


    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　职业资格列表
     */
    public function qualificationList(){
        return $this->configList('qualification');
    }

    /**
     * @author 北川
     * @time 2019/11/7 23:58
     * @comment　设置邀请积分
     */
    public  function setVisitPoint(){
        if(request()->isPost()){
            $info = Db::name('config')->where('name','visit_point')->find();
            if ($info){
                $r = Db::name('config')->where('name','visit_point')->setField('value',input('point'));
            }else{
                $r = Db::name('config')->insert(['name'=>'visit_point','value'=>input('point')]);
            }
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }
        $this->assign('point',Db::name('config')->where('name','visit_point')->value('value'));
        return $this->fetch();
    }


}