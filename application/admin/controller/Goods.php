<?php
namespace app\admin\controller;
use Think\Db;
class Goods extends Common{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:45
     * @comment　商品列表
     */
    public function goodsList(){
        if(request()->isPost()){
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $condition = [];
            $list = Db::name('goods')
                ->where($condition)
                ->order('id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }

        $this->assign('title','价格管理');
        return $this->fetch();
    }


    /**
     * @return array
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　删除配置
     */
    public function configDel(){
        $id = input('id',0);
        $res = Db::name('goods')->where([['id','eq',$id]])->update(['is_del'=>1,'update_time'=>time()]);
        if ($res){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/12 22:57
     * @comment　添加编辑标签
     */
    public function goodsEdit(){
        $id = input('id',0);
        if(request()->isPost()){
            if (!input('name')){
                return api_return(0,'参数异常');
            }
            $updata = array();
            $updata['name'] = input('name');
            $updata['price'] = input('price');
            $updata['update_time'] = time();
            if ($id){
                $res = Db::name('goods')->where([['id','eq',input('id')]])->update($updata);
            }else{
                $updata['create_time'] = time();
                $res = Db::name('goods')->insert($updata);
            }
            if ($res){
                return api_return(1,'操作成功');
            }else{
                return api_return(0,'操作失败');
            }
        }
        $info = Db::name('goods')->where([['id','eq',$id]])->find();
        $info = $info?:[];
        $this->assign('info',$info);
        return $this->fetch();
    }



}