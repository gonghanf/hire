<?php
/**
 * Author: 北川
 * Time: 2019/6/15 21:46
 * @comment　
 */

namespace app\admin\controller;


use think\Db;

class Order extends Common
{
    public function orderList(){
        if(request()->isPost()){
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['b.username|b.mobile','like','%'.input('keyword').'%'];
            }
            if (strlen(input('goods'))>0){
                $where[] = ['a.goods_name','like','%'.input('goods').'%'];
            }
            if (strlen(input('sn'))>0){
                $where[] = ['a.sn','like','%'.input('sn').'%'];
            }
            if (strlen(input('start_time'))>0){
                $where[] = ['a.create_time','egt',strtotime(input('start_time')) ];
            }
            if (strlen(input('end_time'))>0){
                $where[] = ['a.create_time','elt',strtotime(input('end_time')) ];
            }
            $list=Db::name('order')
                ->alias('a')
                ->leftJoin('users b','a.uid=b.id')
                ->field('a.*,b.username,b.mobile,b.sn user_sn')
                ->where($where)
                ->order('a.id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k] = $v;
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }
}