<?php
namespace app\admin\controller;
use app\common\model\Resume;
use app\common\model\ResumePosition;
use app\common\model\WebConfig;
use think\Db;

class Users extends Common{
    //会员列表
    public function index(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $where = [];
            $where[] = ['type','eq',1];//求职者
            $list=db('users')->alias('u')
                ->join(config('database.prefix').'user_level ul','u.level = ul.level_id','left')
                ->field('u.*,ul.level_name')
                ->where($where)
                ->order('u.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['reg_time'] = date('Y-m-d H:s',$v['reg_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }
    //设置会员状态
    public function usersState(){
        $id=input('post.id');
        $is_lock=input('post.is_lock');
        if(db('users')->where('id='.$id)->update(['is_lock'=>$is_lock])!==false){
            return ['status'=>1,'msg'=>'设置成功!'];
        }else{
            return ['status'=>0,'msg'=>'设置失败!'];
        }
    }

    /**
     * @return array
     * @author 北川
     * @time 2019/9/18 21:16
     * @comment　设置热门推荐
     */
    public function usersHot(){
        $id=input('post.id');
        $is_hot=input('post.is_hot');
        if(db('users')->where('id='.$id)->update(['is_hot'=>$is_hot])!==false){
            return ['code'=>1,'msg'=>'设置成功!'];
        }else{
            return ['code'=>0,'msg'=>'设置失败!'];
        }
    }
    public function edit($id=''){
        if(request()->isPost()){
            $id = input('id');
            $data = input('post.');
            unset($data['file']);
            $user_data = [
                'username' => $data['username'],
                'avatar' => $data['avatar'],
                'email' => $data['email'],
                'mobile' => $data['mobile'],
                'sex' => $data['sex'],
                'country' => $data['country'],
                'birthday' => $data['birthday'],
            ];
            if (empty($user_data['mobile']) && empty($user_data['email'])){
                gg(0,'电话和邮箱不能同时为空！');
            }
            if ($user_data['mobile']){
                $has_mobile = Db::name('users')->where([['mobile','eq',$data['mobile']]])->value('id');
                if ($has_mobile && $has_mobile != $id){
                    gg(0,'该手机号已被使用，请更换其他号码');
                }
            }
            if ($user_data['email']){
                $has_email = Db::name('users')->where([['email','eq',$data['email']]])->value('id');
                if ($has_email && $has_email != $id){
                    gg(0,'该邮箱已被使用，请更换其他邮箱');
                }
            }
            if(!empty($data['password'])){
                $user_data['password'] = md5($data['password']);
            }
            Db::startTrans();
            //保存基本信息到users表
            if ($id){
                $r1 = Db::name('users')->where([['id','eq',$id]])->update($user_data);
            }else{
                $r1 = Db::name('users')->insertGetId($user_data);
                $id= $r1;
            }
            if ($r1 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //保存简历信息到简历表
            $resume_data = [
                'uid' => $id,
                'education' => $data['education'],
                'tags' => implode(',',array_filter((array)$data['tags'])),
                'salary_min' => $data['salary_min'],
                'salary_max' => $data['salary_max'],
                'experience' => $data['experience'],
                'province' => $data['province'],
                'city' => $data['city'],
                'district' => $data['district'],
                'full_address' => $data['province'].' '.$data['city'].' '.$data['district'],
                'position_type' => $data['position_type'],
                'job_desc' => $data['job_desc'],
                'self_intro' => $data['self_intro'],
                'start_date' => $data['start_date'],
                'apartment' => $data['apartment'],
                'job_form' => $data['job_form'],
                'qualification' => implode(',',array_filter((array)$data['qualification'])),
            ];
            $resume_info = Db::name('resume')->where([['uid','eq',$id],['is_del','eq',0]])->find();
            if ($resume_info){
                $r2 = Db::name('resume')->where([['id','eq',$resume_info['id']]])->update($resume_data);
            }else{
                $r2 = Db::name('resume')->insertGetId($resume_data);
            }
            if ($r2 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //保存教育经历
            //先删除就记录
            $r3 = Db::name('education_exp')->where([['uid','eq',$id]])->delete();
            if ($r3 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //再插入记录
            if (isset($data['school'])){
                $edu_data = [];
                foreach ($data['school'] as $k=>$v){
                    $edu_data[] = [
                        'uid' => $id,
                        'school' => $v,
                        'major' => $data['major'][$k],
                        'reward' => $data['reward'][$k],
                    ];
                }
                $r4 = Db::name('education_exp')->insertAll($edu_data);
                if ($r4 === false){
                    Db::rollback();
                    gg(0,'操作失败');
                }
            }
            //保存工作经历
            //先删除就记录
            $r5 = Db::name('work_exp')->where([['uid','eq',$id]])->delete();
            if ($r5 === false){
                Db::rollback();
                gg(0,'操作失败');
            }
            //在保存记录
            if (isset($data['company'])){
                $edu_data = [];
                foreach ($data['company'] as $k=>$v){
                    $edu_data[] = [
                        'uid' => $id,
                        'company' => $v,
                        'position' => $data['position'][$k],
                        'desc' => $data['desc'][$k],
                    ];
                }
                $r6 = Db::name('work_exp')->insertAll($edu_data);
                if ($r6 === false){
                    Db::rollback();
                    gg(0,'操作失败');
                }
            }

            //提价表单
            Db::commit();
            gg(1,'操作成功');

        }else{
            $user_level=db('user_level')->order('sort')->select();
            $user_model = new \app\common\model\Users();

            $info = $user_model->getDetail($id);
            $this->assign('info',$info);

            $this->assign('edu_log',$info['edu_log']);
            $this->assign('work_log',$info['work_log']);
            //$this->assign('education',$this->getEduLevel());
            $this->assign('region_data',json_encode($this->allRegionData(2)));
            $this->assign('title',lang('edit').lang('user'));
            $this->assign('user_level',$user_level);
            $config = WebConfig::allConfig();

            $this->assign('tags',(arrColumn($config['tags'],'name')+$info['tags_arr']));
            $this->assign('industry',$config['industry']);
            $this->assign('education',$config['education']);
            $this->assign('position_type',$config['position_type']);
            $this->assign('qualification',(arrColumn($config['qualification'],'name')+$info['qualification']));
            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/8/5 23:43
     * @comment　简历投递记录
     */
    public function resumeLog(){
        if (request()->isPost()){
            $uid = input('id');
            $where = [];
            $where[] = ['a.uid','eq',$uid];
            if (input("key")){
                $where[] = ['b.username','like','%'.input('key').'%'];
            }
            $resume_po = new ResumePosition();
            $list = $resume_po->getLog($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view();
    }

    public function getRegion(){
        $Region=db("region");
        $pid = input("pid");
        $arr = explode(':',$pid);
        $map['pid']=$arr[1];
        $list=$Region->where($map)->select();
        return $list;
    }

    public function usersDel(){
        db('users')->delete(['id'=>input('id')]);
        db('oauth')->delete(['uid'=>input('id')]);
        return $result = ['code'=>1,'msg'=>'删除成功!'];
    }
    public function delall(){
        $map[] =array('id','IN',input('param.ids/a'));
        db('users')->where($map)->delete();
        $result['msg'] = '删除成功！';
        $result['code'] = 1;
        $result['url'] = url('index');
        return $result;
    }

    /***********************************会员组***********************************/
    public function userGroup(){
        if(request()->isPost()){
            $userLevel=db('user_level');
            $list=$userLevel->order('sort')->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch();
    }
    public function groupAdd(){
        if(request()->isPost()){
            $data = input('post.');
            db('user_level')->insert($data);
            $result['msg'] = '会员组添加成功!';
            $result['url'] = url('userGroup');
            $result['code'] = 1;
            return $result;
        }else{
            $this->assign('title',lang('add')."会员组");
            $this->assign('info','null');
            return $this->fetch('groupForm');
        }
    }
    public function groupEdit(){
        if(request()->isPost()) {
            $data = input('post.');
            db('user_level')->update($data);
            $result['msg'] = '会员组修改成功!';
            $result['url'] = url('userGroup');
            $result['code'] = 1;
            return $result;
        }else{
            $map['level_id'] = input('param.level_id');
            $info = db('user_level')->where($map)->find();
            $this->assign('title',lang('edit')."会员组");
            $this->assign('info',json_encode($info,true));
            return $this->fetch('groupForm');
        }
    }
    public function groupDel(){
        $level_id=input('level_id');
        if (empty($level_id)){
            return ['code'=>0,'msg'=>'会员组ID不存在！'];
        }
        db('user_level')->where(array('level_id'=>$level_id))->delete();
        return ['code'=>1,'msg'=>'删除成功！'];
    }
    public function groupOrder(){
        $userLevel=db('user_level');
        $data = input('post.');
        $userLevel->update($data);
        $result['msg'] = '排序更新成功!';
        $result['url'] = url('userGroup');
        $result['code'] = 1;
        return $result;
    }

    public function refund(){
        $data = input('post.');
        $data['admin_id'] = session('aid');
        if ($data['money'] <= 0){
            return ['code'=>0,'msg'=>'退款金额要大于0元'];
        }
        $balance = Db::name('users')->where([['id','eq',$data['uid']]])->value('balance');
        if ($balance < $data['money']){
            return ['code'=>0,'msg'=>'该用户最多只能退款'.$balance.'元'];
        }
        Db::startTrans();
        $r = Db::name('users')->where([['id','eq',$data['uid']]])->setDec('balance',$data['money']);
        if (!$r){
            Db::rollback();
            return ['code'=>0,'msg'=>'退款失败'];
        }
        $data['status'] = 1;
        $data['create_time'] = $data['update_time'] =time();
        $r2 = Db::name('user_refund')->insert($data);
        if (!$r2){
            Db::rollback();
            return ['code'=>0,'msg'=>'退款失败'];
        }

        Db::commit();
        return ['code'=>1,'msg'=>'退款成功！'];


    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/6/14 23:05
     * @comment　用户优惠券列表
     */
    public function couponList(){
        if(request()->isPost()){
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('coupon'))>0){
                $where[] = ['name','like','%'.input('coupon').'%'];
            }
            $list=Db::name('user_coupon')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k]['start_time'] = date('Y-m-d',$v['start_time']);
                $list['data'][$k]['end_time'] = date('Y-m-d',$v['end_time']);
                $list['data'][$k]['used_time'] = $v['used_time']?date('Y-m-d H:i',$v['use_time']):'';
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    public function resumeInfo(){
        $uid = input('uid',0);
        $resume_model = new Resume();
        $resume_info = $resume_model->getResumeInfo($uid);
        $this->assign("info",json_encode($resume_info));
        //$education = $this->getEduLevel();
        $this->assign('education',$this->getEduLevel());
        $this->assign('region_data',json_encode($this->allRegionData()));
        return view();
    }

    /**
     * @author 北川
     * @time 2019/10/22 23:49
     * @comment　修改积分
     */
    public function pointEdit(){
        if(request()->isPost()){
            $param = input('post.');
            $param['amount'] = (int)$param['amount'];
            $point = Db::name('users')->where('id',$param['uid'])->value('point');
            $amount = $param['amount']*pow(-1,$param['type']+1);
            $after = $amount + $point;
            if ($after <0){
                gg(0,'最多只能减少'.$point.'点积分');
            }
            Db::name('users')->where('id',$param['uid'])->setInc('point',$amount);
            $param['balance'] = $after;
            $param['cate'] = 2;
            $param['create_time'] = $param['update_time'] = date('Y-m-d H:i:s');
            $r = Db::name('point_detail')->insert($param);
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }
        return $this->fetch();
    }


    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/11/7 22:40
     * @comment　积分记录
     */
    public function pointList(){
        if(request()->isPost()){
            $where = [];
//            $where[] = ['is_del','eq',0];
            if (strlen(input('uid'))>0){
                $where[] = ['a.uid','eq',input('uid')];
            }
            $list=Db::name('point_detail')
                ->alias('a')
                ->join('users b','a.uid=b.id')
                ->field('a.*,b.username,b.type user_type,b.email,b.mobile')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $v['type_text'] = ($v['user_type']==1)?'求职者':'企业';
                if ($v['type'] == 1){
                    $v['amount'] = '+ '.$v['amount'];
                }else{
                    $v['amount'] = '- '.$v['amount'];

                }
                $list['data'][$k] = $v;
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }
}