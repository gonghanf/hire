<?php
/**
 * Author: 北川
 * Time: 2019/8/11 20:58
 * @comment　
 */

namespace app\admin\controller;


use app\pub\controller\Time;
use think\Db;

class Meeting extends Common
{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/11 21:02
     * @comment　招聘会列表
     */
    public function meetingList(){
        if(request()->isPost()){
            $key=input('post.key');
            $where = [];
            if ($key){
                $where[] = ['name|editor','like',"%".$key."%"];
            }
            if (input('status')){
                $where[] = ['status','eq',input('status')];
            }
            $meeting_model = new \app\common\model\Meeting();
            $list =  $meeting_model->meetingList($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    //设置招聘会状态
    public function meetingState(){
        $id=input('post.id');
        $status=input('post.status',0);
        if(Db::name('meeting')->where('id='.$id)->update(['status'=>$status])!==false){
            gg(1,'设置成功');
        }else{
            gg(0,'设置失败');
        }
    }

    /**
     * @param string $id
     * @return mixed
     * @author 北川
     * @time 2019/8/11 14:53
     * @comment　添加编辑公司信息
     */
    public function addEditMeeting(){
        $id = input('id',0);
        if(request()->isPost()){
            $data = input('post.');
            unset($data['file']);
            $data['start_time'] = strtotime($data['start_time']);
            $data['end_time'] = strtotime($data['end_time']);
            $data['update_time'] = time();
            //保存基本信息到users表
            if ($id){
                $r1 = Db::name('meeting')->where([['id','eq',$id]])->update($data);
            }else{
                $data['create_time'] = time();

                $r1 = Db::name('meeting')->insertGetId($data);
            }
            if ($r1){
                gg(1,'操作成功');

            }else{
                gg(0,'操作失败');
            }
        }else{

            $user_model = new \app\common\model\Meeting();
            $info = $user_model->meetingInfo([['id','eq',$id]]);
            $info= $info?:[];
            $this->assign('info',json_encode($info,true));

            $this->assign('region_data',json_encode($this->allRegionData()));

            return $this->fetch();
        }
    }
    /**
     * @author 北川
     * @time 2019/8/11 15:41
     * @comment　删除招聘会
     */
    public function meetingDel(){
        $id = input('id',0);
        $r = Db::name('meeting')->where([['id','eq',$id]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

}