<?php
/**
 * Author: 北川
 * Time: 2019/6/12 15:42
 * @comment　
 */

namespace app\admin\controller;


use think\Db;

class Station extends Common
{
    /**
 * @return array|mixed
 * @author 北川
 * @time 2019/5/14 16:33
 * @comment　加油站列表
 */
    public function stationList(){
        if(request()->isPost()){
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['name|address','like','%'.input('keyword').'%'];
            }
            $list=Db::name('station')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/5/14 16:34
     * @comment　添加编辑加油站
     */
    public function addEditStation(){
        if(request()->isPost()){
            $param = input('post.');
            $param['update_time'] = time();
            if (input('id')){
                $r = Db::name('station')->where([['id','eq',input('id')]])->update($param);
            }else{
                $param['create_time'] = time();
                $r = Db::name('station')->insert($param);
            }
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }else{
            if (input('id')){
                $info = Db::name('station')->where([['id','eq',input('id')]])->find();
                $info['start_time'] = date("Y-m-d",$info['start_time']);
                $info['end_time'] = date("Y-m-d",$info['end_time']);
            }else{
                $info = [];
            }
            $this->assign('info',json_encode((object)$info));
            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/5/21 18:20
     * @comment　删除加油站
     */
    public function delStation(){

        $r = Db::name('station')->where([['id','eq',input('id')]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/5/14 16:33
     * @comment　油品列表
     */
    public function gasList(){
        if(request()->isPost()){
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['name|address','like','%'.input('keyword').'%'];
            }
            $list=Db::name('gas_type')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();

            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/5/14 16:34
     * @comment　添加编辑加油站
     */
    public function addEditGas(){
        if(request()->isPost()){
            $param = input('post.');
            $param['update_time'] = time();
            if (input('id')){
                $r = Db::name('gas_type')->where([['id','eq',input('id')]])->update($param);
            }else{
                //$param['create_time'] = time();
                $r = Db::name('gas_type')->insert($param);
            }
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }else{
            if (input('id')){
                $info = Db::name('gas_type')->where([['id','eq',input('id')]])->find();
            }else{
                $info = [];
            }
            $this->assign('info',json_encode((object)$info));
            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/5/21 18:20
     * @comment　删除加油站
     */
    public function delGas(){

        $r = Db::name('gas_type')->where([['id','eq',input('id')]])->delete();
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }
}