<?php
namespace app\admin\controller;
use app\admin\model\Users as UsersModel;
use app\common\controller\GG;
use think\console\Input;
use think\Db;

class Staff extends Common{
    //会员列表
    public function applyList(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['staff_name|staff_mobile','like','%'.input('keyword').'%'];
            }
            $list=Db::name('staff_apply')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:s',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/6/15 0:17
     * @comment　员工申请审核
     */
    public function staffAudit(){
        $id = input('id');
        $op = input('op');
        $remark = input('remark');
        $apply = Db::name('staff_apply')->where([['id','eq',$id]])->find();
        if (!$apply){
            gg(0,'数据异常');
        }
        if ($apply['status'] != 0){
            gg(0,'该申请已经审核过，不可重复审核');
        }
        Db::startTrans();
        $r = Db::name('staff_apply')->where([['id','eq',$id]])->update(['status'=>$op,'remark'=>$remark]);
        if (!$r){
            Db::rollback();
            gg(0,'操作失败');
        }
        //审核通过则插入一条数据
        if ($op == 1){
            $indata = [
                'uid' => $apply['uid'],
                'station_name' => $apply['station_name'],
                'station_id' => $apply['station_id'],
                'staff_name' => $apply['staff_name'],
                'staff_mobile' => $apply['staff_mobile'],
                'status' => 1,
                'create_time' => time(),
            ];
            $r2 = Db::name('staff')->insert($indata);
            if (!$r2){
                Db::rollback();
                gg(0,'操作失败');
            }
        }
        Db::commit();
        gg(1,'操作成功');
    }
    public function staffList(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['staff_name|staff_mobile','like','%'.input('keyword').'%'];
            }
            $list=Db::name('staff')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:s',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/6/15 0:33
     * @comment　删除申请记录
     */
    public function applyDel(){
        $r = Db::name('staffApply')->where([['id','eq',input('id')]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    public function staffState(){
        $r = Db::name('staff')->where([['id','eq',input('id')]])->setField('status',input('status'));
        if ($r === false){
            gg(0,'操作失败');
        }else{
            gg(1,'操作成功');
        }
    }
}