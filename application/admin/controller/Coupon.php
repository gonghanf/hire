<?php
/**
 * Author: 北川
 * Time: 2019/6/11 21:47
 * @comment　
 */

namespace app\admin\controller;


use think\Db;

class Coupon extends Common
{

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/5/14 16:33
     * @comment　优惠券列表
     */
    public function couponList(){
        if(request()->isPost()){
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('coupon'))>0){
                $where[] = ['name','like','%'.input('coupon').'%'];
            }
            $list=Db::name('coupon')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k]['start_time'] = date('Y-m-d',$v['start_time']);
                $list['data'][$k]['end_time'] = date('Y-m-d',$v['end_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/5/14 16:34
     * @comment　添加编辑优惠券
     */
    public function addEditCoupon(){
        if(request()->isPost()){
            $id = input('id');
            $param = input('post.');
            $param['update_time'] = time();
            $param['start_time'] = strtotime($param['start_time']);
            $param['end_time'] = strtotime($param['end_time']);
            $station_id = $param['station_id'];
            unset($param['station_id']);
            if (input('id')){
                $r = Db::name('coupon')->where([['id','eq',input('id')]])->update($param);
            }else{
                $r = Db::name('coupon')->insertGetId($param);
                $id = $r;
            }
            if ($r){
                Db::name('coupon_station')->where([['coupon_id','eq',$id]])->delete();
                $data = [];
                foreach ($station_id as $k=>$v){
                    $p = [];
                    $p['station_id'] = $v;
                    $p['coupon_id'] = $id;
                    $data[] = $p;
                }
                Db::name('coupon_station')->insertAll($data);
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }else{
            if (input('id')){
                $info = Db::name('coupon')->where([['id','eq',input('id')]])->find();
                $info['start_time'] = date("Y-m-d",$info['start_time']);
                $info['end_time'] = date("Y-m-d",$info['end_time']);
                $station = Db::name('coupon_station')
                    ->alias('a')
                    ->leftJoin('station b','a.station_id=b.id')
                    ->field('b.*')
                    ->where([['coupon_id','eq',input('id')]])
                    ->select();
                $this->assign('station',$station);
            }else{
                $info = [];
            }
            $this->assign('info',json_encode((object)$info));
            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/5/21 18:20
     * @comment　删除优惠券
     */
    public function delCoupon(){

        $r = Db::name('coupon')->where([['id','eq',input('id')]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/6/14 8:50
     * @comment　赠送优惠券
     */
    public function sendCoupon(){
        if(request()->isPost()){
            $uid = input('uid');
            $all = input('all');
            if (!$all&&!$uid){
                gg(0,'请选择赠送会员');
            }
            $coupon_id = input('coupon_id');
            $where = [];
            $where[] = ['is_lock','eq',0];
            if ($all){
                $uid = Db::name('users')->where($where)->column('id');
            }
            $coupon_info = Db::name('coupon')->where([['id','eq',$coupon_id]])->find();
            $indata = [];
            foreach ($uid as $k=>$v){
                $p = [];
                $p['uid'] = $v;
                $p['coupon_id'] = $coupon_info['id'];
                $p['name'] = $coupon_info['name'];
                $p['content'] = $coupon_info['content'];
                $p['period'] = $coupon_info['period'];
                $p['start_time'] = $coupon_info['start_time'];
                $p['end_time'] = $coupon_info['end_time'];
                $p['get_method'] = 'platform';
                $p['used'] = 0;
                $p['is_expire'] = 0;
                $p['create_time'] = time();
                $indata[] = $p;
            }
            $r = Db::name('user_coupon')->insertAll($indata);
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }
        $info= Db::name('coupon')->where([['id','eq',input('id')]])->find();
        $this->assign('info',json_encode($info));
        return $this->fetch();
    }

    public function userList(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $list=db('users')->alias('u')
                ->join(config('database.prefix').'user_level ul','u.level = ul.level_id','left')
                ->field('u.*,ul.level_name')
                ->where('u.email|u.mobile|u.username','like',"%".$key."%")
                ->order('u.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['reg_time'] = date('Y-m-d H:s',$v['reg_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/5/14 16:33
     * @comment　员工申请赠送优惠券
     */
    public function staffSendCoupon(){
        if(request()->isPost()){
            $where = [];
            $where[] = ['is_del','eq',0];
            if (strlen(input('staff'))>0){
                $where[] = ['b.staff_name|b.staff_mobile','like','%'.input('staff').'%'];
            }
            if (strlen(input('station'))>0){
                $where[] = ['b.station_name','like','%'.input('station').'%'];
            }
            if (strlen(input('coupon_name'))>0){
                $where[] = ['a.coupon_name','like','%'.input('coupon_name').'%'];
            }
            if (strlen(input('user'))>0){
                $where[] = ['c.username|c.mobile','like','%'.input('user').'%'];
            }
            $list=Db::name('staff_send_coupon')
                ->alias('a')
                ->leftJoin('staff b','a.staff_uid=b.uid')
                ->leftJoin('users c','a.staff_uid=c.id')
                ->field('a.*,b.staff_name,b.staff_mobile,b.station_name,c.username,c.mobile')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $list['data'][$k]['start_time'] = date('Y-m-d',$v['start_time']);
                $list['data'][$k]['end_time'] = date('Y-m-d',$v['end_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/6/16 19:33
     * @comment　员工申请赠送优惠券审核
     */
    public function staffSendAudit(){
        $id = input('id');
        $op = input('op');
        $remark = input('remark');
        $apply = Db::name('staff_send_coupon')->where([['id','eq',$id]])->find();
        if (!$apply){
            gg(0,'数据异常');
        }
        if ($apply['status'] != 0){
            gg(0,'该申请已经审核过，不可重复审核');
        }
        Db::startTrans();
        $r = Db::name('staff_send_coupon')->where([['id','eq',$id]])->update(['status'=>$op,'op_remark'=>$remark]);
        if (!$r){
            Db::rollback();
            gg(0,'操作失败');
        }
        //审核通过则插入一条信息
        if ($op == 1){
            $coupon = Db::name('coupon')->where([['id','eq',$apply['coupon_id']]])->find();
            if ($coupon){
                $indata = [
                    'uid' => $apply['uid'],
                    'coupon_id' => $coupon['id'],
                    'name' => $coupon['name'],
                    'content' => $coupon['content'],
                    'period' => $coupon['period'],
                    'start_time' => $coupon['start_time'],
                    'end_time' => $coupon['end_time'],
                    'used' => 0,
                    'is_expire' => 0,
                    'is_del' => 0,
                    'get_method' => 'staff',
                    'create_time' => time(),
                ];
                $r2 = Db::name('user_coupon')->insert($indata);
                if (!$r2){
                    Db::rollback();
                    gg(0,'操作失败');
                }
            }else{
                Db::rollback();
                gg(0,'优惠券信息异常');
            }
        }
        Db::commit();
        gg(1,'操作成功');
    }

    /**
     * @author 北川
     * @time 2019/6/16 19:35
     * @comment　员工申请赠送删除
     */
    public function staffSendDel(){
        $r = Db::name('staff_send_coupon')->where([['id','eq',input('id')]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/6/21 22:40
     * @comment　添加加油站
     */
    public function addStation(){
        if(request()->isPost()){
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('keyword'))>0){
                $where[] = ['name|address','like','%'.input('keyword').'%'];
            }

            //$ids = Db::name('coupon_station')->where([['coupon_id','eq',input('id')]])->coloum('station_id');
            $ids = input('select_ids');
            if ($ids){
                $where[] = ['id','NOT IN',$ids];
            }
            $list=Db::name('station')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }



}