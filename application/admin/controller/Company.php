<?php
namespace app\admin\controller;
use app\common\model\Position;
use app\common\model\Resume;
use app\common\model\ResumePosition;
use app\common\model\Service;
use app\common\model\WebConfig;
use app\pub\controller\Time;
use think\console\Input;
use think\Db;

class Company extends Common{
    //企业列表
    public function companyList(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $where = [];
            //$where[] = ['type','eq',2];//企业招聘者
            $where[] = ['type','in',[2,3]];//企业招聘者
            if ($key){
                $where[] = ['u.email|u.mobile|u.username','like',"%".$key."%"];
            }
            $list=db('users')->alias('u')
                ->join(config('database.prefix').'user_level ul','u.level = ul.level_id','left')
                ->field('u.*,ul.level_name')
                ->where($where)
                ->order('u.id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['reg_time'] = date('Y-m-d H:s',$v['reg_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }
    //设置会员状态
    public function usersState(){
        $id=input('post.id');
        $is_lock=input('post.is_lock');
        if(db('users')->where('id='.$id)->update(['is_lock'=>$is_lock])!==false){
            return ['status'=>1,'msg'=>'设置成功!'];
        }else{
            return ['status'=>0,'msg'=>'设置失败!'];
        }
    }
    /**
     * @return array
     * @author 北川
     * @time 2019/9/18 21:16
     * @comment　设置热门推荐
     */
    public function usersHot(){
        $id=input('post.id');
        $is_hot=input('post.is_hot');
        if(db('users')->where('id='.$id)->update(['is_hot'=>$is_hot])!==false){
            return ['code'=>1,'msg'=>'设置成功!'];
        }else{
            return ['code'=>0,'msg'=>'设置失败!'];
        }
    }

    /**
     * @param string $id
     * @return mixed
     * @author 北川
     * @time 2019/8/11 14:53
     * @comment　添加编辑公司信息
     */
    public function edit($id=''){
        if(request()->isPost()){
            $id = input('id');
            $data = input('post.');
            unset($data['file']);
            $user_data = [
                'username' => $data['username'],
                'avatar' => $data['avatar'],
                'email' => $data['email'],
                'mobile' => $data['mobile'],
                'industry' => $data['industry'],
                'scale' => $data['scale'],
                'desc' => $data['desc'],
                'licence' => $data['licence'],
                'update_time' => time()
            ];
            if (empty($user_data['mobile']) && empty($user_data['email'])){
                gg(0,'电话和邮箱不能同时为空！');
            }
            if ($user_data['mobile']){
                $has_mobile = Db::name('users')->where([['mobile','eq',$data['mobile']]])->value('id');
                if ($has_mobile && $has_mobile != $id){
                    gg(0,'该手机号已被使用，请更换其他号码');
                }
            }
            if ($user_data['email']){
                $has_email = Db::name('users')->where([['email','eq',$data['email']]])->value('id');
                if ($has_email && $has_email != $id){
                    gg(0,'该邮箱已被使用，请更换其他邮箱');
                }
            }
            if(!empty($data['password'])){
                $user_data['password'] = md5($data['password']);
            }
            //保存基本信息到users表
            if ($id){
                $r1 = Db::name('users')->where([['id','eq',$id]])->update($user_data);
            }else{
                $r1 = Db::name('users')->insertGetId($user_data);
            }
            if ($r1){
                gg(1,'操作成功');

            }else{
                gg(0,'操作失败');

            }

        }else{
            $user_level=db('user_level')->order('sort')->select();
            $user_model = new \app\common\model\Users();

            $info = $user_model->getDetail($id);
            $this->assign('info',json_encode($info,true));

            $config = WebConfig::allConfig();

            $this->assign('edu_log',$info['edu_log']);
            $this->assign('work_log',$info['work_log']);
            $this->assign('education',$config['education']);
            $this->assign('industry',$config['industry']);
            $this->assign('region_data',json_encode($this->allRegionData()));
            $this->assign('title',lang('edit').lang('user'));
            $this->assign('user_level',$user_level);

            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/8/5 23:43
     * @comment　简历库
     */
    public function resumeRepository(){
        if (request()->isPost()){
            $company = input('id');
            $where = [];
            $where[] = ['a.company_id','eq',$company];
            if (input("key")){
                $where[] = ['c.username|c.mobile|c.email|c.sn','like','%'.input('key').'%'];
            }
            $resume_po = new ResumePosition();
            $list = $resume_po->getResumePosition($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view();
    }

    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/11 12:08
     * @comment　职位列表
     */
    public function positionList(){
        if (request()->isPost()){
            $company = input('id');
            $where = [];
            $where[] = ['a.company_id','eq',$company];

            if (input("status")){
                $where[] = ['a.status','eq',input('status')];
            }
            $resume_po = new Position();
            $list = $resume_po->positionList($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view();
    }
    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/11 12:08
     * @comment　全部职位列表
     */
    public function allPosition(){
        if (request()->isPost()){

            if (input("status")){
                $where[] = ['a.status','eq',input('status')];
            }
            $resume_po = new Position();
            $list = $resume_po->positionList($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view();
    }
    /**
     * @return array
     * @author 北川
     * @time 2019/10/29 23:04
     * @comment　审核
     */
    public function positionVerify(){

        $r = Db::name('position')->where('id',input('id',0))->setField('verify',input('status'));
        if ($r !== false){
            return api_return(1,'操作成功');
        }else{
            return api_return(0,'操作失败');
        }
    }
    /**
     * @return array
     * @author 北川
     * @time 2019/9/18 21:16
     * @comment　设置热门推荐
     */
    public function positionHot(){
        $id=input('post.id');
        $is_hot=input('post.is_hot');
        if(db('position')->where('id='.$id)->update(['is_hot'=>$is_hot])!==false){
            return ['code'=>1,'msg'=>'设置成功!'];
        }else{
            return ['code'=>0,'msg'=>'设置失败!'];
        }
    }
    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/11 15:26
     * @comment　编辑添加职位
     */
    public function addEditPosition(){
        if(request()->isPost()){
            $id = input('id');
            $data = input('post.');
            $data['salary'] = floatval($data['salary_min']).'K'.'~'.floatval($data['salary_max']).'K';
            $data['full_address'] = $data['province'].' '.$data['city'].' '.$data['district'];

            $data['tags'] = implode(',',array_filter((array)$data['tags']));
            $data['qualification'] = implode(',',array_filter((array)$data['qualification']));
            $data['welfare'] = implode(',',array_filter((array)$data['welfare']));
            $data['update_time'] = time();
            if ($id){
                $r1 = Db::name('position')->where([['id','eq',$id]])->update($data);
            }else{
                $data['create_time'] = time();
                $r1 = Db::name('position')->insertGetId($data);
            }
            if ($r1){
                gg(1,'操作成功');

            }else{
                gg(0,'操作失败');

            }

        }else{
            $id = input('id',0);
            $where = [];
            $where[] = ['id','eq',$id];
            $position_model = new Position();
            $info = $position_model->positionInfo($where);
            $info = $info?:[];

            $this->assign('info',$info);
            $config = WebConfig::allConfig();
            $this->assign('tags',(arrColumn($config['tags'],'name')+$info['tags_arr']));
            $this->assign('experience',$config['experience']);
            $this->assign('welfare',$config['welfare']);
            $this->assign('education',$config['education']);
            $this->assign('position_type',$config['position_type']);
            $this->assign('qualification',(arrColumn($config['qualification'],'name')+$info['qual_arr']));


            $this->assign('region_data',json_encode($this->allRegionData(2)));

            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/8/11 15:39
     * @comment　修改职位在线状态
     */
    public function changePositionStatus(){
        $id = input('id',0);
        $status = input('status');
        $r = Db::name('position')->where([['id','eq',$id]])->update(['status'=>$status,'update_time'=>time()]);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/11 15:41
     * @comment　删除职位
     */
    public function positionDel(){
        $id = input('id',0);
        $r = Db::name('position')->where([['id','eq',$id]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    public function getRegion(){
        $Region=db("region");
        $pid = input("pid");
        $arr = explode(':',$pid);
        $map['pid']=$arr[1];
        $list=$Region->where($map)->select();
        return $list;
    }



    public function usersDel(){
        db('users')->delete(['id'=>input('id')]);
        db('oauth')->delete(['uid'=>input('id')]);
        return $result = ['code'=>1,'msg'=>'删除成功!'];
    }
    public function delall(){
        $map[] =array('id','IN',input('param.ids/a'));
        db('users')->where($map)->delete();
        $result['msg'] = '删除成功！';
        $result['code'] = 1;
        $result['url'] = url('index');
        return $result;
    }

    /***********************************会员组***********************************/
    public function userGroup(){
        if(request()->isPost()){
            $userLevel=db('user_level');
            $list=$userLevel->order('sort')->select();
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list,'rel'=>1];
        }
        return $this->fetch();
    }
    public function groupAdd(){
        if(request()->isPost()){
            $data = input('post.');
            db('user_level')->insert($data);
            $result['msg'] = '会员组添加成功!';
            $result['url'] = url('userGroup');
            $result['code'] = 1;
            return $result;
        }else{
            $this->assign('title',lang('add')."会员组");
            $this->assign('info','null');
            return $this->fetch('groupForm');
        }
    }
    public function groupEdit(){
        if(request()->isPost()) {
            $data = input('post.');
            db('user_level')->update($data);
            $result['msg'] = '会员组修改成功!';
            $result['url'] = url('userGroup');
            $result['code'] = 1;
            return $result;
        }else{
            $map['level_id'] = input('param.level_id');
            $info = db('user_level')->where($map)->find();
            $this->assign('title',lang('edit')."会员组");
            $this->assign('info',json_encode($info,true));
            return $this->fetch('groupForm');
        }
    }
    public function groupDel(){
        $level_id=input('level_id');
        if (empty($level_id)){
            return ['code'=>0,'msg'=>'会员组ID不存在！'];
        }
        db('user_level')->where(array('level_id'=>$level_id))->delete();
        return ['code'=>1,'msg'=>'删除成功！'];
    }
    public function groupOrder(){
        $userLevel=db('user_level');
        $data = input('post.');
        $userLevel->update($data);
        $result['msg'] = '排序更新成功!';
        $result['url'] = url('userGroup');
        $result['code'] = 1;
        return $result;
    }



    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/6 22:45
     * @comment　企业申请审核列表
     */
    public function companyAppList(){
        if(request()->isPost()){
            $key=input('post.key');
            $page =input('page')?input('page'):1;
            $pageSize =input('limit')?input('limit'):config('pageSize');
            $where = [];
            //$where[] = ['type','eq',2];//企业招聘者
            if ($key){
                $where[] = ['email|mobile|username','like',"%".$key."%"];
            }
            $list=Db::name('company_app')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:s',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @return \think\response\View
     * @author 北川
     * @time 2019/8/6 22:46
     * @comment　企业申请详情
     */
    public function companyAppDetail(){
        $id = input('id');
        $info = Db::name('company_app')->where([['id','eq',$id]])->find();
        $this->assign('info',json_encode($info));
        return view();
    }

    /**
     * @author 北川
     * @time 2019/8/6 22:56
     * @comment　企业申请审核
     */
    public function companyAppAudit(){
        $id = input('id');
        $status = input('status');
        $remark = input('remark');
        $app_info = Db::name('company_app')->where([['id','eq',$id]])->find();
        if (!$app_info){
            gg(0,'数据异常,操作失败');
        }
        if($app_info['status'] > 0){
            gg(0,'该记录已审核,请勿重复审核');
        }
        if (empty($app_info['mobile']) && empty($app_info['email'])){
            gg(0,'电话和邮箱不能同时为空，请驳回该申请！');
        }
        if ($app_info['mobile']){
            $has_mobile = Db::name('users')->where([['mobile','eq',$app_info['mobile']]])->value('id');
            if ($has_mobile){
                gg(0,'该手机号已被使用，请驳回申请');
            }
        }
        if ($app_info['email']){
            $has_email = Db::name('users')->where([['email','eq',$app_info['email']]])->value('id');
            if ($has_email){
                gg(0,'该邮箱已被使用，请驳回申请');
            }
        }
        //如果通过
        if ($status == 10 ){
            Db::startTrans();
            //修改申请表状态
            $r1 = Db::name('company_app')->where([['id','eq',$id]])->update(['status'=>$status,'remark'=>$remark,'update_time'=>time()]);
            if (!$r1){
                Db::rollback();
                gg(0,'操作失败');
            }
            $sn_info = create_user_sn(2);
            //插入用户表
            $indata = [
                'sn' => $sn_info['sn'],
                'no' => $sn_info['no'],
                'username' => $app_info['username'],
                'email' => $app_info['email'],
                'password' => $app_info['password'],
                'avatar' => $app_info['avatar'],
                'mobile' => $app_info['mobile'],
                'scale' => $app_info['scale'],
                'industry' => $app_info['industry'],
                'desc' => $app_info['desc'],
                'type' => 2,
                'is_lock' => 0,
                'level' => 1,
                'reg_time' => time(),
                'create_time' => time(),
                'update_time' => time(),
            ];
            $r2 = Db::name('users')->insertGetId($indata);
            if (!$r2){
                Db::rollback();
                gg(0,'操作失败');
            }
            Db::commit();
            gg(1,'操作成功',$r2);
        }else{
            //驳回   更新申请表
            $r = Db::name('company_app')->where([['id','eq',$id]])->update(['status'=>$status,'remark'=>$remark,'update_time'=>time()]);
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }
    }


    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/11 12:08
     * @comment　签证服务列表
     */
    public function serviceList(){
        if (request()->isPost()){
            $where = [];
            if (input('status')){
                $where[] = ['a.status','eq',input('status')];
            }else{
                $where[] = ['a.status','egt',1];//审核通过的
            }
            if (input("service")){
                $where[] = ['title','like','%'.input('service').'%'];
            }
            if (input("company")){
                $where[] = ['b.username|b.mobile|b.email|b.sn','like','%'.input('company').'%'];
            }
            $service_model = new Service();
            $list = $service_model->serviceList($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view();
    }

    /**
     * @author 北川
     * @time 2019/8/11 15:39
     * @comment　修改签证服务状态
     */
    public function changeServiceStatus(){
        $id = input('id',0);
        $status = input('status');
        $data = [
            'status' => $status,
            'update_time' => time()
        ];
        if (input('remark')){
            $data['remark'] = input('remark');
        }
        $r = Db::name('service')->where([['id','eq',$id]])->update($data);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

    /**
     * @return \think\response\View
     * @author 北川
     * @time 2019/8/11 19:45
     * @comment　签证服务详情
     */
    public function serviceInfo(){
        $id = input('id',0);
        $info = Db::name('service')->where([['id','eq',$id]])->find();
        $this->assign('info',$info);
        return view();
    }
    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/11 12:08
     * @comment　签证服务审核列表
     */
    public function serviceAppList(){
        if (request()->isPost()){
            $where = [];
            $where[] = ['a.status','eq',0];//待审核

//            if (input('status')){
//                $where[] = ['a.status','eq',input('status')];
//            }else{
//                $where[] = ['a.status','egt',1];//审核通过的
//            }
            if (input("service")){
                $where[] = ['title','like','%'.input('service').'%'];
            }
            if (input("company")){
                $where[] = ['b.username|b.mobile|b.email|b.sn','like','%'.input('company').'%'];
            }
            $service_model = new Service();
            $list = $service_model->serviceList($where,$this->page,$this->pageSize);
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return view();
    }

    /**
     * @return \think\response\View
     * @author 北川
     * @time 2019/8/11 19:45
     * @comment　签证服务审核详情
     */
    public function serviceAppInfo(){
        $id = input('id',0);
        $info = Db::name('service')->where([['id','eq',$id]])->find();
        $this->assign('info',$info);
        return view();
    }
    /**
     * @author 北川
     * @time 2019/8/11 15:41
     * @comment　删除服务
     */
    public function serviceDel(){
        $id = input('id',0);
        $r = Db::name('service')->where([['id','eq',$id]])->setField('is_del',1);
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }
}