<?php
namespace app\admin\controller;
use app\common\controller\GG;
use think\Db;
use think\Controller;
class Common extends GG
{
    protected $mod,$role,$system,$nav,$menudata,$cache_model,$categorys,$module,$moduleid,$adminRules,$HrefId,$pageSize,$page;
    public function initialize()
    {
        //判断管理员是否登录
        if (!session('aid')) {
            $this->redirect('admin/login/index');
        }
        define('MODULE_NAME',strtolower(request()->controller()));
        define('ACTION_NAME',strtolower(request()->action()));
        //权限管理
        //当前操作权限ID
        if(session('aid')!=1){
            $this->HrefId = db('auth_rule')->where('href',MODULE_NAME.'/'.ACTION_NAME)->value('id');
            //当前管理员权限
            $map['a.admin_id'] = session('aid');
            $rules=Db::table(config('database.prefix').'admin')->alias('a')
                ->join(config('database.prefix').'auth_group ag','a.group_id = ag.group_id','left')
                ->where($map)
                ->value('ag.rules');
            $this->adminRules = explode(',',$rules);
            if($this->HrefId){
                if(!in_array($this->HrefId,$this->adminRules)){
                    $this->error('您无此操作权限');
                }
            }
        }
        $this->cache_model=array('Module','AuthRule','Category','Posid','Field','System','cm');
        foreach($this->cache_model as $r){
            if(!cache($r)){
                savecache($r);
            }
        }
        $this->page =input('page')?input('page'):1;
        $this->pageSize =input('limit')?input('limit'):config('pageSize');
        $this->system = cache('System');
        $this->categorys = cache('Category');
        $this->module = cache('Module');
        $this->mod = cache('Mod');
        $this->rule = cache('AuthRule');
        $this->cm = cache('cm');
    }
    //空操作
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }

    public function regionData(){
        $region_data = cache('region_data');
        if ($region_data){
            return $region_data;
        }
        $prov = Db::name('region')->where([['type','eq',1]])->select();
        $region_data = [];
        foreach ($prov as $k=>$v){
            $p = [];
            $p['value'] = $v['name'];
            $p['label'] = $v['name'];
            $city = Db::name('region')->where([['pid','eq',$v['id']]])->select();
            foreach ($city as $k2=>$v2){
                $m = [];
                $m['value'] = $v2['name'];
                $m['label'] = $v2['name'];
                $dist = Db::name('region')->where([['pid','eq',$v2['id']]])->select();
                foreach ($dist as $k3=>$v3){
                    $n = [];
                    $n['value'] = $v3['name'];
                    $n['label'] = $v3['name'];
                    $n['children'] = [];
                    $m['children'][] = $n;
                }
                $p['children'][] = $m;
            }
            $region_data[] = $p;
        }
        cache('region_data',$region_data);
        return $region_data;
    }


}
