<?php
/**
 * Author: 北川
 * Time: 2019/6/12 15:42
 * @comment　
 */

namespace app\admin\controller;


use think\Db;

class Cart extends Common
{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/5/14 16:33
     * @comment　加油站列表
     */
    public function cartList(){
        if(request()->isPost()){
            $where = [];
            //$where[] = ['is_del','eq',0];
            if (strlen(input('cart'))>0){
                $where[] = ['name','like','%'.input('cart').'%'];
            }
            $list=Db::name('cart_brand')
                ->where($where)
                ->order('id desc')
                ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
                ->toArray();
            foreach ($list['data'] as $k=>$v){
                $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            return $result = ['code'=>0,'msg'=>'获取成功!','data'=>$list['data'],'count'=>$list['total'],'rel'=>1];
        }
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/5/14 16:34
     * @comment　添加编辑加油站
     */
    public function addEditCart(){
        if(request()->isPost()){
            $param = input('post.');
            $param['update_time'] = time();
            if (input('id')){
                $r = Db::name('cart_brand')->where([['id','eq',input('id')]])->update($param);
            }else{
                $param['create_time'] = time();
                $r = Db::name('cart_brand')->insert($param);
            }
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }else{
            if (input('id')){
                $info = Db::name('cart_brand')->where([['id','eq',input('id')]])->find();
                $info['start_time'] = date("Y-m-d",$info['start_time']);
                $info['end_time'] = date("Y-m-d",$info['end_time']);
            }else{
                $info = [];
            }
            $this->assign('info',json_encode((object)$info));
            return $this->fetch();
        }
    }

    /**
     * @author 北川
     * @time 2019/5/21 18:20
     * @comment　删除加油站
     */
    public function delCart(){

        $r = Db::name('cart_brand')->where([['id','eq',input('id')]])->delete();
        if ($r){
            gg(1,'操作成功');
        }else{
            gg(0,'操作失败');
        }
    }

}