<?php
/**
 * Author: 北川
 * Time: 2019/6/20 23:02
 * @comment　
 */

namespace app\admin\model;


use think\Db;
use think\Model;

class BalanceDetail extends Model
{
    /**
     * @param $uid
     * @param $order_id
     * @param $type
     * @param $money
     * @return int|string
     * @author 北川
     * @time 2019/6/20 23:05
     * @comment　记录用户资金详情
     */
    public static function addBalanceLog($uid,$order_id,$type,$money){
        $indata = [
            'uid' =>$uid,
            'type' =>$type,
            'money' =>$money,
            'order_id' =>$order_id,
        ];
        $r = Db::name('balance_detail')->insertGetId($indata);
        return $r;
    }

}