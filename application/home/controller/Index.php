<?php
/**
 * Author: 北川
 * Time: 2019/8/14 21:49
 * @comment　
 */

namespace app\home\controller;


use app\common\model\Collection;
use app\common\model\Position;
use app\common\model\Users;
use app\common\model\WebConfig;
use think\Db;

class Index extends Common
{
    /**
     * @author 北川
     * @time 2019/8/14 21:51
     * @comment　大厅首页
     */
    public function index(){
        $banner = Db::name('ad')->where([['as_id','eq',1],['open','eq',1]])->select();
        $hot_position = Db::name('position')->where([['is_del','eq',0],['is_hot','eq',1],['status','eq',1]])->select();
        $field = 'id,avatar,username,industry,desc';
        $hot_company = Db::name('users')->field($field)->where([['is_hot','eq',1],['status','eq',1],['type','eq',2]])->select();
        //$collectionList = Db::name('collection')->where([['uid','eq',$this->uid]])->select();
        $col_arr = Collection::collectionArr($this->uid);
        foreach ($hot_position as $key => $o) {
            $hot_position[$key]['is_collection'] = $col_arr[2][$o['id']]?1:0;
            $company = Db::name('users')->where([['id','eq',$o['company_id']]])->find();
            $hot_position[$key]['company'] = $company;
        }
        foreach ($hot_company as $key => $o) {
            $hot_company[$key]['is_collection'] = $col_arr[1][$o['id']]?1:0;
            $hot_company[$key]['position_count'] = Db::name('position')->where([['company_id','eq',$o['id']]])->count();
        }

        session('nav', 'home');
        $config = WebConfig::allConfig();

        $this->assign('config', $config);
        $this->assign('user_type', $this->user_type);
        //$this->assign('collection_list', $collectionList);
        $this->assign('banner',$banner);
        $this->assign('hot_position',$hot_position);
        $this->assign('hot_company',$hot_company);
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/14 22:47
     * @comment　所有职位
     */
    public function position(){
        $keyword = input('keyword', '');
        $config = WebConfig::allConfig();

        session('nav', 'position');
        $this->assign('education', $config['education']);
        $this->assign('experience', $config['experience']);
        $this->assign('position_type', $config['position_type']);
        $this->assign('keyword', $keyword);

        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/14 22:47
     * @comment　所有人才
     */
    public function person(){
        $keyword = input('keyword', '');
        $config = WebConfig::allConfig();
        $positionList = Db::name('position')->where([['company_id','eq',$this->uid],['is_del','eq',0]])->select();

        session('nav', 'person');
        $this->assign('positionList', $positionList);
        $this->assign('education', $config['education']);
        $this->assign('experience', $config['experience']);
        $this->assign('position_type', $config['position_type']);
        $this->assign('keyword', $keyword);
        $this->assign('province', Db::name('region')->where('type',1)->select());

        return $this->fetch();
    }
    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/16 23:14
     * @comment　人才详情
     */
    public function personInfo(){
        $user_id = input('user_id',0);
        if(!$user_id) {
            return $this->redirect('userList');
        }
        $this->assign('user_id', $user_id);
        return $this->fetch();
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/14 23:21
     * @comment　职位详情
     */
    public function positionInfo(){
        $position_id = input('position_id',0);
        if(!$position_id) {
            return $this->redirect('position');
        }
        $resumePosition = Db::name('resume_position')->where([['uid','eq',$this->uid],['position_id','eq',$position_id]])->find();

        $isSend = 0;
        if($resumePosition) {
            $isSend = 1;
        }
        session('nav', 'position');
        $this->assign('is_send', $isSend);
        $this->assign('user_type', $this->user_type);
        $this->assign('uid', $this->uid);
        $this->assign('position_id', $position_id);
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/14 23:22
     * @comment　投递职位
     */
    public function appPosition(){
        $position_id = input('position_id',0);
        $company_id = Db::name('position')->where('id',$position_id)->value('company_id');
        $indata = [
            'uid' => $this->uid,
            'company_id' => $company_id,
            'position_id' => $position_id,
            'status' => 0,
            'is_del' => 0,
            'create_time' => time(),
            'update_time' =>time()
        ];
        $r = Db::name('resume_position')->insert($indata);
        if ($r){
            gg(1,'投递成功');
        }else{
            gg(0,'投递失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/14 23:34
     * @comment　收藏职位
     */
    public function likePosition(){
        if(!$this->is_login) {
            gg(3,'请先登录');
        }
        $position_id = input('position_id',0);
        $where = [
            ['uid','eq',$this->uid],
            ['target_id','eq',$position_id],
            ['type','eq',2]
        ];
        $id = Db::name('collection')->where($where)->value('id');
        if ($id){
            $r = Db::name('collection')->where('id',$id)->delete();
            $op = '取消收藏';
        }else{
            $company_id = Db::name('position')->where('id',$position_id)->value('company_id');
            $indata = [
                'uid' => $this->uid,
                'company_id' => $company_id,
                'target_id' => $position_id,
                'type' => 2,//收藏职位
                'create_time' => time(),
                'update_time' =>time()
            ];
            $r = Db::name('collection')->insert($indata);
            $op = '收藏';

        }
        if ($r){
            gg(1,$op.'成功');
        }else{
            gg(0,$op.'失败');
        }
    }


    /**
     * @author 北川
     * @time 2019/8/15 21:22
     * @comment　企业列表
     */
    public function company(){
        $keyword = input('keyword', '');
        $industry = Db::name('web_config')->where([['type','eq','industry']])->order('sort')->select();
        $scale = Db::name('web_config')->where([['type','eq','scale']])->order('sort')->select();

        session('nav', 'company');
        $this->assign('keyword', $keyword);
        $this->assign('industry',$industry);
        $this->assign('scale',$scale);

        return $this->fetch();
    }
    /**
     * @author 北川
     * @time 2019/8/15 21:37
     * @comment　企业详情
     */
    public function companyInfo(){
        $id= input('id');
        session('nav', 'company');
        $this->assign('id',$id);
        return $this->fetch();
    }
    /**
     * @author 北川
     * @time 2019/8/14 23:34
     * @comment　收藏/取消收藏企业
     */
    public function likeCompany(){
        if(!$this->is_login) {
            gg(3,'请先登录');
        }
        $company_id = input('company_id',0);
        $where = [
            ['uid','eq',$this->uid],
            ['target_id','eq',$company_id],
            ['type','eq',1]
        ];
        $id = Db::name('collection')->where($where)->value('id');
        if ($id){
            $r = Db::name('collection')->where('id',$id)->delete();
            $op = '取消收藏';
        }else{
            $indata = [
                'uid' => $this->uid,
                'company_id' => $company_id,
                'target_id' => $company_id,
                'type' => 1,//收藏企业
                'create_time' => time(),
                'update_time' =>time()
            ];
            $r = Db::name('collection')->insert($indata);
            $op = '收藏';
        }
        if ($r){
            gg(1,$op.'成功');
        }else{
            gg(0,$op.'失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/15 21:57
     * @comment　企业上线职位
     */
    public function companyPosition(){
        $company_id = input('company_id',0);
        $where = [];
        $where[] = ['company_id','eq',$company_id];
        $where[] = ['a.is_del','eq',0];
        $where[] = ['a.status','eq',1];
        $position_model = new Position();
        $list = $position_model->positionList($where,$this->page,$this->pageSize);
        gg(1,'success',$list);
    }


    /**
     * @author 北川
     * @time 2019/8/15 21:22
     * @comment　求职者列表
     */
    public function userList(){
        $keyword = input('keyword', '');
        $education = Db::name('web_config')->where([['type','eq','education']])->order('sort')->select();
        $experience = Db::name('web_config')->where([['type','eq','experience']])->order('sort')->select();
        $position_type = Db::name('web_config')->where([['type','eq','position_type']])->order('sort')->select();
        $this->assign('keyword',$keyword);
        $this->assign('education',$education);
        $this->assign('experience',$experience);
        $this->assign('position_type',$position_type);

        return $this->fetch();
    }

    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/16 23:14
     * @comment　人才详情
     */
    public function userInfo(){
        $id = input('id',0);

        $user_model = new \app\common\model\Users();
        $info = $user_model->getDetail($id);
        $info['tags'] = explode(',',$info['tags']);
        $this->assign('info',$info);

        $this->assign('edu_log',$info['edu_log']);
        $this->assign('work_log',$info['work_log']);
        //$this->assign('education',$this->getEduLevel());

        //$this->assign('region_data',json_encode($this->allRegionData()));

        return $this->fetch();
    }

    //签证规则
    public function visaList() {
        session('nav', 'home');
        return $this->fetch();
    }
    //签证规则详情
    public function visaInfo() {
        $id=input('param.id');
        $info=db('visa')->where(array('id'=>$id))->find();
        $info['addtime'] = date('Y-m-d',$info['addtime']);

        $this->assign('info',$info);
        session('nav', 'home');
        return $this->fetch();
    }

    //收入计算
    public function taxation() {
        session('nav', 'home');
        return $this->fetch();
    }

    //关于我们
    public function aboutus() {
        session('nav', 'home');
        return $this->fetch();
    }

    // 签证服务
    public function visa() {
        session('nav', 'visa');
        return $this->fetch();
    }

    // 表格下载
    public function downloadList() {
        session('nav', null);
        return $this->fetch();
    }
}