<?php
/**
 * Author: 北川
 * Time: 2019/9/16 22:53
 * @comment　
 */

namespace app\home\controller;

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use think\Controller;

class PayTest extends Controller
{
    public function index()
    {
        $apiContext = new \PayPal\Rest\ApiContext(new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret')));
        // ### Payer
        // A resource representing a Payer that funds a payment
        // For paypal account payments, set payment method
        // to 'paypal'.
        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information
        $item1 = new Item();
        $item1->setName('升级砖石会员')
            ->setCurrency('USD')
            ->setQuantity(1)
            //->setSku("一年")// Similar to `item_number` in Classic API
            ->setPrice(100);

        $itemList = new ItemList();
        $itemList->setItems(array($item1));
        // ### Additional payment details
        // Use this optional field to set additional
        // payment information such as tax, shipping
        // charges etc.
        $details = new Details();
        $details->setShipping(1.2)
            ->setTax(1.3)
            ->setSubtotal(17.50);

        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal(100);
            //->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it.

        $invoice = uniqid();
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('购买砖石会员')
            ->setInvoiceNumber($invoice)
            ->setNotifyUrl('http://xianxinli.3todo.com/tdmin/a/a.html');

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after
        // payment approval/ cancellation.
        $baseUrl = request()->domain();
        $redirectUrls = new RedirectUrls();
        $return_url = $baseUrl.url('payTest/payNotify',"invoice={$invoice}");
        $redirectUrls->setReturnUrl($return_url)
            ->setCancelUrl("$baseUrl/ExecutePayment.php?success=false");

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));


        // For Sample Purposes Only.
        //$request = clone $payment;

        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($apiContext);
        } catch (\Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            //ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
            exit(1);
        }

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getApprovalLink()
        // method
        $approvalUrl = $payment->getApprovalLink();

        // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
        //ResultPrinter::printResult("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", "<a href='$approvalUrl' >$approvalUrl</a>", $request, $payment);
        header("Location: {$approvalUrl}");
        //return $payment;
    }

    public function payNotify(){
        if(!isset($_GET['paymentId'], $_GET['PayerID'])){
            die();
        }

        $paymentID = $_GET['paymentId'];
        $payerId = $_GET['PayerID'];
        $apiContext = new \PayPal\Rest\ApiContext(new OAuthTokenCredential(config('paypal.client_id'), config('paypal.secret')));

        $payment = Payment::get($paymentID, $apiContext);

        $execute = new PaymentExecution();
        $execute->setPayerId($payerId);

        try{
            $result = $payment->execute($execute, $apiContext);
        }catch(\Exception $e){
            die($e);
        }
        return $payment;
        echo '支付成功！感谢支持!';
    }
    public function index2(){
        $data = [
            'cmd' => '_xclick',
            'business' => '304761681-facilitator@qq.com',
            'item_name' => '会员升级',
            'amount' => '100',
            'currency_code' => 'USD',
            'return' => 'http://test.cq.com',
            'notify_url' => 'http://test.cq.com',
            'cancel_return' => 'http://test.cq.com',
            'invoice' => 'uniqid()',
            'charset' => 'utf-8',
            'no_shipping' => '1',
            'no_note' => '1',
            'rm' => '2',
            ];
        $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
        return $this->curlRequest($url,$data,'POST');
    }

    private function curlRequest($url, $data = [], $method = 'GET')
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url); // 要访问的地址
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 对认证证书来源的检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
            if ($data != '') {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
            }
        }
        curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
        curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
        //curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
        $tmpInfo = curl_exec($curl); // 执行操作
        curl_close($curl); // 关闭CURL会话
        return $tmpInfo; // 返回数据
    }

}