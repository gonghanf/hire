<?php
/**
 * Author: 北川
 * Time: 2019/8/18 9:23
 * @comment　
 */

namespace app\home\controller;


use think\Db;

class Meeting extends Common
{
    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/11 21:02
     * @comment　招聘会列表
     */
    public function meeting(){
        session('nav','meeting');
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:29
     * @comment　资讯列表
     */
    public function news(){
        session('nav','news');
        return $this->fetch();
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:31
     * @comment　资讯详情
     */
    public function newsInfo(){
        $id=input('param.id');
        $info=db('link')->where(array('id'=>$id))->find();
        $info['addtime'] = date('Y-m-d',$info['addtime']);
        
        session('nav','news');
        $this->assign('info',$info);
        return $this->fetch();
    }

}