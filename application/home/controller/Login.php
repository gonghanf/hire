<?php
/**
 * Created by PhpStorm.
 * User: 江远
 * Date: 2018/11/30
 * Time: 18:46
 */
namespace app\home\controller;

use app\common\controller\Mail;
use phpDocumentor\Reflection\TypeResolver;
use SMS;
use tests\thinkphp\library\think\sessionTest;
use think\captcha\Captcha;
use think\Controller;
use EasyWeChat\Factory;
use Think\Db;
use think\facade\Cache;
use think\facade\Validate;

class Login extends Controller
{
    public function initialize()
    {
        if(session('language') && session('language') == 'en') {
            $this->assign('language', 'en');
        } else {
            $this->assign('language', 'cn');
        }
    }

    public function login() {
        session('uid',null);
        $this->assign('alert_msg',input('post.msg'));
        return $this->fetch();
    }
    public function mobileRegister() {
        return $this->fetch();
    }
    public function emailRegister() {
        return $this->fetch();
    }
    public function forget() {
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/6/19 22:46
     * @comment　微信授权登录
     */
    public function wxLogin(){
        // 测试默认用户
        // $uid = 3;
        // $user = Db::name('users')->where([['id','eq',$uid]])->find();
        // session('uid',$user['id']);
        // session('username',$user['username']);
        // session('avatar',$user['avatar']);
        // if(empty($user['mobile'])) {
        //     session('bindMobile', false);
        // } else {
        //     session('bindMobile', true);
        // }

        // $this->redirect('index/index');
        // die();
        // 测试默认用户
        $app = Factory::officialAccount(config('wechat.'));
        $oauth = $app->oauth;

        if (!input('code')){
            $oauth->redirect()->send();
        }
        $user = $oauth->user();
        $openid = $user->getId();
        $user_info = Db::name('users')->where([['openid','eq',$openid]])->find();
        //已注册则跳转
        if (!$user_info) {
            $no = date('YmdHis').str_pad(mt_rand(0,999999),6,0,STR_PAD_LEFT);
            //未注册则先注册
            $user_info = [
                'no' => $no,
                'reg_time' => time(),
                'create_time' => time(),
                'openid' => $user->getId(),
                'username' => $user->getNickname(),
                'avatar' => $user->getAvatar(),
                'level' => 1,
                'sex' => 0,
                'is_lock' => 0,

                //'mobile' => $user->getMobile(),
            ];
            $r = Db::name('users')->insertGetId($user_info);
            if (!$r) {
                return $this->error('登录失败');
            }
            $user_info['id'] = $r;
        }

        session('uid',$user_info['id']);
        session('avatar',$user_info['avatar']);
        session('username',$user_info['username']);
        //未绑定手机号字调到保定页面
        if (!isset($user_info['mobile'])||!$user_info['mobile']){
            $this->redirect('index/index');
        }else{
            //已绑定则进入主页面
            $this->redirect('index/index');
        }
    }

    /**
     * @author 北川
     * @time 2019/6/22 21:31
     * @comment　短信接口
     */
    public function sendSms(){
        $account = input('account');

        $op = input('type','register');
        if(preg_match('/^1[345678]\d{9}$/',$account)){
           $type = 1;
        }elseif (Validate::isEmail($account)){
            $type = 2;
        }else{
            gg(0,'账号格式有误，请检查后重新输入');
        }

        //生成验证码
        $code = rand('100000','999999');
        //发送短信
        if ($type == 1){
            $response = SMS::sendSms(
                "佳美丰", // 短信签名
                "SMS_169102276", // 短信模板编号
                $account, // 短信接收者
                Array(  // 短信模板中字段的值
                    "code"=>$code,
                ),
                "123"   // 流水号,选填
            );

            if ($response->Code == 'OK') {
                cache($op.$account, $code, 300);
                gg('1', '短信发送成功', '');
            } else {
                gg('0', '短信发送失败', $response->Message);
            }
        }else{
            //发送邮件
            $mail = new Mail();
            $r = $mail->sendEmail($account,$code);
            if ($r['code']==1){
                cache($op.$account, $code, 300);
                gg('1', '邮件发送成功', '');
            } else {
                gg('0', '邮件发送失败',$r['msg']);
            }
        }

    }

    /**
     * @author 北川
     * @time 2019/7/6 21:41
     * @comment　注册
     */
    public function userRegister(){
        $usertype = input('type');
        if (!$usertype){
            gg(0,'请选择角色身份');
        }
        $account = input('account');
        if(preg_match('/^1[345678]\d{9}$/',$account)){
            $account_type = 'mobile';
        }elseif (Validate::isEmail($account)){
            $account_type = 'email';
        }else{
            gg(0,'账号格式有误，请检查后重新输入');
        }

        $code = input('code');
        $password = input('password');

        if (!$account){
            gg(0,'请输入手机号或者邮箱');
        }
        if (!$code){
            gg(0,'请输入验证码');
        }
        if (!$password){
            gg(0,'请输入密码');
        }

        $has_mobile = Db::name('users')->where([['mobile|email','eq',$account]])->find();
        if ($has_mobile){
            gg(0,'该账号已注册');

        }
        if($code != cache('register'.$account)){
            gg(0,'验证码错误');
        }
        if (strlen($password)<6 || strlen($password)>15){
            gg(0,'请输入6~15位密码');
        }
        $token = md5(time().$account.mt_rand(1,9999));
        $indata = [
            $account_type => $account,
            'password' => md5(md5($password)),
            'username' => $account,
            'type' => $usertype,
            'sex' => '男',
            'birthday' => '2010-01-01',
            'avatar' => '/static/img/default_avatar.jpg',
            'reg_time' => time(),
            'status' => 1,
            'create_time' => time(),
            'token' => $token
        ];

        $r = Db::name('users')->insert($indata);
        if ($r){
            $indata['avatar'] = add_domain($indata['avatar']);
            gg(1,'注册成功',$indata);
        }else{
            gg(0,'注册失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/25 10:49
     * @comment　公众号内更新用户openid
     */
    public function getOpenid(){
        $option = config('wechat.');
        $option['oauth']['callback'] .= '?uid='.session('uid');
        $app = Factory::officialAccount(config('wechat.'));
        $oauth = $app->oauth;

        if (!input('code')){
            $oauth->redirect()->send();
        }
        $user = $oauth->user();
        $openid = $user->getId();
        $uid = input('uid',0);
        Db::name('users')->where('id',$uid)->setField('openid',$openid);
        $user = Db::name('users')->where('id',$uid)->find();
        $user['avatar'] = add_domain($user['avatar']);
        gg(1,'登录成功',$user);
    }

    /**
     * @author 北川
     * @time 2019/7/6 21:46
     * @comment　登录
     */
    public function userLogin(){

        $account = input('account');
        $password = input('password');
        if (!$account){
            gg(0,'请输入手机号或者邮箱');
        }
        if (!$password){
            gg(0,'请输入密码');
        }
        $user = Db::name('users')->where([['mobile|email','eq',$account]])->find();
        if (!$user){
            gg(0,'该账号未注册');
        }
        if ($user['password'] != md5(md5($password))){
            gg(0,'账号或密码错误，请确认后重新输入');
        }
        if ($user['is_lock'] == 1){
            gg(0,'您的账号已被禁用，请联系管理员解禁');
        }
        unset($user['password']);
        //$token = md5(time().$mobile.mt_rand(1,9999));
        //$r = Db::name('users')->where([['id','eq',$user['id']]])->setField('token',$token);
        //if ($r){
        session('uid',$user['id']);
        session('user_sn',$user['sn']);
        session('user_type',$user['type']);
        session('is_intermediary',$user['is_intermediary']);
        session('licence', $user['licence']);
        session('card1', $user['card1']);
        session('card2', $user['card2']);
        session('verify', $user['verify']);
        session('video', $user['video']);
        session('user_level',$user['level']);
        session('user_name',$user['username']);
        session('user_avatar',$user['avatar']);
        //如果没有openid则更新open_id;
        if (!$user['openid'] && (request()->isMobile())){
            $this->getOpenid();
        }
        if (true){
            //$user['token'] = $token;
            $user['avatar'] = add_domain($user['avatar']);

            gg(1,'登录成功',$user);
        }else{
            gg(0,'登录失败');
        }
    }

    public function logout() {
        session(null);
        gg(1,'退出成功');
    }

    /**
     * @author 北川
     * @time 2019/7/6 21:55
     * @comment　重置密码
     */
    public function resetPassword(){

        $account = input('account');
        $code = input('code');
        $password = input('password');
        if (!$account){
            gg(0,'请输入手机号或者邮箱');
        }
        if (!$code){
            gg(0,'请输入验证码');
        }
        if (!$password){
            gg(0,'请输入密码');
        }
        $user = Db::name('users')->where([['mobile|email','eq',$account]])->find();
        if (!$user){
            gg(0,'该账号未注册');
        }
        if($code != cache('reset_password'.$account)){
            gg(0,'验证码错误');
        }
        if (strlen($password)<6 || strlen($password)>15){
            gg(0,'请输入6~15位密码');
        }
        $token = md5(time().$account.mt_rand(1,9999));
        $r = Db::name('users')->where([['id','eq',$user['id']]])->update(['password'=>md5(md5($password)),'token'=>$token]);
        if ($r){
            unset($user['password']);
            gg(1,'密码已修改',$user);
        }else{
            gg(0,'密码修改失败');
        }
    }

    // 上传证件
    public function uploadFile() {
        $usertype = session('user_type');
        $card1 = session('card1');
        $card2 = session('card2');
        $licence = session('licence');
        $licence = session('licence');
        $is_intermediary = session('is_intermediary');
        $verify = session('verify');

        if($usertype == 2) {
            $user_model = new \app\common\model\Users();
            $user_info = $user_model->getCompanyInfo(session('uid'));
            // $user_info['avatar'] = add_domain($user_info['avatar']);
            $this->assign('left_info',$user_info);
        } else {
            $id = session('uid');
            $user_model = new \app\common\model\Users();

            $info = $user_model->getDetail($id);
            $info['tags'] = $info['tags_arr'];
    //        $info['qualification'] = array_filter(explode(',', $info['qualification']));

            $prov_arr = Db::name('region')->where('pid', 0)->select();
            if ($info['province']){
                $prov_id = Db::name('region')->where([['name', 'eq', $info['province']], ['type', 'eq', 1]])->value('id');
                $city_arr = Db::name('region')->where('pid', $prov_id)->select();
            }else{
                $city_arr = [];
            }
            if ($info['city']){
                $city_id = Db::name('region')->where([['name', 'eq', $info['city']], ['type', 'eq', 2]])->value('id');
                $dist_arr = Db::name('region')->where('pid', $city_id)->select();
            }else{
                $city_arr =[];
            }

            $this->assign('info', $info, true);
        }

        $this->assign('user_type', $usertype);
        $this->assign('card1', $card1);
        $this->assign('card2', $card2);
        $this->assign('licence', $licence);
        $this->assign('licence', $licence);
        $this->assign('is_intermediary', $is_intermediary);
        $this->assign('verify', $verify);
        return $this->fetch();
    }
    // 更换语言
    public function changeLang() {
        $lang = input('lang', 'en');

        session('language', $lang);
        return ['code'=>1,'msg'=>'切换成功'];
    }
}
