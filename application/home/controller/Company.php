<?php
/**
 * Author: 北川
 * Time: 2019/8/15 22:46
 * @comment　
 */

namespace app\home\controller;


use alipay\Pagepay;
use alipay\Wappay;
use app\common\model\Position;
use app\common\model\ResumePosition;
use app\common\model\Users;
use app\common\model\WebConfig;
use app\common\model\Goods;
use EasyWeChat\Factory;
use think\Db;

class Company extends Common
{
    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:17
     * @comment　企业中心
     */
    public function companyCenter(){
        $user_model = new Users();
        $user_info = $user_model->getCompanyInfo($this->uid);
        // $user_info['avatar'] = add_domain($user_info['avatar']);
        $this->assign('left_info',$user_info);
        return $this->fetch();
        //gg($user_info);
    }

    /**
     * @author 北川
     * @time 2019/6/19 23:14
     * @comment　企业信息详情
     */
    public function companyInfo(){
        $uid = $this->uid;
        $user_info = Db::name('users')->where([['id','eq',$uid]])->find();
        $user_info['reg_time'] = date('Y-m-d H:i',$user_info['reg_time']);
        $user_info['level_text'] = Db::name('user_level')->where([['level_id','eq',$user_info['level']]])->value('level_name');
        // $user_info['avatar'] = add_domain($user_info['avatar']);
        $config = WebConfig::allConfig();

        $this->assign('info',$user_info);
        $this->assign('industry',$config['industry']);
        $this->assign('scale',$config['scale']);

        $this->assign('left_info',model('users')->getCompanyInfo($this->uid));
        return $this->fetch();
        //gg($user_info);

    }

    /**
     * @author 北川
     * @time 2019/8/18 9:18
     * @comment　修改企业信息
     */
    public function editCompanyInfo(){
        $data = input('post.');
        unset($data['file']);
        $users_model = new Users();
        $r = $users_model->isUpdate(true)->allowField(true)->save($data,['id' => $this->uid]);
        if ($r){
            gg(1,'修改成功');
        }else{
            gg(0,'修改失败');
        }
    }


    /**
     * @author 北川
     * @time 2019/8/13 23:26
     * @comment　上传营业执照
     */
    public function editLicence(){
        if (request()->isPost()){
            $id = $this->uid;
            $licence = input('licence');
            if(!$licence){
                gg(0,'请上传营业执照');
            }
            $r = Db::name('users')->where([['id','eq',$id]])->update(['card1' => $licence,'update_time'=>time()]);
            if ($r){
                gg(1,'操作成功');
            }else{
                gg(0,'操作失败');
            }
        }
        return $this->fetch();
    }
    /**
     * @author 北川
     * @time 2019/8/14 22:47
     * @comment　职位列表
     */
    public function positionList(){
        if (request()->isPost()){
            $where = [];
            $company_id = $this->uid;
            $where[] = ['company_id','eq',$company_id];
            //职位名称
            if (input('keyword')){
                $where[] = ['name','like','%'.input('keyword').'%'];
            }
            //经验
            if (input('experience')){
                $where[] = ['experience','eq',input('experience')];
            }
            //学历
            if (input('education')){
                $where[] = ['education','eq',input('education')];
            }
            //职位类型
            if (input('position_type')){
                $where[] = ['position_type','eq',input('position_type')];
            }
            //薪资
            if (input('salary_min')){
                $where[] = ['salary_min','egt',input('salary_min')];
            }
            if (input('salary_max')){
                $where[] = ['salary_max','elt',input('salary_max')];
            }

            $position_model = new Position();
            $list = $position_model->positionList($where,$this->page,$this->pageSize);
            gg(1,'success',$list);
        }

        $education = Db::name('web_config')->where([['type','eq','education']])->order('sort')->select();
        $experience = Db::name('web_config')->where([['type','eq','experience']])->order('sort')->select();
        $position_type = Db::name('web_config')->where([['type','eq','position_type']])->order('sort')->select();
        $this->assign('education',$education);
        $this->assign('experience',$experience);
        $this->assign('position_type',$position_type);

        return $this->fetch();
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/15 23:04
     * @comment　修改发布职位  带id上传则为更新 ,否则为新增
     */
    public function addEditPosition(){

        $id = input('id',0);
        $info = Db::name('position')->where('id',$id)->find();
        if ($info){
            $info['tags'] = array_filter(explode(',', $info['tags']));
            $info['insurance'] = array_filter(explode(',', $info['insurance']));
            $info['qualification'] = array_filter(explode(',', $info['qualification']));
            $info['welfare'] = array_filter(explode(',', $info['welfare']));
            $prov_arr = Db::name('region')->where('pid',0)->select();
            $prov_id = Db::name('region')->where([['name','eq',$info['province']],['type','eq',1]])->value('id');
            $city_arr = Db::name('region')->where('pid',$prov_id)->select();
            $city_id = Db::name('region')->where([['name','eq',$info['city']],['type','eq',2]])->value('id');
            $dist_arr = Db::name('region')->where('pid',$city_id)->select();
        }else{
            $info['tags'] = [];
            $info['insurance'] = [];
            $info['qualification'] = [];
            $info['welfare'] = [];
            $prov_arr = Db::name('region')->where('pid',0)->select();
            $city_arr = [];
            $dist_arr = [];
        }

//        $tags = Db::name('web_config')->where([['type','eq','tags']])->select();
//        $welfare = Db::name('web_config')->where([['type','eq','welfare']])->select();
//        if($info) {
//            $info['tags'] = explode(',', $info['tags']);
//            $info['add_tags'] = $info['tags'];
//            foreach ($tags as $key => $o) {
//                foreach ($info['add_tags'] as $key2 => $p) {
//                    if($o['name'] == $p) {
//                        unset($info['add_tags'][$key2]);
//                    }
//                }
//            }
//        } else {
//            $info['tags'] = [];
//        }
        $config = WebConfig::allConfig();
        $tag_arr = [];
        foreach ($config['tags'] as $k=>$v){
            $tag_arr[] = $v['name'];
        }
        $tags = array_unique(array_merge($tag_arr,$info['tags']));

        $qual_arr = [];
        foreach ($config['qualification'] as $k=>$v){
            $qual_arr[] = $v['name'];
        }
        $quals = array_unique(array_merge($qual_arr,$info['qualification']));

        $insurance = array_unique(array_merge(['medical','accident'],$info['insurance']));;

        $this->assign('insurance',$insurance);
        $this->assign('quals',$quals);
        $this->assign('info',$info);
        $this->assign('tags',$tags);
        $this->assign('welfare',$config['welfare']);
        $this->assign('education',$config['education']);
        $this->assign('experience',$config['experience']);
        $this->assign('province',$prov_arr);
        $this->assign('city',$city_arr);
        $this->assign('district',$dist_arr);

        $this->assign('left_info',model('users')->getCompanyInfo($this->uid));

        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/15 21:22
     * @comment　人才列表
     */
    public function userList(){

        $keyword = input('keyword', '');
        $education = Db::name('web_config')->where([['type','eq','education']])->order('sort')->select();
        $experience = Db::name('web_config')->where([['type','eq','experience']])->order('sort')->select();
        $position_type = Db::name('web_config')->where([['type','eq','position_type']])->order('sort')->select();

        $this->assign('keyword', $keyword);
        $this->assign('education',$education);
        $this->assign('experience',$experience);
        $this->assign('position_type',$position_type);

        return $this->fetch();
    }
    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/16 23:14
     * @comment　人才详情
     */
    public function userInfo(){
        $user_id = input('user_id',0);
        if(!$user_id) {
            return $this->redirect('userList');
        }
        $this->assign('user_id', $user_id);
        return $this->fetch();
    }
    /**
     * @author 北川
     * @time 2019/8/14 23:34
     * @comment　收藏/取消收藏人才
     */
    public function likeUser(){
        $user_id = input('user_id',0);
        $where = [
            ['uid','eq',$this->uid],
            ['target_id','eq',$user_id],
            ['type','eq',3]
        ];
        $id = Db::name('collection')->where($where)->value('id');
        if ($id){
            $r = Db::name('collection')->where('id',$id)->delete();
            $op = '取消收藏';
        }else{
            $indata = [
                'uid' => $this->uid,
                'target_id' => $user_id,
                'type' => 3,//收藏人才
                'create_time' => time(),
                'update_time' =>time()
            ];
            $r = Db::name('collection')->insert($indata);
            $op = '收藏';
        }
        if ($r){
            gg(1,$op.'成功');
        }else{
            gg(0,$op.'失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/5 23:43
     * @comment　简历库
     */
    public function resumeRepository(){
        $user_model = new Users();
        $user_info = $user_model->getCompanyInfo($this->uid);
        $this->assign('left_info',$user_info);
        return view();
    }



    /**
     * @author 北川
     * @time 2019/8/15 22:35
     * @comment　企业收藏列表
     */
    public function myCollection(){
        $user_model = new Users();
        $user_info = $user_model->getCompanyInfo($this->uid);
        // $user_info['avatar'] = add_domain($user_info['avatar']);
        $this->assign('left_info',$user_info);
        return view();
    }

    /**
     * @author 北川
     * @time 2019/8/25 10:20
     * @comment　消费记录
     */
    public function orderList(){
        $company_id = $this->uid;
        $where = [];
        $where[] = ['uid','eq',$company_id];
        $where[] = ['id_del','eq',0];
        $where[] = ['status','eq',1];//已支付
        $order_model = new \app\common\model\Order();
        $list = $order_model->orderList($where);
        gg(1,'success',$list);
    }

    /**
     * @return mixed
     * @author 北川
     * @time 2019/9/19 22:58
     * @comment　充值
     */
    public function recharge() {

        $goods_model = new Goods();
        $list = $goods_model->goodsList();
        $this->assign('goods',$list);

        $user_model = new Users();
        $user_info = $user_model->getCompanyInfo($this->uid);
        // $user_info['avatar'] = add_domain($user_info['avatar']);
        $this->assign('left_info',$user_info);
        $content = $this->fetch();
        return $this->fetch();
    }

    public function myOrderList() {
        $user_model = new Users();
        $user_info = $user_model->getCompanyInfo($this->uid);
        $this->assign('left_info',$user_info);
        return $this->fetch();
    }

    public function myScore() {
        $user_model = new Users();
        $user_info = $user_model->getCompanyInfo($this->uid);
        $user_info['total_point'] = Db::name('point_detail')->where([['uid','eq',$this->uid],['type','eq',1]])->sum('amount');
        $this->assign('left_info',$user_info);

        $point = Db::name('users')->where('id',$this->uid)->value('point');
        $total = Db::name('point_detail')->where([['uid','eq',$this->uid],['type','eq',1]])->sum('amount');
        $this->assign('total',$total);
        $this->assign('point',$point);
        return $this->fetch();
    }

    public function myQrcode() {
        $avatar = session('user_avatar');
        $invite_url = request()->domain().'/'.'mobile/login/mobileregister.html?pid='.session('uid');
        $this->assign('avatar',$avatar);
        $this->assign('invite_url',$invite_url);
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/25 10:22
     * @comment　充值和解锁手机号购买
     */
    public function buy(){

        $pay_type = input('pay_type');
        $open_id = Db::name('users')->where('id',$this->uid)->value('openid');
        $goods_id = input('goods_id',0);
        $goods_info = Db::name('goods')->where('id',$goods_id)->find();
        if (!$goods_info){
            gg(0,'数据异常');
        }
        //生成订单
        $order_data =[
            'sn' => create_sn(),
            'goods_id' => $goods_info['id'],
            'goods_name' => $goods_info['name'],
            'price' => $goods_info['price'],
            'num' => 1,
            'goods_amount' => $goods_info['price']*1,
            'uid' => $this->uid,
            'username' => $this->username,
            'user_sn' => $this->user_sn,
            'status' => 0,
            'is_del' =>0,
            'create_time' => time(),
            'update_time' => time(),
        ];
        $res = Db::name('order')->insertGetId($order_data);
        if (!$res){
            gg(0,'订单生成失败');
        }
        //支付
        $this->orderPay($pay_type,$order_data,$open_id);

    }

    /**
     * @param $pay_type
     * @param $order_data
     * @param string $openid
     * @author 北川
     * @time 2019/8/25 11:22
     * @comment　支付接口
     */
    public function orderPay($pay_type,$order_data,$openid = ''){

        if ($pay_type == 'ali_wap'){
            $param = [
                'subject' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_amount' =>$order_data['goods_amount']
            ];
            $result = Wappay::pay($param,request()->domain().'#/form',request()->domain().'/mobile/pay_notify/aliNotify');
            gg(1,'success',$this->display($result));
            //return ['code'=>1,'data'=>['data'=>$this->display($result),'type'=>'ali_pc']];
        }elseif ($pay_type == 'ali_pc'){
            $param = [
                'subject' => $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],
                'total_amount' =>$order_data['goods_amount']
            ];
            $result = Pagepay::pay($param,request()->domain().'#/form',request()->domain().'/mobile/pay_notify/aliNotify');
            gg(1,'success',$result);

        }elseif ($pay_type == 'wx_wap'){
            //$options = config('wxpay');
            $app = Factory::payment(config('wechat.'));
            $attributes = [
                'trade_type' => 'JSAPI', // JSAPI，NATIVE，APP...
                'body' => $order_data['goods_name'],
                'detail'=> $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],//订单号
                'total_fee' => 1,
                //'total_fee' => $order_data['goods_amount']*100, // 单位：分
                'notify_url' => request()->Domain().'/mobile/pay_notify/wxpayNotify', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
                'openid' => $openid, // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识
                //'openid' => 'oEvUi5lr6VbH-6EXWAIIukIoJFnE', // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识
            ];
            //生成预付款订单
            $result = $app->order->unify($attributes);
            if ($result['return_code'] == 'SUCCESS' && $result['result_code'] == 'SUCCESS') {
                $prepayId = $result['prepay_id'];
                //生成支付 JS 配置
                $json = $app->jssdk->bridgeConfig($prepayId);
                //$json = $app->configForPayment($prepayId); // 返回 json 字符串，如果想返回数组，传第二个参数 false
                gg(1, 'success', $json);
            }else{
                gg(0, '支付失败', $result);
            }
        }elseif ($pay_type == 'wx_pc'){
            $app = Factory::payment(config('wechat.'));
            $attributes = [
                'trade_type' => 'JSAPI', // JSAPI，NATIVE，APP...
                'body' => $order_data['goods_name'],
                'detail'=> $order_data['goods_name'],
                'out_trade_no' => $order_data['sn'],//订单号
                'total_fee' => 1,
                //'total_fee' => $order_data['goods_amount']*100, // 单位：分
                'notify_url' => request()->Domain().'/mobile/pay_notify/wxpayNotify', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
                'openid' => $openid, // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识
                //'openid' => 'oEvUi5lr6VbH-6EXWAIIukIoJFnE', // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识
            ];
            //生成预付款订单
            $result = $app->order->unify($attributes);
            if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
                //var_dump($result);
                //$prepayId = $result->prepay_id;
                gg(1, 'success', ['data'=>$result['code_url'],'type'=>'wx_pc','out_trade_no'=>$order_data['sn']]);
            }else{
                gg(0, '支付失败', $result);
            }
        }else{
            gg(0,'支付方式异常');
        }
    }

}