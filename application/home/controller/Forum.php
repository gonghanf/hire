<?php
/**
 * Author: 北川
 * Time: 2019/8/18 9:32
 * @comment　
 */

namespace app\home\controller;


use think\Db;

class Forum extends Common
{
    public function initialize()
    {
        parent::initialize();
        if (!session('uid')){
            $redirect_url = request()->domain().url('/home/login/login');

            $data = array('msg' => '贴吧板块登陆后才能访问，请先登录！');
            $headers = array('Content-Type: application/x-www-form-urlencoded');
            exit(httpRequest($redirect_url,'post',$data,$headers));
        }
    }
    /**
     * @return mixed
     * @author 北川
     * @time 2019/8/18 9:36
     * @comment　帖子首页
     */
    public function index(){
        $keyword = input('keyword', '');
        $list = Db::name('forum_category')->select();
//        foreach ($list as $k=>$v){
//            $v['name'] = $v['en_name'];
//            $list[$k] = $v;
//        }
        $banner = Db::name('ad')->where('as_id',5)->select();

        session('nav', 'forum');
        $this->assign('keyword', $keyword);
        $this->assign('category',$list);
        $this->assign('banner',$banner);
        return $this->fetch();
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　帖子列表
     */
    public function forumList(){
        $page =input('page')?input('page'):1;
        $pageSize =input('limit')?input('limit'):config('pageSize');
        $condition = [];
        $condition[] = ['a.is_hide','eq',0];
        //$condition[] = ['is_delete','eq',0];
        if (input('get.title')){
            $condition[] = ['a.title','like','%'.input('get.title').'%'];
        }
            if (input('get.cat_id')){
            $condition[] = ['c.cat_id','eq',input('get.cat_id')];
        }
        $list = Db::name('forum')
            ->alias('a')
            //->leftJoin('forum_category c','a.cat_id = c.id')
            ->leftJoin('users d','a.uid = d.id')
            ->field('a.*,d.username')
            ->where($condition)
            ->order('a.is_top desc,a.id desc')
            ->paginate(array('list_rows'=>$pageSize,'page'=>$page))
            ->toArray();
        //$sql = Db::getLastSql();
        foreach ($list['data'] as $k=>$v){
            $list['data'][$k]['create_time'] = date('Y-m-d H:i',$v['create_time']);
            $list['data'][$k]['desc'] = strip_tags($v['content']);
        }
        gg(1,'success',$list);
    }

    /**
     * @return array|mixed
     * @author 北川
     * @time 2019/8/18 9:33
     * @comment　帖子详情
     */
    public function forumInfo(){
        $id = input('id');
        session('nav', 'forum');
        $this->assign('id',$id);
        return $this->fetch();
    }

    /**
     * @author 北川
     * @time 2019/8/21 23:28
     * @comment　发表编辑帖子
     */
    public function addEditForum(){
        $id = input('id', 0);
        if ($id){
            $info = Db::name('forum')->where('id',$id)->find();
        }else{
            $info = [];
        }

        $category = Db::name('forum_category')->select();
        $this->assign('category',$category);
        $this->assign('info',$info);
        return $this->fetch();
    }

    public function replyInfo() {
        $id = input('id', 0);

        $reply = Db::name('forum_reply')->where([['id','eq',$id]])->find();
        $user = Db::name('users')->where([['id','eq',$reply['uid']]])->find();
        $reply['create_time'] = date('Y-m-d H:i:s', $reply['create_time']);
        $goodCount = Db::name('forum_good')->where([['type','eq',2],['reply_id','eq',$id]])->count();

        $this->assign('good_count', $goodCount);
        $this->assign('user', $user);
        $this->assign('reply', $reply);
        $this->assign('id', $id);
        return $this->fetch();
    }

    /**
     * @return array|\think\response\View
     * @author 北川
     * @time 2019/8/12 22:15
     * @comment　评论列表
     */
    public function replyList(){
        $fid = input('forum_id',0);

        $list = Db::name('forum_reply')
            ->alias('a')
            ->leftJoin('users b','a.uid=b.id')
            ->leftJoin('forum_good c','a.id=c.reply_id and c.uid='.$this->uid)
            ->field('a.*,b.username,b.sn,c.id good')
            ->where([['fid','eq',$fid],['pid','eq',0],['is_del','eq',0]])
            ->paginate(array('list_rows'=>$this->pageSize,'page'=>$this->page))
            ->toArray();
        foreach($list['data'] as $k=>$v){
            //$list[$k]['lay_is_open']=true;

            $list['data'][$k]['create_time']=date('Y-m-d H:i',$v['create_time']);
            $list['data'][$k]['child'] = $this->getChildReply($v['id']);
            $list['data'][$k]['good'] = $v['good']?true:false;
        }
        gg(1,'success',$list);
    }

    /**
     * @author 北川
     * @time 2019/8/18 10:02
     * @comment　获取回复评论
     */
    public function getChildReply($pid){
        $list = Db::name('forum_reply')
            ->alias('a')
            ->leftJoin('users b','a.uid=b.id')
            ->field('a.*,b.username,b.sn')
            ->where([['pid','eq',$pid],['is_del','eq',0]])
            ->select();
        if ($list){
            foreach($list as $k=>$v){
                $list[$k]['create_time']=date('Y-m-d H:i',$v['create_time']);
                $list[$k]['good'] = $v['good']?true:false;
                $list[$k]['child'] = $this->getChildReply($v['id']);
            }
        }else{
            $list = [];
        }
        return $list;

    }

    /**
     * @author 北川
     * @time 2019/8/18 9:49
     * @comment　评论和回复评论
     */
    public function reply(){
        $fid = input('forum_id',0);
        $rid = input('reply_id',0);
        $uid = $this->uid;
        $content = input('content');
        $indata = [
            'fid' => $fid,
            'pid' => $rid,
            'uid' => $uid,
            'content' => $content,
            'is_del' => 0,
            'create_time' => time(),
            'update_time' => time()
        ];
        $r = Db::name('forum_reply')->insert($indata);
        if ($r){
            Db::name('forum')->where([['id','eq',$fid]])->setInc('reply_num',1);
            gg(1,'评论成功');
        }else{
            gg(0,'评论失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 9:57
     * @comment　删除评论
     */
    public function delReply(){
        $id = input('reply_id',0);
        $reply_info = Db::name('forum_reply')->where([['id','eq',$id]])->find();
        if (!$reply_info){
            gg(0,'删除失败',1);
        }
        $r = Db::name('forum_reply')->where([['id','eq',$id]])->setField('is_del',1);
        if ($r){
            Db::name('forum')->where([['id','eq',$reply_info['fid']]])->setDec('reply_num',1);
            gg(1,'删除成功');
        }else{
            gg(0,'删除失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 16:49
     * @comment　点赞帖子
     */
    public function forumGood(){
        $id = input('forum_id',0);
        $has_good = Db::name('forum_good')->where([['uid','eq',$this->uid],['forum_id','eq',$id]])->find();
        if ($has_good){
            gg(1,'点赞成功');
        }
        $indata = [
            'forum_id' => $id,
            'uid' => $this->uid,
            'type' => 1,
            'create_time' => time(),
            'update_time' => time(),
        ];
        $r= Db::name('forum_good')->insert($indata);
        if ($r){
            Db::name('forum')->where([['id','eq',$id]])->setInc('good_num',1);
            gg(1,'点赞成功');
        }else{
            gg(0,'点赞失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 16:49
     * @comment　点赞评论
     */
    public function replyGood(){
        $id = input('forum_id',0);
        $rid = input('reply_id',0);
        $has_good = Db::name('forum_good')->where([['uid','eq',$this->uid],['forum_id','eq',$id],['reply_id','eq',$rid]])->find();
        if ($has_good){
            gg(1,'点赞成功');
        }
        $indata = [
            'forum_id' => $id,
            'reply_id' => $rid,
            'uid' => $this->uid,
            'type' => 2,//点赞评论
            'create_time' => time(),
            'update_time' => time(),
        ];
        $r= Db::name('forum_good')->insert($indata);
        if ($r){
            Db::name('forum_reply')->where([['id','eq',$id]])->setInc('good_num',1);
            gg(1,'点赞成功');
        }else{
            gg(0,'点赞失败');
        }
    }

    /**
     * @author 北川
     * @time 2019/8/18 16:58
     * @comment　举报帖子和评论   type = 1 举报帖子 2举报评论
     */
    public function forumComplain(){
        $forum_id = input('forum_id',0);
        $reply_id = input('reply_id',0);
        $type = input('type',1);
        $content = input('content');
        if (!$content){
            gg(0,'请输入举报内容');
        }

        $indata = [
            'forum_id' => $forum_id,
            'reply_id' => $reply_id,
            'uid' => $this->uid,
            'type' => $type,//点赞评论
            'content' => $content,//举报内容
            'create_time' => time(),
            'update_time' => time(),
        ];
        $r= Db::name('forum_complain')->insert($indata);
        if ($r){
            gg(1,'举报成功');
        }else{
            gg(0,'举报失败');
        }
    }
    // 举报页面
    public function report() {
        $forum_id = input('forum_id', 0);
        $reply_id = input('reply_id', 0);

        if($forum_id > 0) {
            $type = 1;
        } else {
            $type = 2;
        }
        $this->assign('type', $type);
        $this->assign('forum_id', $forum_id);
        $this->assign('reply_id', $reply_id);
        return $this->fetch();
    }
}