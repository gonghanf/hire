<?php
namespace app\home\controller;
use app\common\controller\GG;
use think\Db;
use clt\Leftnav;
use think\Controller;
class Common extends GG
{
    protected $page,$pagesize,$isLogin,$uid,$username,$user_avatar,$user_level,$user_type,$user_sn;
    public function initialize()
    {


        if(isMobile()){
            $this->redirect('mobile/index/index');
        }

        $controller = request()->controller();
        $action = request()->action();

        if(session('language') && session('language') == 'en') {
            $verify_msg = 'Other operations can only be carried out after improving enterprise information!';

            $this->assign('language', 'en');
        } else {
            $verify_msg = '完善企业信息后才能进行其他操作！';

            $this->assign('language', 'cn');
        }


        $this->is_login = true;
        if (session('user_type') == 2){
            if($controller == 'Index' && in_array($action, ['index','likeposition','likecompany','changelang','visalist','taxation'])) {
                if(!session('uid')) {
                    $this->is_login = false;
                }
            } else {
                if (!session('uid')){
                    $this->redirect('login/login');
                    exit;
                }
                if (!session('verify')){
                    if ((strtolower($controller).'/'.strtolower($action)) != 'company/companyinfo'){
                        //                $this->assign('verify_msg',$verify_msg);
                        //                $this->redirect('home/company/companyInfo?verify_msg='.$verify_msg);
                        $this->redirect(url('home/company/companyInfo').'?verify_msg='.$verify_msg);
                    }
                }
            }
        }

        $this->uid = session('uid');
        $this->user_level = session('user_level');
        $this->user_type = session('user_type');
        $this->is_intermediary = session('is_intermediary');
        $this->licence = session('licence');
        $this->card1 = session('card1');
        $this->card2 = session('card2');
        $this->verify = session('verify');
        $this->username = session('user_name');
        $this->user_avatar = session('user_avatar');
        $this->user_sn = session('user_sn');
        $this->page = input('page')?:1;
        $this->pageSize = input('limit')?:config('app.pageSize');
        $this->assign('user_type', $this->user_type);


    }
    //空操作
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }
    public function changeLang() {
        $lang = input('lang', 'en');

        session('language', $lang);
        return ['code'=>1,'msg'=>'切换成功'];
    }
}
